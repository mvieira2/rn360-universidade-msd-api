using System.Security.Cryptography;
using System.Text;
using System.Linq;

namespace UniversidadeMsd.Persistence.Utils {

    public class Hashing
    {
        public static string ToSha1(string data)
        {
            var bytes = Encoding.UTF8.GetBytes(data);
            SHA1Managed hashAlgorithm = new SHA1Managed();
            var computedHash = hashAlgorithm.ComputeHash(bytes);
            var builder = new StringBuilder();
            foreach (var part in computedHash)
                builder.Append($"{part:x2}");
            return builder.ToString();
        }
    }
}