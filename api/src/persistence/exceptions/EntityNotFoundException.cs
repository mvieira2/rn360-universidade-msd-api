using System;

namespace UniversidadeMsd.Persistence.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public String EntityName { get; private set; }

        public EntityNotFoundException() { }
        public EntityNotFoundException(Type type) : base($"{type.Name} does not exist.")
        {
            EntityName = type.Name.ToLower();
        }
    }
}
