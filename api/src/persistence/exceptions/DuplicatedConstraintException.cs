using System;

namespace UniversidadeMsd.Persistence.Exceptions
{
    public class DuplicatedConstraintException : Exception
    {
        public DuplicatedConstraintException() { }
        public DuplicatedConstraintException(string message) : base(message)
        {
        }
    }
}
