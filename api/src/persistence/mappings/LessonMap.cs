using FluentNHibernate.Mapping;
using NHibernate.Type;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.ObjectValues;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class LessonMap : ClassMap<Lesson>
    {
        public LessonMap()
        {
            Id(s => s.Id);
            Map(s => s.CreatedAt);
            Map(s => s.Identifier).Not.Nullable();
            Map(s => s.Name).Not.Nullable();
            Map(s => s.Teachers).Nullable();
            Map(s => s.Description).Length(1000).Nullable();
            Map(s => s.StartedAt).Nullable();
            Map(s => s.Link).Nullable();
            Map(s => s.ProfileAccess).Nullable();
            Map(s => s.Active).Not.Nullable();
            Map(s => s.Options).CustomType(typeof(LessonOptions)).Not.Nullable();
            HasManyToMany(s => s.Schools).Table("LessonSchool")
                                         .ParentKeyColumn("LessonId")
                                         .ChildKeyColumn("SchoolId")
                                         .Cascade.SaveUpdate()
                                         .Not.LazyLoad();
            //HasMany(s => s.Questions)
            //    .Cascade.All()
            //    .LazyLoad()
            //    .Table("LessonQuestion")
            //    .KeyColumn("LessonId")
            //    .Component(s => { s.Map(x => x.Title); });

            References(c => c.Segment).Column("SegmentId").Nullable().Not.LazyLoad();
        }
    }
}
