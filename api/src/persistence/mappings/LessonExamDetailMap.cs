﻿using FluentNHibernate.Mapping;
using NHibernate.Type;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.ObjectValues;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class LessonExamDetailMap : ClassMap<LessonExamDetail>
    {
        public LessonExamDetailMap()
        {
            Id(s => s.Id);
            Map(s => s.Question).Not.Nullable();
            Map(s => s.Answer).Not.Nullable();
            Map(s => s.IsRight).Not.Nullable();

            References(c => c.LessonExam).Column("LessonExamId").Nullable().Not.LazyLoad();
        }
    }
}