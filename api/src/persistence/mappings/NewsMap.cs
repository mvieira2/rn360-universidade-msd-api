using FluentNHibernate.Mapping;
using System;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class NewsMap : ClassMap<News>
    {
        public NewsMap()
        {
            Id(s => s.Id);
            Map(s => s.CreatedAt);
            Map(s => s.Identifier).Not.Nullable();
            Map(s => s.Title).Not.Nullable();
            Map(s => s.Content).Length(Int32.MaxValue).Nullable();
            Map(s => s.Description).Length(1000).Nullable();
            Map(s => s.PublishedAt).Not.Nullable();
            Map(s => s.Photo).Nullable();
            HasManyToMany(s => s.Schools).Table("NewsSchool")
                                         .ParentKeyColumn("NewsId")
                                         .ChildKeyColumn("SchoolId")
                                         .Cascade.SaveUpdate()
                                         .Not.LazyLoad();
            References(c => c.Segment).Column("SegmentId").Nullable().Not.LazyLoad();
        }
    }
}
