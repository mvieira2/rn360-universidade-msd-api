﻿using FluentNHibernate.Mapping;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class GameficationActionMap : ClassMap<GameficationAction>
    {
        public GameficationActionMap()
        {
            Id(s => s.Id);
            Map(s => s.Identifier).Not.Nullable();
            Map(s => s.ActionName).Not.Nullable();
            Map(s => s.Points).Not.Nullable();
            Map(s => s.UniqueUserPoint).Not.Nullable();
        }
    }
}
