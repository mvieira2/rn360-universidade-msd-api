﻿using FluentNHibernate.Mapping;
using NHibernate.Type;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.ObjectValues;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class LessonCommentsMap : ClassMap<LessonComments>
    {
        public LessonCommentsMap()
        {
            Id(s => s.Id);
            Map(s => s.CreatedAt);
            Map(s => s.Identifier).Not.Nullable();
            Map(s => s.Comments).Not.Nullable();
            Map(s => s.StatusComment).Not.Nullable(); //CustomType(typeof(StatusComment))

            References(c => c.Lesson).Column("LessonId").Nullable().Not.LazyLoad();
            References(c => c.User).Column("UserId").Nullable().Not.LazyLoad();
        }
    }
}
