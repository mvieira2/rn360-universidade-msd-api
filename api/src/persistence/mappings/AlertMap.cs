using FluentNHibernate.Mapping;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class AlertMap : ClassMap<Alert>
    {
        public AlertMap()
        {
            Id(s => s.Id);
            Map(s => s.CreatedAt);
            Map(s => s.Identifier).Not.Nullable();
            Map(s => s.Name).Not.Nullable();
            Map(s => s.Description).Length(1000).Nullable();
            HasManyToMany(s => s.Schools).Table("AlertSchool")
                                         .ParentKeyColumn("AlertId")
                                         .ChildKeyColumn("SchoolId")
                                         .Cascade.SaveUpdate()
                                         .Not.LazyLoad();
            References(c => c.Segment).Column("SegmentId").Nullable().Not.LazyLoad();
        }
    }
}
