﻿using FluentNHibernate.Mapping;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class LessonQuestionMap : ClassMap<LessonQuestion>
    {
        public LessonQuestionMap()
        {
            Id(s => s.Id);
            Map(s => s.Identifier).Not.Nullable();
            Map(s => s.Title).Length(1000).Not.Nullable();

            References(c => c.Lesson).Column("LessonId").Nullable().Not.LazyLoad();

            HasMany(s => s.Answers)
                .Cascade.All()
                .LazyLoad()
                .Table("LessonQuestionAnswer")
                .KeyColumn("LessonQuestionId")
                .Component(s => { s.Map(x => x.Identifier); s.Map(x => x.Value); s.Map(x => x.IsRight); });
        }
    }
    }