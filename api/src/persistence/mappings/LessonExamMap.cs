﻿using FluentNHibernate.Mapping;
using NHibernate.Type;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.ObjectValues;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class LessonExamMap : ClassMap<LessonExam>
    {
        public LessonExamMap()
        {
            Id(s => s.Id);
            Map(s => s.CreatedAt);
            Map(s => s.Identifier);
            Map(s => s.QtyQuestions).Nullable();
            Map(s => s.QtySuccess).Nullable();
            Map(s => s.QtyWrong).Nullable();

            References(c => c.Lesson).Column("LessonId").Nullable().Not.LazyLoad();
            References(c => c.User).Column("UserId").Nullable().Not.LazyLoad();

            HasMany(s => s.Details)
                .Cascade.All()
                .LazyLoad()
                .Table("LessonExamDetail")
                .KeyColumn("LessonExamId")
                .Component(s => { s.Map(x => x.Question); s.Map(x => x.Answer); s.Map(x => x.IsRight); });
        }
    }
}