using FluentNHibernate.Mapping;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class DownloadMap : ClassMap<Download>
    {
        public DownloadMap()
        {
            Id(s => s.Id);
            Map(s => s.CreatedAt);
            Map(s => s.Identifier).Not.Nullable();
            Map(s => s.Name).Not.Nullable();
            Map(s => s.Description).Length(1000).Nullable();
            Map(s => s.DownloadFile).Nullable();
            HasManyToMany(s => s.Schools).Table("DownloadSchool")
                                         .ParentKeyColumn("DownloadId")
                                         .ChildKeyColumn("SchoolId")
                                         .Cascade.SaveUpdate()
                                         .Not.LazyLoad();
            References(c => c.Segment).Column("SegmentId").Nullable().Not.LazyLoad();
        }
    }
}
