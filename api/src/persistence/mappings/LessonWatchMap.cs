﻿using FluentNHibernate.Mapping;
using NHibernate.Type;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.ObjectValues;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class LessonWatchMap : ClassMap<LessonWatch>
    {
        public LessonWatchMap()
        {
            Id(s => s.Id);
            Map(s => s.CreatedAt);
            Map(s => s.Identifier);

            References(c => c.Lesson).Column("LessonId").Nullable().Not.LazyLoad();
            References(c => c.User).Column("UserId").Nullable().Not.LazyLoad();
        }
    }
}