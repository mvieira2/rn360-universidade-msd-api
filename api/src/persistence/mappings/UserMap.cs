using FluentNHibernate.Mapping;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.ObjectValues;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Id(s => s.Id);
            Map(s => s.CreatedAt);
            Map(s => s.Name).Not.Nullable();
            Map(s => s.Password).Not.Nullable();
            Map(s => s.Identifier).Not.Nullable();
            Map(s => s.Email).Not.Nullable();
            Map(c => c.Phone).Not.Nullable();
            Map(c => c.Profile).CustomType(typeof(Profile)).Not.Nullable();
            Map(c => c.Photo).Nullable();
            Map(c => c.HashNewPassword).Nullable();
            Map(c => c.Active).Not.Nullable();
            References(c => c.School).Column("SchoolId").Nullable().Not.LazyLoad();
            References(c => c.Segment).Column("SegmentId").Nullable().Not.LazyLoad();
        }
    }
}
