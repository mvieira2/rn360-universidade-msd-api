﻿using FluentNHibernate.Mapping;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class LessonQuestionAnswerMap : ClassMap<LessonQuestionAnswer>
    {
        public LessonQuestionAnswerMap()
        {
            Id(s => s.Id);
            Map(s => s.Identifier).Not.Nullable();
            Map(s => s.Value).Length(1000).Not.Nullable();
            Map(s => s.IsRight).Not.Nullable();

            //References(c => c.Question).Column("LessonQuestionId").Nullable().Not.LazyLoad();
        }
    }
}
