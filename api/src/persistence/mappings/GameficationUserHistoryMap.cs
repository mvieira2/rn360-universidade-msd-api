﻿using FluentNHibernate.Mapping;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class GameficationUserHistoryMap : ClassMap<GameficationUserHistory>
    {
        public GameficationUserHistoryMap()
        {
            Id(s => s.Id);
            Map(s => s.Identifier).Not.Nullable();
            Map(s => s.CreatedAt).Not.Nullable();
            Map(s => s.Points).Not.Nullable();

            References(c => c.User).Column("UserId").Nullable().Not.LazyLoad();
            References(c => c.Action).Column("GameficationActionId").Nullable().Not.LazyLoad();
        }
    }
}
