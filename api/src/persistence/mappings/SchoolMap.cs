using FluentNHibernate.Mapping;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class SchoolMap : ClassMap<School>
    {
        public SchoolMap()
        {
            Id(s => s.Id);
            Map(s => s.CreatedAt);
            Map(s => s.Name).Not.Nullable();
            Map(s => s.Photo).Nullable();
            Map(s => s.Identifier).Not.Nullable();
            References(c => c.Segment).Column("SegmentId").Nullable().Not.LazyLoad();
        }
    }
}
