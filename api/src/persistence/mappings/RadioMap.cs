using FluentNHibernate.Mapping;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class RadioMap : ClassMap<Radio>
    {
        public RadioMap()
        {
            Id(s => s.Id);
            Map(s => s.CreatedAt);
            Map(s => s.Identifier).Not.Nullable();
            Map(s => s.Name).Not.Nullable();
            Map(s => s.Description).Length(1000).Nullable();
            Map(s => s.Edition).Nullable();
            Map(s => s.AudioFile).Nullable();
            HasManyToMany(s => s.Schools).Table("RadioSchool")
                                         .ParentKeyColumn("RadioId")
                                         .ChildKeyColumn("SchoolId")
                                         .Cascade.SaveUpdate()
                                         .Not.LazyLoad();
            References(c => c.Segment).Column("SegmentId").Nullable().Not.LazyLoad();
        }
    }
}
