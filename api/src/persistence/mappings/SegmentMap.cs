using FluentNHibernate.Mapping;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Persistence.Mappings
{
    public sealed class SegmentMap : ClassMap<Segment>
    {
        public SegmentMap()
        {
            Id(s => s.Id);
            Map(s => s.CreatedAt);
            Map(s => s.Name).Not.Nullable();
            Map(s => s.Identifier).Not.Nullable();
        }
    }
}
