using NHibernate;
using Microsoft.Extensions.Configuration;
using System.IO;
using System;

namespace UniversidadeMsd.Persistence.Managers
{
    public abstract class SessionManagerBase
    {
        private static ISessionFactory sessionFactory;

        public static ISessionFactory SessionFactory
        {
            get
            {
                if (sessionFactory == null)
                {
                    var config = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json").Build();

                    if (config["AppSettings:Database"] == "SqlServer")
                    {
                        sessionFactory = SqlServerSessionManager.BuildSessionFactory(config.GetConnectionString("SqlServer_UniversidadeMsd"));
                    }
                    else
                    {
                        sessionFactory = MySQLSessionManager.BuildSessionFactory(config.GetConnectionString("MySQL_UniversidadeMsd"));
                    }
                }
                return sessionFactory;
            }
        }
    }
}
