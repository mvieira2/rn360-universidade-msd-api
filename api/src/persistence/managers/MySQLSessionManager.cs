using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using UniversidadeMsd.Persistence.Mappings;

namespace UniversidadeMsd.Persistence.Managers
{
    internal class MySQLSessionManager
    {
        public static ISessionFactory BuildSessionFactory(string connectionString)
        {
            return Fluently.Configure().Database(
                    MySQLConfiguration.Standard
                        .ConnectionString(c => c.Is(connectionString)).ShowSql())
                .ExposeConfiguration(c => c.Properties.Add("hbm2ddl.keywords", "none"))
                .ExposeConfiguration(c => c.Properties.Add("current_session_context_class", "managed_web"))
                .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true))
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<UserMap>())
                .BuildSessionFactory();
        }
    }
}
