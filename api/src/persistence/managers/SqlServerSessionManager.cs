using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using UniversidadeMsd.Persistence.Mappings;

namespace UniversidadeMsd.Persistence.Managers
{
    internal class SqlServerSessionManager
    {
        public static ISessionFactory BuildSessionFactory(string connectionString)
        {
            return Fluently.Configure().Database(
                MsSqlConfiguration.MsSql2012
                    .ShowSql()
                    .ConnectionString(c => c.Is(connectionString))
            )
            .Mappings(m => m.FluentMappings.AddFromAssemblyOf<UserMap>())
            .ExposeConfiguration(c => c.Properties.Add("hbm2ddl.keywords", "none"))
            .ExposeConfiguration(c => c.Properties.Add("current_session_context_class", "managed_web"))
            .BuildSessionFactory();
        }
    }
}
