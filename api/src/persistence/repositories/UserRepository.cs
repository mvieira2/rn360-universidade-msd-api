using System;
using System.Collections.Generic;
using System.Text;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Persistence.Utils;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public new void SaveOrUpdate(User user) {

            if (user.School != null)
                user.School = Load<School>(user.School.Identifier);

            if (user.Segment != null)
                user.Segment = Load<Segment>(user.Segment.Identifier);

            user.Password = Hashing.ToSha1(user.Password);

            base.SaveOrUpdate(user);
        }


        public new bool Update(User oldEntity, User newEntity)
        {
            if (newEntity == null)
                return false;

            oldEntity.Name = newEntity.Name;
            oldEntity.Email = newEntity.Email;
            oldEntity.Phone = newEntity.Phone;
            oldEntity.Profile = newEntity.Profile;
            oldEntity.Segment = Replace(oldEntity.Segment, newEntity.Segment);
            oldEntity.School = newEntity.School == null ? null : Replace(oldEntity.School, newEntity.School);
            oldEntity.Active = newEntity.Active;

            if (!String.IsNullOrEmpty(newEntity.Photo))
                oldEntity.Photo = newEntity.Photo;

            if (!String.IsNullOrWhiteSpace(newEntity.Password))
                oldEntity.Password = Hashing.ToSha1(newEntity.Password);

            base.SaveOrUpdate(oldEntity);
            return true;
        }

        public User Authenticate(IDictionary<String, object> data) {
            data["Password"] = Hashing.ToSha1(data["Password"].ToString());
            return FindOne(data);
        }
    }
}