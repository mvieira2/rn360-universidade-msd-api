﻿using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class LessonExamRepository : BaseRepository<LessonExam>, ILessonExamRepository
    {
        public new void SaveOrUpdate(LessonExam lessonExam)
        {
            lessonExam.Lesson = Load<Lesson>(lessonExam.Lesson.Identifier);
            lessonExam.User = Load<User>(lessonExam.User.Identifier);
            base.SaveOrUpdate(lessonExam);

        }
        public new bool Update(LessonExam oldEntity, LessonExam newEntity)
        {
            if (newEntity == null)
                return true;

            oldEntity.QtyQuestions = newEntity.QtyQuestions;
            oldEntity.QtySuccess = newEntity.QtySuccess;
            oldEntity.QtyWrong = newEntity.QtyWrong;

            oldEntity.Lesson = Load<Lesson>(oldEntity.Lesson.Identifier);
            oldEntity.User = Load<User>(oldEntity.User.Identifier);

            base.SaveOrUpdate(oldEntity);
            return true;
        }
    }
}