﻿using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class LessonQuestionAnswerRepository : BaseRepository<LessonQuestionAnswer>, ILessonQuestionAnswerRepository
    {
        public new void SaveOrUpdate(LessonQuestionAnswer lessonQuestionAnswer)
        {
            base.SaveOrUpdate(lessonQuestionAnswer);

        }
        public new bool Update(LessonQuestionAnswer oldEntity, LessonQuestionAnswer newEntity)
        {
            if (newEntity == null)
                return true;

            oldEntity.Value = newEntity.Value;
            oldEntity.IsRight = newEntity.IsRight;

            base.SaveOrUpdate(oldEntity);
            return true;
        }
    }
}