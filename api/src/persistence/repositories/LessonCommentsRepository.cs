﻿using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class LessonCommentsRepository : BaseRepository<LessonComments>, ILessonCommentsRepository
    {
        public new void SaveOrUpdate(LessonComments lessonComments)
        {
            lessonComments.Lesson = Load<Lesson>(lessonComments.Lesson.Identifier);
            lessonComments.User = Load<User>(lessonComments.User.Identifier);
            base.SaveOrUpdate(lessonComments);

        }
        public new bool Update(LessonComments oldEntity, LessonComments newEntity)
        {
            if (newEntity == null)
                return true;

            oldEntity.Comments = newEntity.Comments;

            if (oldEntity.StatusComment != newEntity.StatusComment)
                oldEntity.StatusComment = newEntity.StatusComment;

            oldEntity.Lesson = Load<Lesson>(oldEntity.Lesson.Identifier);
            oldEntity.User = Load<User>(oldEntity.User.Identifier);

            base.SaveOrUpdate(oldEntity);
            return true;
        }
    }
}