using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class LessonRepository : BaseRepository<Lesson>, ILessonRepository
    {
        public new void SaveOrUpdate(Lesson lesson)
        {
            lesson.Segment = Load<Segment>(lesson.Segment.Identifier);
            lesson.Schools = lesson.Schools == null ? null : LoadList<School>(lesson.Schools);
            base.SaveOrUpdate(lesson);

        }
        public new bool Update(Lesson oldEntity, Lesson newEntity) {
            if (newEntity == null)
                return true;

            oldEntity.Name = newEntity.Name;

            if (oldEntity.Teachers != newEntity.Teachers)
                oldEntity.Teachers = newEntity.Teachers;

            if (oldEntity.Description != newEntity.Description)
                oldEntity.Description = newEntity.Description;

            if (oldEntity.StartedAt != newEntity.StartedAt)
                oldEntity.StartedAt = newEntity.StartedAt;

            if (oldEntity.Link != newEntity.Link)
                oldEntity.Link = newEntity.Link;

            if (oldEntity.Options != newEntity.Options)
                oldEntity.Options = newEntity.Options;

            if (oldEntity.ProfileAccess != newEntity.ProfileAccess)
                oldEntity.ProfileAccess = newEntity.ProfileAccess;
            
            if (oldEntity.Active != newEntity.Active)
                oldEntity.Active = newEntity.Active;

            oldEntity.Segment = Load<Segment>(newEntity.Segment.Identifier);
            oldEntity.Schools = newEntity.Schools == null ? null : LoadList<School>(newEntity.Schools);

            base.SaveOrUpdate(oldEntity);
            return true;
        }
    }
}