﻿using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class LessonQuestionRepository : BaseRepository<LessonQuestion>, ILessonQuestionRepository
    {
        public new void SaveOrUpdate(LessonQuestion lessonQuestion)
        {
            lessonQuestion.Lesson = Load<Lesson>(lessonQuestion.Lesson.Identifier);
            base.SaveOrUpdate(lessonQuestion);

        }
        public new bool Update(LessonQuestion oldEntity, LessonQuestion newEntity)
        {
            if (newEntity == null)
                return true;

            oldEntity.Title = newEntity.Title;
            oldEntity.Answers = newEntity.Answers;
            oldEntity.Lesson = Load<Lesson>(oldEntity.Lesson.Identifier);

            base.SaveOrUpdate(oldEntity);
            return true;
        }
    }
}