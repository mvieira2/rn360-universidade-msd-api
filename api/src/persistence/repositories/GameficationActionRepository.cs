﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class GameficationActionRepository : BaseRepository<GameficationAction>, IGameficationActionRepository
    {
        public new void SaveOrUpdate(GameficationAction GameficationAction)
        {
            base.SaveOrUpdate(GameficationAction);
        }

        public new bool Update(GameficationAction oldEntity, GameficationAction newEntity)
        {
            if (newEntity == null)
                return true;

            oldEntity.ActionName = newEntity.ActionName;
            oldEntity.Points = newEntity.Points;
            oldEntity.UniqueUserPoint = newEntity.UniqueUserPoint;

            base.SaveOrUpdate(oldEntity);
            return true;
        }
    }
}