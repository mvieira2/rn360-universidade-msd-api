using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.ObjectValues;
using UniversidadeMsd.Core.Repositories;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class ProfileRepository : IProfileRepository
    {
        private static IDictionary<String, object> profiles;

        public IDictionary<String, object> GetAll() {
            if (profiles == null) {
                var names = Enum.GetNames(typeof(Profile));
                var values = Enum.GetValues(typeof(Profile));
                profiles = new Dictionary<string, object>();

                for (var i = 0; i < names.Length; i++) {
                    profiles.Add(names[i], values.GetValue(i));
                }
            }
            return profiles;
        }
    }
}