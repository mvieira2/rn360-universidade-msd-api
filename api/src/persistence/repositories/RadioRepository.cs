using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class RadioRepository : BaseRepository<Radio>, IRadioRepository
    {
        public new void SaveOrUpdate(Radio radio)
        {
            radio.Segment = Load<Segment>(radio.Segment.Identifier);
            radio.Schools = radio.Schools == null ? null: LoadList<School>(radio.Schools);
            base.SaveOrUpdate(radio);
        }

        public new bool Update(Radio oldEntity, Radio newEntity) {
            if (newEntity == null)
                return true;

            oldEntity.Name = newEntity.Name;
            oldEntity.Description = newEntity.Description;
            oldEntity.Edition = newEntity.Edition;
            oldEntity.Segment = Load<Segment>(newEntity.Segment.Identifier);
            oldEntity.Schools = newEntity.Schools == null ? null : LoadList<School>(newEntity.Schools);

            base.SaveOrUpdate(oldEntity);
            return true;
        }
    }
}