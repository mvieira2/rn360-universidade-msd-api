using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class NewsRepository : BaseRepository<News>, INewsRepository
    {
        public new void SaveOrUpdate(News news)
        {
            news.Segment = Load<Segment>(news.Segment.Identifier);
            news.Schools = news.Schools == null ? null : LoadList<School>(news.Schools);
            base.SaveOrUpdate(news);
        }

        public new bool Update(News oldEntity, News newEntity) {
            if (newEntity == null)
                return true;

            oldEntity.Title = newEntity.Title;
            oldEntity.Content = newEntity.Content;
            oldEntity.Description = newEntity.Description;
            oldEntity.PublishedAt = newEntity.PublishedAt;
            oldEntity.Photo = newEntity.Photo;
            oldEntity.Segment = Load<Segment>(newEntity.Segment.Identifier);
            oldEntity.Schools = newEntity.Schools == null ? null : LoadList<School>(newEntity.Schools);

            base.SaveOrUpdate(oldEntity);
            return true;
        }
    }
}