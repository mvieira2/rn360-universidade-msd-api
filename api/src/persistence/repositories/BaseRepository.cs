using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using UniversidadeMsd.Persistence.Managers;
using UniversidadeMsd.Core.Repositories;

using Order = UniversidadeMsd.Core.Repositories.Order;
using UniversidadeMsd.Core.Models;
using System;
using UniversidadeMsd.Persistence.Exceptions;
using System.Text;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : Entity
    {
        private ISession session;

        protected ISession Session
        {
            get
            {
                if (session == null)
                    session = SessionManagerBase.SessionFactory.OpenSession();
                return session;
            }
        }

        public int Count { get { return Session.QueryOver<T>().RowCount(); } }

        public void SaveOrUpdate(T entity)
        {
            try {
                if (session == null)
                    session = SessionManagerBase.SessionFactory.OpenSession();

                Session.SaveOrUpdate(entity);
                Session.Flush();
            }
            catch (Exception ex)
            {
                session = null;
                var innerException = ex.InnerException;
                if (innerException.Message.Contains("UX_"))
                    throw new DuplicatedConstraintException(innerException.Message);
                throw ex;
            }
        }

        public bool Insert(T entity)
        {
            Session.Save(entity);
            return true;
        }

        public bool Update(T entity)
        {
            Session.Update(entity);
            return true;
        }

        public bool Update(T oldEntity, T newEntity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(T entity)
        {
            Session.Delete(entity);
            Session.Flush();
            return true;
        }

        public bool Delete(String identifier)
        {
            var entity = FindOne(new { Identifier = identifier});
            return Delete(entity);
        }

        public T Get(int id)
        {
            return Session.Get<T>(id);
        }

        public IList<T> GetAll(params Order[] orders)
        {
            ICriteria criteria = AddRestrictions(null, orders);
            return criteria.List<T>();
        }

        public IList<T> GetAll(int? offset, int? limit, params Order[] orders)
        {
            ICriteria criteria = AddRestrictions(null, orders);
            if (offset.HasValue)
                criteria.SetFirstResult(offset.Value);
            if (limit.HasValue)
                criteria.SetMaxResults(limit.Value);
            return criteria.List<T>();
        }

        public T FindOne(object properties)
        {
            ICriteria criteria = AddRestrictions(properties);
            return criteria.UniqueResult<T>();
        }

        public IList<T> FindAll(object properties, params Order[] orders)
        {
            return FindAll(properties, null, null, orders);
        }

        public IList<T> FindAll(object properties, int? offset, int? limit, params Order[] orders)
        {
            ICriteria criteria = AddRestrictions(properties, orders);
            if (offset.HasValue)
                criteria.SetFirstResult(offset.Value);
            if (limit.HasValue)
                criteria.SetMaxResults(limit.Value);
            return criteria.List<T>();
        }

        public int FindCount(object properties) {
            ICriteria criteria = AddRestrictions(properties);
            criteria.SetProjection(Projections.Count("Id"));
            return criteria.UniqueResult<int>();
        }

        public IList<TU> RawSql<TU>(string query)
        {
            return Session.CreateSQLQuery(query).SetResultTransformer(Transformers.AliasToBean(typeof(TU))).List<TU>();
        }

        protected TU Load<TU>(String identifier) {
            var entity = Session.CreateCriteria(typeof(TU))
                                .Add(Restrictions.Eq("Identifier", identifier))
                                .UniqueResult<TU>();
            if (entity == null) {
                throw new EntityNotFoundException(typeof(TU));
            }
            return entity;
        }

        protected IList<TU> LoadList<TU>(IList<TU> identifiers) where TU : Entity {
            var entities = Session.CreateCriteria(typeof(TU))
                                  .Add(Restrictions.In("Identifier", identifiers.Select(s => s.Identifier).ToArray()))
                                .List<TU>();
            if (entities.Count == 0) {
                throw new EntityNotFoundException(typeof(TU));
            }
            return entities;
        }

        protected TU Replace<TU>(TU oldEntity, TU newEntity) where TU : Entity {
            if (newEntity == null) return null;
            if (!newEntity.Equals(oldEntity)) return Load<TU>(newEntity.Identifier);
            return oldEntity;
        }

        private ICriteria AddRestrictions(object properties, params Order[] orders)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(T));
            if (properties != null) {
                IDictionary<String, object> parameters;
                if (properties is IDictionary<String, object>)
                {
                    parameters = (IDictionary<String, object>)properties;
                }
                else
                {
                    parameters = properties.GetType().GetProperties()
                            .ToDictionary(x => x.Name, x => x.GetValue(properties, null));
                }
                foreach (var property in parameters)
                {
                    criteria.Add(property.Value != null
                        ? Restrictions.Eq(property.Key, property.Value)
                        : Restrictions.IsNull(property.Key));
                }
            }
            for (var i = 0; i < orders.Length; i++)
               criteria.AddOrder(new NHibernate.Criterion.Order(orders[i].Column, (orders[i].Direction == OrderDirection.Ascending)));
            return criteria;
        }
    }
}
