using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class AlertRepository : BaseRepository<Alert>, IAlertRepository
    {
        public new void SaveOrUpdate(Alert alert)
        {
            alert.Segment = Load<Segment>(alert.Segment.Identifier);
            alert.Schools = alert.Schools == null ? null : LoadList<School>(alert.Schools);
            base.SaveOrUpdate(alert);
        }

        public new bool Update(Alert oldEntity, Alert newEntity) {
            if (newEntity == null)
                return true;

            oldEntity.Name = newEntity.Name;
            oldEntity.Segment = Load<Segment>(newEntity.Segment.Identifier);
            oldEntity.Schools = newEntity.Schools == null ? null : LoadList<School>(newEntity.Schools);
            if (oldEntity.Description != newEntity.Description)
                oldEntity.Description = newEntity.Description;

            base.SaveOrUpdate(oldEntity);
            return true;
        }
    }
}