﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class GameficationUserHistoryRepository : BaseRepository<GameficationUserHistory>, IGameficationUserHistoryRepository
    {
        public new void SaveOrUpdate(GameficationUserHistory GameficationUserHistory)
        {
            GameficationUserHistory.User = Load<User>(GameficationUserHistory.User.Identifier);
            GameficationUserHistory.Action = Load<GameficationAction>(GameficationUserHistory.Action.Identifier);
            base.SaveOrUpdate(GameficationUserHistory);
        }

        public new bool Update(GameficationUserHistory oldEntity, GameficationUserHistory newEntity)
        {
            if (newEntity == null)
                return true;

            oldEntity.Points = newEntity.Points;
            oldEntity.User = Load<User>(newEntity.User.Identifier);
            oldEntity.Action = Load<GameficationAction>(newEntity.Action.Identifier);

            base.SaveOrUpdate(oldEntity);
            return true;
        }
    }
}