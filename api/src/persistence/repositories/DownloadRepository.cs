using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class DownloadRepository : BaseRepository<Download>, IDownloadRepository
    {
        public new void SaveOrUpdate(Download download)
        {
            download.Segment = Load<Segment>(download.Segment.Identifier);
            download.Schools = download.Schools == null ? null :  LoadList<School>(download.Schools);
            base.SaveOrUpdate(download);
        }

        public new bool Update(Download oldEntity, Download newEntity) {
            if (newEntity == null)
                return true;

            oldEntity.Name = newEntity.Name;
            oldEntity.Description = newEntity.Description;
            oldEntity.Segment = Load<Segment>(newEntity.Segment.Identifier);
            oldEntity.Schools = newEntity.Schools == null ? null : LoadList<School>(newEntity.Schools);

            base.SaveOrUpdate(oldEntity);
            return true;
        }
    }
}