using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class SchoolRepository : BaseRepository<School>, ISchoolRepository
    {
        public new void SaveOrUpdate(School entity) {
            entity.Segment = Load<Segment>(entity.Segment.Identifier);
            base.SaveOrUpdate(entity);
        }

        public new bool Update(School oldEntity, School newEntity) {
            if (newEntity == null)
                return false;

            oldEntity.Name = newEntity.Name;
            oldEntity.Segment = Load<Segment>(newEntity.Segment.Identifier);

            if (newEntity.Photo != null) {
                oldEntity.Photo = newEntity.Photo;
            }

            base.SaveOrUpdate(oldEntity);
            return true;
        }
    }
}