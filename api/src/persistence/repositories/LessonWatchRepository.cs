﻿using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;

namespace UniversidadeMsd.Persistence.Repositories
{
    public class LessonWatchRepository : BaseRepository<LessonWatch>, ILessonWatchRepository
    {
        public new void SaveOrUpdate(LessonWatch lessonWatch)
        {
            lessonWatch.Lesson = Load<Lesson>(lessonWatch.Lesson.Identifier);
            lessonWatch.User = Load<User>(lessonWatch.User.Identifier);
            base.SaveOrUpdate(lessonWatch);

        }
        public new bool Update(LessonWatch oldEntity, LessonWatch newEntity)
        {
            if (newEntity == null)
                return true;

            oldEntity.Lesson = Load<Lesson>(oldEntity.Lesson.Identifier);
            oldEntity.User = Load<User>(oldEntity.User.Identifier);

            base.SaveOrUpdate(oldEntity);
            return true;
        }
    }
}