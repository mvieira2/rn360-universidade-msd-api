﻿using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Forms
{
    public class AddLessonQuestionForm : BaseForm
    {
        public String Title { get; set; }
        public String Lesson { get; set; }
        public IList<LessonQuestionAnswer> Answers { get; set; }

        protected override void Validate()
        {
            if (String.IsNullOrWhiteSpace(Title))
            {
                this.AppendError("Title", "Title is required.");
            }
        }

        public LessonQuestion Data
        {
            get
            {
                if (IsValid)
                {
                    return new LessonQuestion
                    {
                        CreatedAt = DateTime.UtcNow,
                        Title = Title,
                        Identifier = Uuid.NewUuid(),
                        Lesson = LoadEntity<Lesson>(Lesson),
                        Answers = Answers
                    };
                }
                return null;
            }
        }
    }
}