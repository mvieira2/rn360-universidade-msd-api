using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class ForgetEmailForm : BaseForm
    {
        public String Email { get; set; }

        protected override void Validate()
        {
            if (String.IsNullOrWhiteSpace(Email)) {
                this.AppendError("email", "Email is required.");
            }

            if (!EmailValidator.Validate(Email)) {
                this.AppendError("email", "Email is not valid.");
            }
        }
    }
}