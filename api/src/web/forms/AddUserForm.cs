using System;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

using EnumProfile = UniversidadeMsd.Core.ObjectValues.Profile;

namespace UniversidadeMsd.Web.Forms
{
    public class AddUserForm : BaseForm, IUploadableForm
    {
        public String Name { get; set; }
        public String Email { get; set; }
        public String Phone { get; set; }
        public String Photo { get; set; }
        public String School { get; set; }
        public String Segment { get; set; }
        public int Profile { get; set; }
        public bool Active { get; set; }

        public Boolean HasFile { get { return !String.IsNullOrEmpty(Photo); } }

        protected override void Validate()
        {
            if (String.IsNullOrWhiteSpace(Name)) {
                this.AppendError("name", "Name is required.");
            }

            if (String.IsNullOrWhiteSpace(Email)) {
                this.AppendError("email", "Email is required.");
            }

            if (!EmailValidator.Validate(Email)) {
                this.AppendError("email", "Email is not valid.");
            }

            if (String.IsNullOrWhiteSpace(Phone)) {
                this.AppendError("phone", "Phone is required.");
            }

            if (!Enum.IsDefined(typeof(EnumProfile), Profile)) {
                this.AppendError("profile", "Profile is invalid.");
            }
            else {
                ValidateProfile((EnumProfile)Profile);
            }
        }

        private void ValidateProfile(EnumProfile profile) {
            if (profile == EnumProfile.Administrator) {
                School = null;
                Segment = null;
            }
            else {
                if (String.IsNullOrWhiteSpace(Segment)) {
                    this.AppendError("segment", "Segment is required.");
                }
            }
        }

        public User Data {
            get {
                if (IsValid) {
                    return new User
                    {
                        CreatedAt = DateTime.UtcNow,
                        Name = Name,
                        Email = Email,
                        Phone = Phone,
                        Identifier = Uuid.NewUuid(),
                        Profile = (EnumProfile)Profile,
                        School = School == null ? null : LoadEntity<School>(School),
                        Segment = LoadEntity<Segment>(Segment),
                        Active = Active
                    };
                }
                return null;
            }
        }
    }
}