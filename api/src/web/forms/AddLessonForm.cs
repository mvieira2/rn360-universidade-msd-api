using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.ObjectValues;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;


namespace UniversidadeMsd.Web.Forms
{
    public class AddLessonForm : BaseForm
    {
        public String Name { get; set; }
        public String Teachers { get; set; }
        public String Description { get; set; }
        public DateTime? StartedAt { get; set; }
        public String Link { get; set; }
        public int? Options { get; set; }
        public IList<int> ProfileAccess { get; set; }
        public String Segment { get; set; }
        public IList<String> Schools { get; set; }
        public bool Active { get; set; }

        protected override void Validate()
        {
            if (String.IsNullOrWhiteSpace(Name)) {
                this.AppendError("name", "Name is required.");
            }

            if (Options.HasValue && !Enum.IsDefined(typeof(LessonOptions), Options)) {
                this.AppendError("options", "Options is invalid.");
            }

            if (String.IsNullOrWhiteSpace(Segment)) {
                this.AppendError("segment", "Segment is required.");
            }


            if (ProfileAccess != null && ProfileAccess.Any(p => !Enum.IsDefined(typeof(Profile), p))) {
                this.AppendError("profileAccess", "ProfileAccess has invalid value.");
            }
        }

        public Lesson Data {
            get {
                if (IsValid) {
                    int[] arrProfileAccess = new int[ProfileAccess.Count];
                    if (ProfileAccess != null)
                    {
                        int i = 0;
                        foreach (var profile in ProfileAccess)
                        {
                            arrProfileAccess[i] = Convert.ToInt32(profile.ToString());
                            i++;
                        }
                    }

                    return new Lesson
                    {
                        CreatedAt = DateTime.UtcNow,
                        Name = Name,
                        Identifier = Uuid.NewUuid(),
                        Description = Description,
                        StartedAt = StartedAt,
                        Link = Link,
                        Teachers = Teachers,
                        Segment = LoadEntity<Segment>(Segment),
                        ProfileAccess = JsonConvert.SerializeObject(arrProfileAccess),
                        Active = Active,
                        Options = Options.HasValue
                                ? (LessonOptions)Options.Value : LessonOptions.None,
                        Schools = Schools == null || Schools.Count == 0
                                ? null : Schools.Select(s => new School { Identifier = s}).ToList()
                    };
                }
                return null;
            }
        }
    }
}