﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class AddGameficationUserHistoryForm : BaseForm
    {
        public int Points { get; set; }
        public String User{ get; set; }
        public String Action { get; set; }

        protected override void Validate()
        {
        }

        public GameficationUserHistory Data
        {
            get
            {
                if (IsValid)
                {
                    return new GameficationUserHistory
                    {
                        CreatedAt = DateTime.UtcNow,
                        Identifier = Uuid.NewUuid(),
                        Points = Points,
                        User = LoadEntity<User>(User),
                        Action = LoadEntity<GameficationAction>(Action)
                    };
                }
                return null;
            }
        }
    }
}