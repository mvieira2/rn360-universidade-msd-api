﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class AddGameficationActionForm : BaseForm
    {
        public String ActionName { get; set; }
        public int Points { get; set; }
        public Boolean UniqueUserPoint { get; set; }

        protected override void Validate()
        {
            if (String.IsNullOrWhiteSpace(ActionName))
            {
                this.AppendError("ActionName", "ActionName is required.");
            }
        }

        public GameficationAction Data
        {
            get
            {
                if (IsValid)
                {
                    return new GameficationAction
                    {
                        CreatedAt = DateTime.UtcNow,
                        ActionName = ActionName,
                        Identifier = Uuid.NewUuid(),
                        Points = Points,
                        UniqueUserPoint = UniqueUserPoint
                    };
                }
                return null;
            }
        }
    }
}