using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.ObjectValues;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class EditLessonForm : AddLessonForm
    {
        public new Lesson Data
        {
            get {
                if (IsValid) {
                    int[] arrProfileAccess = new int[ProfileAccess.Count];
                    if (ProfileAccess != null) {
                        int i = 0;
                        foreach (var profile in ProfileAccess)
                        {
                            arrProfileAccess[i] = Convert.ToInt32(profile.ToString());
                            i++;
                        }
                    }

                    return new Lesson
                    {
                        Name = Name,
                        Description = Description,
                        StartedAt = StartedAt,
                        Link = Link,
                        Teachers = Teachers,
                        Segment = LoadEntity<Segment>(Segment),
                        ProfileAccess = JsonConvert.SerializeObject(arrProfileAccess),
                        Active = Active,
                        Options = Options.HasValue
                                ? (LessonOptions)Options.Value : LessonOptions.None,
                        Schools = Schools == null || Schools.Count == 0
                                ? null : Schools.Select(s => new School { Identifier = s}).ToList()
                    };
                }
                return null;
            }
        }
    }
}