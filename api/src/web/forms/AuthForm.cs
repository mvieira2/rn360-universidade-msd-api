using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Persistence.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class AuthForm : BaseForm
    {
        public String Email { get; set; }
        public String Password { get; set; }

        protected override void Validate()
        {
            if (String.IsNullOrWhiteSpace(Email)) {
                this.AppendError("email", "Email is required.");
            }

            if (String.IsNullOrWhiteSpace(Password)) {
                this.AppendError("password", "Password is required.");
            }
        }

        public IDictionary<String, object> Data {
            get {
                if (IsValid) {
                    return new Dictionary<String, object> {
                        { "Email",  Email },
                        { "Password",  Password }
                    };
                }
                return null;
            }
        }
    }
}