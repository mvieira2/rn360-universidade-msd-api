using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Web.Forms
{
    public abstract class BaseForm
    {
        private IDictionary<String, IList<String>> _errors = new SortedDictionary<String, IList<String>>();
        private Boolean? _isValid = null;

        protected void AppendError(string field, string message)
        {
            if (!this._errors.ContainsKey(field))
                this._errors.Add(field, new List<String>());
            this._errors[field].Add(message);
        }

        protected abstract void Validate();

        protected void Clean() { }

        protected TU LoadEntity<TU>(String identifier) where TU : Entity {
            if (identifier == null) return null;

            var entity = (TU)Activator.CreateInstance(typeof(TU));
            entity.Identifier = identifier;
            return entity;
        }

        public Boolean IsValid
        {
            get
            {
                if (!_isValid.HasValue) {
                    Clean();
                    Validate();
                    _isValid = _errors.Count == 0 ? true : false;
                }
                return _isValid.Value;
            }
        }

        public IDictionary<String, IList<String>> Errors { get { return _errors; } }
    }
}
