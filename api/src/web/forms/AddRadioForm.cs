using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class AddRadioForm : BaseForm, IUploadableForm
    {
        public String Name { get; set; }
        public String Description { get; set; }
        public String Edition { get; set; }
        public String AudioFile { get; set; }
        public String Segment { get; set; }
        public IList<String> Schools { get; set; }

        protected override void Validate()
        {
            if (String.IsNullOrWhiteSpace(Name)) {
                this.AppendError("name", "Title is required.");
            }

            if (String.IsNullOrWhiteSpace(Segment)) {
                this.AppendError("segment", "Segment is required.");
            }
        }

        public Radio Data {
            get {
                if (IsValid) {
                    return new Radio
                    {
                        CreatedAt = DateTime.UtcNow,
                        Identifier = Uuid.NewUuid(),
                        Name = Name,
                        Edition = Edition,
                        Description = Description,
                        Segment = LoadEntity<Segment>(Segment),
                        Schools = Schools == null || Schools.Count == 0
                                ? null : Schools.Select(s => new School { Identifier = s }).ToList()
                    };
                }
                return null;
            }
        }

        public bool HasFile => !String.IsNullOrEmpty(AudioFile);
    }
}