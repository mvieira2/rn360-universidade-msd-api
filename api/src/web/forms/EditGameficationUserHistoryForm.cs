﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class EditGameficationUserHistoryForm : AddGameficationUserHistoryForm
    {
        protected override void Validate()
        {
        }

        public new GameficationUserHistory Data
        {
            get
            {
                if (IsValid)
                {
                    return new GameficationUserHistory
                    {
                        Points = Points,
                        User = LoadEntity<User>(User),
                        Action = LoadEntity<GameficationAction>(Action)
                    };
                }
                return null;
            }
        }
    }
}