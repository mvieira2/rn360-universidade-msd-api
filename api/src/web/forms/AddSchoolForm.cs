using System;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class AddSchoolForm : BaseForm, IUploadableForm
    {
        public String Name { get; set; }
        public String Photo { get; set; }
        public String Segment { get; set; }


        protected override void Validate()
        {
            if (String.IsNullOrWhiteSpace(Name)) {
                this.AppendError("name", "Name is required.");
            }

            if (String.IsNullOrWhiteSpace(Segment)) {
                this.AppendError("segment", "Segment is required.");
            }
        }

        public School Data {
            get {
                if (IsValid) {
                    return new School
                    {
                        CreatedAt = DateTime.UtcNow,
                        Identifier = Uuid.NewUuid(),
                        Name = Name,
                        Segment = LoadEntity<Segment>(Segment)
                    };
                }
                return null;
            }
        }

        public bool HasFile => !String.IsNullOrEmpty(Photo);
    }
}