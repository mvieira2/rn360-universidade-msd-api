﻿using System;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Forms
{
    public class EditLessonExamForm : AddLessonExamForm
    {
        public new LessonExam Data
        {
            get
            {
                if (IsValid)
                {
                    return new LessonExam
                    {
                        QtyQuestions = QtyQuestions,
                        QtySuccess = QtySuccess,
                        QtyWrong = QtyWrong,
                        Lesson = LoadEntity<Lesson>(Lesson),
                        User = LoadEntity<User>(User),
                        Details = Details
                    };
                }
                return null;
            }
        }
    }
}