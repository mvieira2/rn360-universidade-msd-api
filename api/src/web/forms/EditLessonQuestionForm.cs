﻿using System;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Forms
{
    public class EditLessonQuestionForm : AddLessonQuestionForm
    {
        public new LessonQuestion Data
        {
            get
            {
                if (IsValid)
                {
                    return new LessonQuestion
                    {
                        CreatedAt = DateTime.UtcNow,
                        Title = Title,
                        Identifier = Uuid.NewUuid(),
                        Lesson = LoadEntity<Lesson>(Lesson),
                        Answers = Answers
                    };
                }
                return null;
            }
        }
    }
}