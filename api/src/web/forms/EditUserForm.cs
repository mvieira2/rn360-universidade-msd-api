using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.ObjectValues;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class EditUserForm : AddUserForm
    {
        public String Password { get; set; }

        public new User Data
        {
            get {
                if (IsValid) {
                    return new User
                    {
                        Name = Name,
                        Email = Email,
                        Phone = Phone,
                        Profile = (Profile)Profile,
                        Password = Password,
                        Photo = Photo,
                        School = School == null ? null : LoadEntity<School>(School),
                        Segment = LoadEntity<Segment>(Segment),
                        Active = Active
                    };
                }
                return null;
            }
        }
    }
}