using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class EditNewsForm : AddNewsForm
    {
        public new News Data {
            get {
                if (IsValid) {
                    return new News
                    {
                        PublishedAt = PublishedAt,
                        Title = Title,
                        Content = Content,
                        Description = Description,
                        Segment = LoadEntity<Segment>(Segment),
                        Photo = Photo,
                        Schools = Schools == null || Schools.Count == 0
                                ? null : Schools.Select(s => new School { Identifier = s }).ToList()
                    };
                }
                return null;
            }
        }
    }
}