using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class AddDownloadForm : BaseForm, IUploadableForm
    {
        public String Name { get; set; }
        public String Description { get; set; }
        public String DownloadFile { get; set; }
        public String Segment { get; set; }
        public IList<String> Schools { get; set; }

        protected override void Validate()
        {
            if (String.IsNullOrWhiteSpace(Name)) {
                this.AppendError("name", "Title is required.");
            }

            if (String.IsNullOrWhiteSpace(DownloadFile)) {
                this.AppendError("downloadFile", "DownloadFile is required.");
            }

            if (String.IsNullOrWhiteSpace(Segment)) {
                this.AppendError("segment", "Segment is required.");
            }
        }

        public Download Data {
            get {
                if (IsValid) {
                    return new Download
                    {
                        CreatedAt = DateTime.UtcNow,
                        Identifier = Uuid.NewUuid(),
                        Name = Name,
                        DownloadFile = DownloadFile,
                        Description = Description,
                        Segment = LoadEntity<Segment>(Segment),
                        Schools = Schools == null || Schools.Count == 0
                                ? null : Schools.Select(s => new School { Identifier = s }).ToList()
                    };
                }
                return null;
            }
        }

        public bool HasFile => !String.IsNullOrWhiteSpace(DownloadFile);
    }
}