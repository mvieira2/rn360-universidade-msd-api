﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class NewPasswordForm : BaseForm
    {
        public String HashNewPassword { get; set; }
        public String Password { get; set; }
        public String ConfirmPassword { get; set; }

        protected override void Validate()
        {
            if (String.IsNullOrWhiteSpace(HashNewPassword))
            {
                this.AppendError("HashNewPassword", "HashNewPassword is required.");
            }

            if (String.IsNullOrWhiteSpace(Password))
            {
                this.AppendError("Password", "Password is required.");
            }

            if (String.IsNullOrWhiteSpace(ConfirmPassword))
            {
                this.AppendError("ConfirmPassword", "ConfirmPassword is required.");
            }

            if (!Password.Equals(ConfirmPassword))
            {
                this.AppendError("ConfirmPassword", "Password and Confirm Password invalid.");
            }
        }
    }
}