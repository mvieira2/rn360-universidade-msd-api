namespace UniversidadeMsd.Web.Forms
{
    public class PaginationForm
    {
        public int? Offset { get; set; }
        public int? Limit { get; set; }

        public bool IsPaginated {
            get {
                return Offset.HasValue || Limit.HasValue;
            }
        }
    }
}