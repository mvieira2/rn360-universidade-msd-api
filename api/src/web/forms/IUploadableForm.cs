using System;

namespace UniversidadeMsd.Web.Forms {

   public interface IUploadableForm
   {
        Boolean HasFile { get; }
   }
}