using System;
using System.Collections.Generic;

namespace UniversidadeMsd.Web.Forms
{
    public class SearchForm : PaginationForm
    {
        private IDictionary<String, object> terms;

        public string Query { get; set; }

        public IDictionary<String, object> Terms {
            get {
                if (terms == null) {
                    terms = new Dictionary<String, object>();
                    try {
                        var elements = Query.Split(",");
                        foreach (var el in elements) {
                            var input = el.Split(":");
                            terms.Add(input[0], input[1]);
                        }
                    } catch (Exception ex) { Console.Write(ex.Message); }
                }
                return terms;
            }
        }
    }
}