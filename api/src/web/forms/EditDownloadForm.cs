using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class EditDownloadForm : AddDownloadForm
    {
        protected override void Validate() {

            if (String.IsNullOrWhiteSpace(Name)) {
                this.AppendError("name", "Title is required.");
            }

            if (String.IsNullOrWhiteSpace(Segment)) {
                this.AppendError("segment", "Segment is required.");
            }
        }

        public new Download Data {
            get {
                if (IsValid) {
                    return new Download
                    {
                        Name = Name,
                        DownloadFile = DownloadFile,
                        Description = Description,
                        Segment = LoadEntity<Segment>(Segment),
                        Schools = Schools == null || Schools.Count == 0
                                ? null : Schools.Select(s => new School { Identifier = s }).ToList()
                    };
                }
                return null;
            }
        }
    }
}