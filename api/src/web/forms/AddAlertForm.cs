using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class AddAlertForm : BaseForm
    {
        public String Name { get; set; }
        public String Description { get; set; }
        public String Segment { get; set; }
        public IList<String> Schools { get; set; }

        protected override void Validate()
        {
            if (String.IsNullOrWhiteSpace(Name)) {
                this.AppendError("name", "Name is required.");
            }

            if (String.IsNullOrWhiteSpace(Segment)) {
                this.AppendError("segment", "Segment is required.");
            }
        }

        public Alert Data {
            get {
                if (IsValid) {
                    return new Alert
                    {
                        CreatedAt = DateTime.UtcNow,
                        Name = Name,
                        Identifier = Uuid.NewUuid(),
                        Description = Description,
                        Segment = LoadEntity<Segment>(Segment),
                        Schools = Schools == null || Schools.Count == 0 ? null : Schools.Select(s => new School { Identifier = s}).ToList()
                    };
                }
                return null;
            }
        }
    }
}