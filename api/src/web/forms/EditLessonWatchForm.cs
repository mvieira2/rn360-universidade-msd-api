﻿using System;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Forms
{
    public class EditLessonWatchForm : AddLessonWatchForm
    {
        public new LessonWatch Data
        {
            get
            {
                if (IsValid)
                {
                    return new LessonWatch
                    {
                        Lesson = LoadEntity<Lesson>(Lesson),
                        User = LoadEntity<User>(User)                    };
                }
                return null;
            }
        }
    }
}