﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class EditGameficationActionForm : AddGameficationActionForm
    {
        protected override void Validate()
        {

            if (String.IsNullOrWhiteSpace(ActionName))
            {
                this.AppendError("ActionName", "ActionName is required.");
            }
        }

        public new GameficationAction Data
        {
            get
            {
                if (IsValid)
                {
                    return new GameficationAction
                    {
                        ActionName = ActionName,
                        Points = Points,
                        UniqueUserPoint = UniqueUserPoint
                    };
                }
                return null;
            }
        }
    }
}