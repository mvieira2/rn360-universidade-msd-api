﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniversidadeMsd.Web.Forms;

namespace UniversidadeMsd.Web.Forms
{
    public class UploadFileForm : BaseForm, IUploadableForm
    {
        public String File { get; set; }

        public Boolean HasFile { get { return !String.IsNullOrEmpty(File); } }


        protected override void Validate()
        {
        }
    }
}
