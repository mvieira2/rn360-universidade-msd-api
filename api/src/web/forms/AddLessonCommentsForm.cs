﻿using System;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Forms
{
    public class AddLessonCommentsForm : BaseForm
    {
        public String Comments { get; set; }
        public String Lesson { get; set; }
        public String User { get; set; }
        public int StatusComment { get; set; }

        protected override void Validate()
        {
            if (String.IsNullOrWhiteSpace(Comments))
            {
                this.AppendError("comments", "Comments is required.");
            }
        }

        public LessonComments Data
        {
            get
            {
                if (IsValid)
                {
                    return new LessonComments
                    {
                        CreatedAt = DateTime.UtcNow,
                        Comments = Comments,
                        Identifier = Uuid.NewUuid(),
                        StatusComment = StatusComment,
                        Lesson = LoadEntity<Lesson>(Lesson),
                        User = LoadEntity<User>(User)
                    };
                }
                return null;
            }
        }
    }
}