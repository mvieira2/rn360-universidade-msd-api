﻿using System;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Forms
{
    public class EditLessonCommentsForm : AddLessonCommentsForm
    {
        public new LessonComments Data
        {
            get
            {
                if (IsValid)
                {
                    return new LessonComments
                    {
                        CreatedAt = DateTime.UtcNow,
                        Comments = Comments,
                        Identifier = Uuid.NewUuid(),
                        StatusComment = StatusComment,
                        Lesson = LoadEntity<Lesson>(Lesson),
                        User = LoadEntity<User>(User)
                    };
                }
                return null;
            }
        }
    }
}