﻿using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Forms
{
    public class AddLessonExamForm : BaseForm
    {
        public String Lesson { get; set; }
        public String User { get; set; }
        public int QtyQuestions { get; set; }
        public int QtySuccess { get; set; }
        public int QtyWrong { get; set; }
        public IList<LessonExamDetail> Details { get; set; }


        protected override void Validate()
        {
        }

        public LessonExam Data
        {
            get
            {
                if (IsValid)
                {
                    return new LessonExam
                    {
                        CreatedAt = DateTime.UtcNow,
                        Identifier = Uuid.NewUuid(),
                        QtyQuestions = 0,
                        QtySuccess = 0,
                        QtyWrong = 0,
                        Lesson = LoadEntity<Lesson>(Lesson),
                        User = LoadEntity<User>(User),
                        Details = Details
                    };
                }
                return null;
            }
        }
    }
}