using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class AddNewsForm : BaseForm
    {
        public String Title { get; set; }
        public String Description { get; set; }
        public String Content { get; set; }
        public DateTime PublishedAt { get; set; }
        public String Segment { get; set; }
        public IList<String> Schools { get; set; }
        public String Photo { get; set; }

        protected new void Clean()
        {
            if (PublishedAt == null)
                PublishedAt = DateTime.UtcNow;
        }

        protected override void Validate()
        {
            if (String.IsNullOrWhiteSpace(Title)) {
                this.AppendError("title", "Title is required.");
            }

            if (String.IsNullOrWhiteSpace(Content)) {
                this.AppendError("content", "Content is required.");
            }

            if (String.IsNullOrWhiteSpace(Segment)) {
                this.AppendError("segment", "Segment is required.");
            }
        }

        public News Data {
            get {
                if (IsValid) {
                    return new News
                    {
                        CreatedAt = DateTime.UtcNow,
                        PublishedAt = PublishedAt,
                        Identifier = Uuid.NewUuid(),
                        Title = Title,
                        Content = Content,
                        Description = Description,
                        Segment = LoadEntity<Segment>(Segment),
                        Photo = Photo,
                        Schools = Schools == null || Schools.Count == 0
                                ? null : Schools.Select(s => new School { Identifier = s }).ToList()
                    };
                }
                return null;
            }
        }
    }
}