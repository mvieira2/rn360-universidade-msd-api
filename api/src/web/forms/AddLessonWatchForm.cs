﻿using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Forms
{
    public class AddLessonWatchForm : BaseForm
    {
        public String Lesson { get; set; }
        public String User { get; set; }

        protected override void Validate()
        {
        }

        public LessonWatch Data
        {
            get
            {
                if (IsValid)
                {
                    return new LessonWatch
                    {
                        CreatedAt = DateTime.UtcNow,
                        Identifier = Uuid.NewUuid(),
                        Lesson = LoadEntity<Lesson>(Lesson),
                        User = LoadEntity<User>(User)                    };
                }
                return null;
            }
        }
    }
}