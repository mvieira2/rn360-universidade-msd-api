using System;
using System.Collections.Generic;
using System.Linq;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

namespace UniversidadeMsd.Web.Forms
{
    public class EditRadioForm : AddRadioForm
    {
        public new Radio Data {
            get {
                if (IsValid) {
                    return new Radio
                    {
                        Name = Name,
                        Edition = Edition,
                        Description = Description,
                        Segment = LoadEntity<Segment>(Segment),
                        Schools = Schools == null || Schools.Count == 0
                                ? null : Schools.Select(s => new School { Identifier = s }).ToList()
                    };
                }
                return null;
            }
        }
    }
}