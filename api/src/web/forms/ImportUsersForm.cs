﻿using System;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Web.Validators;

using EnumProfile = UniversidadeMsd.Core.ObjectValues.Profile;

namespace UniversidadeMsd.Web.Forms
{
    public class ImportUsersForm : BaseForm, IUploadableForm
    {
        public String File { get; set; }

        public Boolean HasFile { get { return !String.IsNullOrEmpty(File); } }

        protected override void Validate()
        {
            
        }
    }
}