﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/lessonWatch")]
    public class LessonWatchController : Controller
    {
        ILessonWatchRepository lessonWatchRepository;
        ILessonQuestionRepository lessonQuestionRepository;
        ILessonQuestionAnswerRepository lessonQuestionAnswerRepository;
        IUserRepository userRepository;
        ILessonRepository lessonRepository;

        public LessonWatchController(ILessonWatchRepository lessonWatchRepository, ILessonQuestionRepository lessonQuestionRepository, ILessonQuestionAnswerRepository lessonQuestionAnswerRepository, IUserRepository userRepository, ILessonRepository lessonRepository)
        {
            this.lessonWatchRepository = lessonWatchRepository;
            this.lessonQuestionRepository = lessonQuestionRepository;
            this.lessonQuestionAnswerRepository = lessonQuestionAnswerRepository;
            this.userRepository = userRepository;
            this.lessonRepository = lessonRepository;
        }

        [HttpGet]
        public JsonResult List(SearchForm form)
        {
            return JsonBase.ResponsePaginated(
                lessonWatchRepository.FindAll(form.Terms, form.Offset, form.Limit),
                form.Offset, form.Limit,
                lessonWatchRepository.FindCount(form.Terms)
            );
        }

        [HttpPost]
        public JsonResult Add([FromBody] AddLessonWatchForm form)
        {
            if (!form.IsValid)
            {
                return JsonBase.ResponseError(form.Errors);
            }

            try
            {
                var lessonWatch = form.Data;
                lessonWatchRepository.SaveOrUpdate(lessonWatch);
                return JsonBase.ResponseObject(lessonWatch);
            }
            catch (EntityNotFoundException ex)
            {
                return JsonBase.ResponseError(ex.EntityName, ex.Message);
            }
        }

        [Route("/lessonWatch/{identifier}")]
        [HttpPut]
        public IActionResult Edit(String identifier, [FromBody] EditLessonWatchForm form)
        {
            if (!form.IsValid)
                return JsonBase.ResponseError(form.Errors);

            var lessonWatch = lessonWatchRepository.FindOne(new { Identifier = identifier });
            if (lessonWatch == null)
                return StatusCode(StatusCodes.Status404NotFound);

            lessonWatchRepository.Update(lessonWatch, form.Data);
            return JsonBase.ResponseObject(lessonWatch);
        }

        [Route("/lessonWatch/{identifier}")]
        [HttpDelete]
        public StatusCodeResult Delete(String identifier)
        {
            var lessonWatch = lessonWatchRepository.FindOne(new { Identifier = identifier });
            if (lessonWatch == null)
                return StatusCode(StatusCodes.Status404NotFound);

            lessonWatchRepository.Delete(lessonWatch);
            return StatusCode(StatusCodes.Status204NoContent);
        }

        [Route("/lessonWatch/{userIdentifier}/{lessonIdentifier}")]
        [HttpGet]
        public JsonResult List(String userIdentifier, String lessonIdentifier)
        {
            var user = userRepository.FindOne(new { Identifier = userIdentifier });
            var lesson = lessonRepository.FindOne(new { Identifier = lessonIdentifier });
            var lessonWatch = lessonWatchRepository.FindOne(new { User = user, Lesson = lesson });

            if (lessonWatch == null)
                return JsonBase.Response("false", 200);
            else
                return JsonBase.Response("true", 200);
        }

        [Route("/lessonWatch/export/{lessonIdentifier}")]
        [HttpGet]
        public IActionResult Export(String lessonIdentifier)
        {
            var lesson = lessonRepository.FindOne(new { Identifier = lessonIdentifier });
            var lessonWatch = lessonWatchRepository.FindAll(new { Lesson = lesson });

            StringBuilder strLessonWatch = new StringBuilder();

            strLessonWatch.Append("<table>");
            strLessonWatch.Append("<tr>");
            strLessonWatch.Append("<td>Name</td>");
            strLessonWatch.Append("<td>Email</td>");
            strLessonWatch.Append("<td>CreatedAt</td>");
            strLessonWatch.Append("</tr>");

            for (int i = 0; i < lessonWatch.Count; i++)
            {
                strLessonWatch.Append("<tr>");
                strLessonWatch.Append("<td>" + lessonWatch[i].User.Name + "</td>");
                strLessonWatch.Append("<td>" + lessonWatch[i].User.Email + "</td>");
                strLessonWatch.Append("<td>" + lessonWatch[i].CreatedAt + "</td>");
                strLessonWatch.Append("</tr>");
            }

            strLessonWatch.Append("</table>");

            byte[] temp = System.Text.Encoding.ASCII.GetBytes(strLessonWatch.ToString());
            return File(temp, "application/octet-stream"); 
        }
    }
}