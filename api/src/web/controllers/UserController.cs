using System;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Web.Services;
using UniversidadeMsd.Core.Services;
using UniversidadeMsd.Web.Exceptions;
using System.Text;
using System.Globalization;
using System.IO;

using EnumProfile = UniversidadeMsd.Core.ObjectValues.Profile;
using System.Collections.Generic;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/users")]
    public class UserController : BaseController
    {
        private readonly IUserRepository userRepository;
        private readonly IUploadService uploadService;
        private readonly IMailService mailService;
        private readonly ISegmentRepository segmentRepository;
        private readonly ISchoolRepository schoolRepository;

        public UserController(IUserRepository userRepository, ISessionManager sessionManager,
                              IUploadService uploadService, IMailService mailService, ISegmentRepository segmentRepository, ISchoolRepository schoolRepository) : base(sessionManager) {
            this.userRepository = userRepository;
            this.uploadService = uploadService;
            this.mailService = mailService;
            this.segmentRepository = segmentRepository;
            this.schoolRepository = schoolRepository;
        }

        [HttpGet]
        public JsonResult List(SearchForm form) {
            return JsonBase.ResponsePaginated(
                userRepository.FindAll(form.Terms, form.Offset, form.Limit),
                form.Offset, form.Limit,
                userRepository.FindCount(form.Terms)
            );
        }

        [Route("/users/importUsers")]
        [HttpPost]
        public IActionResult ImportUsers([FromBody] ImportUsersForm form)
        {
            StringBuilder sbError = new StringBuilder();
            string linhaserror = "";
            List<User> users = new List<User>();

            // Verifica se arquivo foi enviado
            if (form.HasFile)
            {
                // Le arquivo CSV
                byte[] toDecodeByte = Convert.FromBase64String(form.File);

                System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
                System.Text.Decoder utf8Decode = encoder.GetDecoder();

                int charCount = utf8Decode.GetCharCount(toDecodeByte, 0, toDecodeByte.Length);

                char[] decodedChar = new char[charCount];
                utf8Decode.GetChars(toDecodeByte, 0, toDecodeByte.Length, decodedChar, 0);
                string result = new String(decodedChar);

                string[] results = result.TrimEnd().Split("\n");
                string[] linhaseparada = null;

                // Analisa poss�veis erros
                for (int i = 0; i < results.Length; i++)
                {
                    if (results[i].Contains(","))
                    {
                        linhaseparada = results[i].Split(',');
                    } else if (results[i].Contains(";"))
                    {
                        linhaseparada = results[i].Split(';');
                    } else
                    {
                        return JsonBase.ResponseError("Error", "Imposs�vel ler arquivo CSV");
                    }

                    // Desconsidera cabe�alho
                    if (linhaseparada[0] != "Name")
                    {
                        bool error = false;
                        User user = new User();
                        user.Name = linhaseparada[0].ToString();
                        user.Email = linhaseparada[1].ToString();
                        user.Phone = linhaseparada[2].ToString();

                        try
                        {
                            user.Profile = (EnumProfile)Convert.ToInt32(linhaseparada[3]);
                        } catch
                        {
                            error = true;
                        }

                        try
                        {
                            user.Segment = segmentRepository.FindOne(new { Id = Convert.ToInt32(linhaseparada[4]) });
                        } catch
                        {
                            error = true;
                        }

                        try
                        {
                            user.School = linhaseparada[5].ToString() == "" || linhaseparada[5].ToString() == "\r" ? null : schoolRepository.FindOne(new { Id = Convert.ToInt32(linhaseparada[5]) });
                        } catch
                        {
                            error = true;
                        }

                        if (linhaseparada[5].ToString() != "" & linhaseparada[5].ToString() != "\r" & user.School == null)
                        {
                            error = true;
                        }

                            user.CreatedAt = DateTime.UtcNow;
                        user.Identifier = Uuid.NewUuid();
                        user.Active = true;

                        // Gera uma senha aleat�ria
                        string password = Guid.NewGuid().ToString("d").Substring(1, 7);
                        user.Password = password;

                        try
                        {
                            var userExist = userRepository.FindOne(new { Email = user.Email });
                            if (userExist == null)
                            {
                                users.Add(user);
                            }
                        } catch (Exception ex)
                        {
                            error = true;
                        }

                        if (error == true)
                        {
                            linhaserror += i + 1 + ", ";
                        }

                    }
                }
            }
            if (linhaserror != "")
            {
                return JsonBase.ResponseError("Error", "Identificamos problemas na(s) linha(s): " + linhaserror.Substring(0, linhaserror.Length-2));
            } else
            {
                if (users.Count > 0)
                {
                    foreach (User u in users)
                    {
                        string password = "";
                        password = u.Password;
                        userRepository.SaveOrUpdate(u);

                        // Envia por e-mail
                        mailService.SendNewUser(u.Name, u.Email, password);
                    }
                }
            }


            return Ok();
        }


        [HttpPost]
        public JsonResult Add([FromBody] AddUserForm form) {
            if (!form.IsValid) {
                return JsonBase.ResponseError(form.Errors);
            }
            try {
                var user = form.Data;
                if (form.HasFile) {
                    user.Photo = uploadService.SaveFile(user.Identifier, form.Photo);
                }
                // Gera uma senha aleat�ria
                string password = Guid.NewGuid().ToString("d").Substring(1, 7);
                user.Password = password;

                userRepository.SaveOrUpdate(user);

                // Envia por e-mail
                mailService.SendNewUser(user.Name, user.Email, password);


                return JsonBase.ResponseObject(user);
            } catch (DuplicatedConstraintException) {
                return JsonBase.ResponseError("email", "Email already exists.");
            } catch (FileNotSupportedException ex) {
                return JsonBase.ResponseError("photo", ex.Message);
            } catch (EntityNotFoundException ex) {
                return JsonBase.ResponseError(ex.EntityName, ex.Message);
            }
        }

        [Route("/users/auth")]
        [HttpPost]
        public IActionResult Auth([FromBody] AuthForm form) {
            if (!form.IsValid) {
                return JsonBase.ResponseError(form.Errors);
            }
            var user = userRepository.Authenticate(form.Data);
            if (user == null || user.Active == false)
                return StatusCode(StatusCodes.Status401Unauthorized);

            sessionManager.Set(HttpContext, user);
            return JsonBase.ResponseObject(user);
        }

        [Route("/users/auth")]
        [HttpGet]
        public IActionResult Auth() {
            var identifier = sessionManager.Get<User>(HttpContext);
            if (identifier != null) {
                return JsonBase.ResponseObject(userRepository.FindOne(new { Identifier = identifier }));
            }
            return StatusCode(StatusCodes.Status401Unauthorized);
        }

        [Route("/users/export")]
        [HttpGet]
        public IActionResult Export(SearchForm form)
        {
            var users = userRepository.FindAll(form.Terms, form.Offset, form.Limit);
            StringBuilder strUsers = new StringBuilder();

            strUsers.Append("<table>");
            strUsers.Append("<tr>");
            strUsers.Append("<td>Name</td>");
            strUsers.Append("<td>Email</td>");
            strUsers.Append("<td>Phone</td>");
            strUsers.Append("<td>Profile</td>");
            strUsers.Append("<td>Segment</td>");
            strUsers.Append("<td>School</td>");
            strUsers.Append("<td>CreatedAt</td>");
            strUsers.Append("</tr>");

            for (int i = 0; i < users.Count; i++)
            {
                string schoolName = "";
                string segmentName = "";

                if (users[i].School != null)
                    schoolName = users[i].School.Name;
                if (users[i].Segment != null)
                    segmentName = users[i].Segment.Name;

                strUsers.Append("<tr>");
                strUsers.Append("<td>"+users[i].Name+"</td>");
                strUsers.Append("<td>" + users[i].Email + "</td>");
                strUsers.Append("<td>" + users[i].Phone+ "</td>");
                strUsers.Append("<td>" + users[i].Profile + "</td>");
                strUsers.Append("<td>" + segmentName + "</td>");
                strUsers.Append("<td>" + schoolName + "</td>");
                strUsers.Append("<td>" + users[i].CreatedAt + "</td>");
                strUsers.Append("</tr>");
            }

            strUsers.Append("</table>");

            byte[] temp = System.Text.Encoding.GetEncoding(CultureInfo.GetCultureInfo("pt-BR").TextInfo.ANSICodePage).GetBytes(strUsers.ToString());
            return File(temp, "application/octet-stream");
        }


        [Route("/users/logout")]
        [HttpGet]
        public IActionResult Logout()
        {
            var identifier = sessionManager.Get<User>(HttpContext);
            identifier = null;
            sessionManager = null;
            HttpContext.Session.Clear();
            return StatusCode(StatusCodes.Status204NoContent);
        }


        [Route("/users/{identifier}")]
        [HttpPut]
        public JsonResult Edit(String identifier, [FromBody] EditUserForm form) {
            if (!form.IsValid) {
                return JsonBase.ResponseError(form.Errors);
            }
            try {
                var user = userRepository.FindOne(new { Identifier = identifier });
                if (user == null)
                    return JsonBase.Response(statusCode: StatusCodes.Status404NotFound);

                var newUser = form.Data;
                if (form.HasFile)
                    newUser.Photo = uploadService.SaveFile(identifier, form.Photo);
                userRepository.Update(user, newUser);
                return JsonBase.ResponseObject(user);
            } catch (DuplicatedConstraintException) {
                return JsonBase.ResponseError("email", "Email already exists.", StatusCodes.Status400BadRequest);
            } catch (FileNotSupportedException ex) {
                return JsonBase.ResponseError("photo", ex.Message);
            } catch (EntityNotFoundException ex) {
                return JsonBase.ResponseError(ex.EntityName, ex.Message);
            }
        }

        [Route("/users/{identifier}")]
        [HttpDelete]
        public StatusCodeResult Delete(String identifier) {
            var user = userRepository.FindOne(new { Identifier = identifier });
            if (user == null)
                return StatusCode(StatusCodes.Status404NotFound);

            userRepository.Delete(user);
            return StatusCode(StatusCodes.Status204NoContent);
        }

        [Route("/users/rank/{type}/{tIdentifier}")]
        [HttpPost]
        public ActionResult Rank(String type, String tIdentifier)
        {
            Segment segment = new Segment();
            School school = new School();

            if (type == "segment")
            {
                segment = segmentRepository.FindOne(new { Identifier = tIdentifier });
            }             
            else if (type == "school")
            {
                school = schoolRepository.FindOne(new { Identifier = tIdentifier });
            }

            String query = "select  u.Identifier UserIdentifier, " +
                            "       u.Id UserId, " +
                            "       u.Name UserName, " +
                            "       s.Identifier SegmentIdentifier, " +
                            "       s.Id SegmentId, " +
                            "       s.Name SegmentName, " +
                            "       sh.Identifier SchoolIdentifier, " +
                            "       sh.Id SchoolId, " +
                            "       sh.Name SchoolName, " +
                            "       sum(guh.Points) Points " +
                            "from   [User] u " +
                            "join   GameficationUserHistory guh on guh.UserId = u.Id " +
                            "left   join Segment s on s.Id = u.SegmentId " +
                            "left   join School sh on sh.Id = u.SchoolId ";

            String where = "where u.Profile = 0 ";

            String group = "group by u.Identifier, u.Id, u.Name, s.Identifier, s.Id, s.Name, sh.Identifier, sh.Id, sh.Name ";

            String order = "order by points desc";


            if (type == "segment")
            {
                // Ranking do Segmento
                where = "and s.Identifier = '" + tIdentifier + "'";

                var rank = userRepository.RawSql<Rank>(query + where + group + order);

                if (rank != null)
                    return JsonBase.ResponseObject(rank);
            }
            else if (type == "school")
            {
                // Ranking da Escola
                where = "and sh.Identifier = '" + tIdentifier + "'";

                var rank = userRepository.RawSql<Rank>(query + where + group + order);

                if (rank != null)
                    return JsonBase.ResponseObject(rank);
            }
            else if (type == "general")
            {
                // Ranking Geral
                var rank = userRepository.RawSql<Rank>(query + where + group + order);
                if (rank != null)
                    return JsonBase.ResponseObject(rank);
            }
            else
            {
                return StatusCode(StatusCodes.Status404NotFound);
            }

            return StatusCode(StatusCodes.Status404NotFound);
        }

        [Route("/users/summary/{userIdentifier}")]
        [HttpGet]
        public ActionResult Summary(String userIdentifier)
        {
            var user = userRepository.FindOne(new { Identifier = userIdentifier });

            if (user == null)
                return StatusCode(StatusCodes.Status404NotFound);

            string query = "select	distinct l.Name LessonName, " +
                            "       l.Identifier LessonIdentifier, " +
                            "       l.Id LessonId, " +
                            "       s.Name SegmentName, " +
                            "       s.Identifier SegmentIdentifier, " +
                            "       s.Id SegmentId, " +
                            "       min(lw.CreatedAt) CreatedAt, " +
                            "       Case when lq.Id is null then 'Not Available' when count(le.Id) > 0 then 'OK' else 'Pending' end as exam,  " +
                            "       isnull(le.QtyQuestions, 0) QtyQuestions, " +
                            "       isnull(le.QtySuccess, 0) QtySuccess " +
                            "       from    LessonWatch lw " +
                            "       join Lesson l on l.Id = lw.LessonId " +
                            "       join Segment s on s.Id = l.SegmentId " +
                            "       left join LessonExam le on le.LessonId = l.Id and le.UserId = lw.UserId " +
                            "       left join LessonQuestion lq on lq.LessonId = l.Id " +
                            "       where lw.UserId = " +user.Id +
                            "       group by l.Name, l.Identifier, l.Id, s.Name, s.Identifier, le.Id, lq.Id, s.Id, le.QtyQuestions, le.QtySuccess ";

            var summary = userRepository.RawSql<UserSummary>(query);

            if (summary != null)
                return JsonBase.ResponseObject(summary);

            return StatusCode(StatusCodes.Status204NoContent);
        }

        [Route("/users/forget-password")]
        [HttpPost]
        public ActionResult ForgetPassword([FromBody] ForgetEmailForm form) {
            if (!form.IsValid)
                return JsonBase.ResponseError(form.Errors);
            var user = userRepository.FindOne(new { Email = form.Email });

            user.HashNewPassword = Uuid.NewUuid();
            userRepository.SaveOrUpdate(user);

            mailService.SendForgetPassword(user.Name, user.Email, user.HashNewPassword);

            if (user == null)
                return StatusCode(StatusCodes.Status404NotFound);
            return StatusCode(200);
        }

        [Route("/users/new-password")]
        [HttpPost]
        public ActionResult NewPassword([FromBody] NewPasswordForm form)
        {
            if (!form.IsValid)
                return JsonBase.ResponseError(form.Errors);

            var user = userRepository.FindOne(new { HashNewPassword = form.HashNewPassword });

            user.Password = form.Password;
            user.HashNewPassword = null;
            userRepository.SaveOrUpdate(user);

            if (user == null)
                return StatusCode(StatusCodes.Status404NotFound);
            return StatusCode(200);

        }

    }
}