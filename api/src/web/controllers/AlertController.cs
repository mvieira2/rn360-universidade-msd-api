using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/alerts")]
    public class AlertController : Controller
    {
        IAlertRepository alertRepository;

        public AlertController(IAlertRepository alertRepository) {
            this.alertRepository = alertRepository;
        }

        [HttpGet]
        public JsonResult List(SearchForm form) {
            return JsonBase.ResponsePaginated(
                alertRepository.FindAll(form.Terms, form.Offset, form.Limit),
                form.Offset, form.Limit,
                alertRepository.FindCount(form.Terms)
            );
        }

        [HttpPost]
        public JsonResult Add([FromBody] AddAlertForm form) {
           if (!form.IsValid) {
               return JsonBase.ResponseError(form.Errors);
           }

            try
            {
                var alert = form.Data;
                alertRepository.SaveOrUpdate(alert);
                return JsonBase.ResponseObject(alert);
            } catch (EntityNotFoundException ex) {
                return JsonBase.ResponseError(ex.EntityName, ex.Message);
            }
        }

        [Route("/alerts/{identifier}")]
        [HttpPut]
        public JsonResult Edit(String identifier, [FromBody] EditAlertForm form) {
           if (!form.IsValid) {
               return JsonBase.ResponseError(form.Errors);
           }
            var alert = alertRepository.FindOne(new { Identifier = identifier });
            if (alert == null)
                return JsonBase.Response(statusCode: StatusCodes.Status404NotFound);
            try
            {
                alertRepository.Update(alert, form.Data);
                return JsonBase.ResponseObject(alert);
            }
            catch (EntityNotFoundException ex)
            {
                return JsonBase.ResponseError(ex.EntityName, ex.Message);
            }
        }

        [Route("/alerts/{identifier}")]
        [HttpDelete]
        public StatusCodeResult Delete(String identifier) {
           var alert = alertRepository.FindOne(new { Identifier = identifier });
           if (alert == null)
               return StatusCode(StatusCodes.Status404NotFound);

           alertRepository.Delete(alert);
           return StatusCode(StatusCodes.Status204NoContent);
        }
    }
}