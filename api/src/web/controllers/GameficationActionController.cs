﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/gamefication-action")]
    public class GameficationActionController : Controller
    {
        IGameficationActionRepository GameficationActionRepository;

        public GameficationActionController(IGameficationActionRepository GameficationActionRepository)
        {
            this.GameficationActionRepository = GameficationActionRepository;
        }

        [HttpGet]
        public JsonResult List(SearchForm form)
        {
            return JsonBase.ResponsePaginated(
                GameficationActionRepository.FindAll(form.Terms, form.Offset, form.Limit),
                form.Offset, form.Limit,
                GameficationActionRepository.FindCount(form.Terms)
            );
        }

        [HttpPost]
        public JsonResult Add([FromBody] AddGameficationActionForm form)
        {
            if (!form.IsValid)
            {
                return JsonBase.ResponseError(form.Errors);
            }

            try
            {
                var GameficationAction = form.Data;
                GameficationActionRepository.SaveOrUpdate(GameficationAction);
                return JsonBase.ResponseObject(GameficationAction);
            }
            catch (EntityNotFoundException ex)
            {
                return JsonBase.ResponseError(ex.EntityName, ex.Message);
            }
        }

        [Route("/gamefication-action/{identifier}")]
        [HttpPut]
        public JsonResult Edit(String identifier, [FromBody] EditGameficationActionForm form)
        {
            if (!form.IsValid)
            {
                return JsonBase.ResponseError(form.Errors);
            }
            var GameficationAction = GameficationActionRepository.FindOne(new { Identifier = identifier });
            if (GameficationAction == null)
                return JsonBase.Response(statusCode: StatusCodes.Status404NotFound);
            try
            {
                GameficationActionRepository.Update(GameficationAction, form.Data);
                return JsonBase.ResponseObject(GameficationAction);
            }
            catch (EntityNotFoundException ex)
            {
                return JsonBase.ResponseError(ex.EntityName, ex.Message);
            }
        }

        [Route("/gamefication-action/{identifier}")]
        [HttpDelete]
        public StatusCodeResult Delete(String identifier)
        {
            var GameficationAction = GameficationActionRepository.FindOne(new { Identifier = identifier });
            if (GameficationAction == null)
                return StatusCode(StatusCodes.Status404NotFound);

            GameficationActionRepository.Delete(GameficationAction);
            return StatusCode(StatusCodes.Status204NoContent);
        }
    }
}