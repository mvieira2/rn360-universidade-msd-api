using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Core.Services;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Web.Exceptions;
using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/downloads")]
    public class DownloadController : Controller
    {
        IDownloadRepository downloadRepository;
        IUploadService uploadService;

        public DownloadController(IDownloadRepository downloadRepository, IUploadService uploadService) {
            this.downloadRepository = downloadRepository;
            this.uploadService = uploadService;
        }

        [HttpGet]
        public JsonResult List(SearchForm form) {
            return JsonBase.ResponsePaginated(
                downloadRepository.FindAll(form.Terms, form.Offset, form.Limit).OrderByDescending(n => n.CreatedAt),
                form.Offset, form.Limit,
                downloadRepository.FindCount(form.Terms)
            );
        }

        [HttpPost]
        public JsonResult Add([FromBody] AddDownloadForm form)
        {
            if (!form.IsValid) {
                return JsonBase.ResponseError(form.Errors);
            }

            try {
                var download = form.Data;
                if (form.HasFile) {
                    download.DownloadFile = uploadService.SaveFile(download.Identifier, form.DownloadFile);
                }
                downloadRepository.SaveOrUpdate(download);
                return JsonBase.ResponseObject(download);
            } catch (EntityNotFoundException ex) {
                return JsonBase.ResponseError(ex.EntityName, ex.Message);
            } catch (FileNotSupportedException ex) {
                return JsonBase.ResponseError("downloadFile", ex.Message);
            }
        }

        [Route("/downloads/{identifier}")]
        [HttpPut]
        public JsonResult Edit(String identifier, [FromBody] EditDownloadForm form) {
           if (!form.IsValid)
               return JsonBase.ResponseError(form.Errors);

           var download = downloadRepository.FindOne(new { Identifier = identifier });
           if (download == null)
               return JsonBase.Response(statusCode: StatusCodes.Status404NotFound);

           try {
               var newDownload = form.Data;
               if (form.HasFile)
                   download.DownloadFile = uploadService.SaveFile(identifier, form.DownloadFile);

               downloadRepository.Update(download, newDownload);
               return JsonBase.ResponseObject(download);
           } catch (EntityNotFoundException ex) {
               return JsonBase.ResponseError(ex.EntityName, ex.Message);
           } catch (FileNotSupportedException ex) {
               return JsonBase.ResponseError("downloadFile", ex.Message);
           }
        }

        [Route("/downloads/{identifier}")]
        [HttpDelete]
        public StatusCodeResult Delete(String identifier) {
           var download = downloadRepository.FindOne(new { Identifier = identifier });
           if (download == null)
               return StatusCode(StatusCodes.Status404NotFound);

           downloadRepository.Delete(download);
           return StatusCode(StatusCodes.Status204NoContent);
        }
    }
}