﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/lessonComments")]
    public class LessonCommentsController : Controller
    {
        ILessonCommentsRepository lessonCommentsRepository;
        ILessonRepository lessonRepository;

        public LessonCommentsController(ILessonCommentsRepository lessonCommentsRepository, ILessonRepository lessonRepository)
        {
            this.lessonCommentsRepository = lessonCommentsRepository;
            this.lessonRepository = lessonRepository;
        }

        [HttpGet]
        public JsonResult List(SearchForm form)
        {
            return JsonBase.ResponsePaginated(
                lessonCommentsRepository.FindAll(form.Terms, form.Offset, form.Limit),
                form.Offset, form.Limit,
                lessonCommentsRepository.FindCount(form.Terms)
            );
        }

        [HttpPost]
        public JsonResult Add([FromBody] AddLessonCommentsForm form)
        {
            if (!form.IsValid)
            {
                return JsonBase.ResponseError(form.Errors);
            }
            var lessonComments = form.Data;
            lessonCommentsRepository.SaveOrUpdate(lessonComments);
            return JsonBase.ResponseObject(lessonComments);
        }

        [Route("/lessonComments/{identifier}")]
        [HttpPut]
        public IActionResult Edit(String identifier, [FromBody] EditLessonCommentsForm form)
        {
            if (!form.IsValid)
                return JsonBase.ResponseError(form.Errors);

            var lessonComments = lessonCommentsRepository.FindOne(new { Identifier = identifier });
            if (lessonComments == null)
                return StatusCode(StatusCodes.Status404NotFound);

            lessonCommentsRepository.Update(lessonComments, form.Data);
            return JsonBase.ResponseObject(lessonComments);
        }

        [Route("/lessonComments/{identifier}")]
        [HttpDelete]
        public StatusCodeResult Delete(String identifier)
        {
            var lessonComments = lessonCommentsRepository.FindOne(new { Identifier = identifier });
            if (lessonComments == null)
                return StatusCode(StatusCodes.Status404NotFound);

            lessonCommentsRepository.Delete(lessonComments);
            return StatusCode(StatusCodes.Status204NoContent);
        }

        [Route("/lessonComments/{lessonIdentifier}")]
        [HttpGet]
        public ActionResult List(String lessonIdentifier)
        {
            var lesson = lessonRepository.FindOne(new { Identifier = lessonIdentifier });

            if (lesson == null)
                return StatusCode(StatusCodes.Status204NoContent);
            else
            {
                var lessonComments = lessonCommentsRepository.FindAll(new { Lesson = lesson });
                return JsonBase.ResponseObject(lessonComments);
            }
        }
    }
}