using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/profiles")]
    public class ProfileController : Controller
    {
        private IProfileRepository profileRepository;

        public ProfileController(IProfileRepository profileRepository) {
            this.profileRepository = profileRepository;
        }

        [HttpGet]
        public JsonResult List()
        {
            return JsonBase.Response(new JsonBase { Data = profileRepository.GetAll() });
        }
    }
}