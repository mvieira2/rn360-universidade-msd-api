using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/segments")]
    public class SegmentController : Controller
    {
        private ISegmentRepository segmentRepository;

        public SegmentController(ISegmentRepository segmentRepository) {
            this.segmentRepository = segmentRepository;
        }

        [HttpGet]
        public JsonResult List()
        {
            var segments = segmentRepository.GetAll();
            return JsonBase.ResponsePaginated(segments, null, null, segments.Count);
        }
    }
}