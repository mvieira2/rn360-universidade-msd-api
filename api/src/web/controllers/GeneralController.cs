using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    public class GeneralController : Controller
    {
        [Route("/health")]
        [HttpGet]
        public StatusCodeResult Health() {
            return StatusCode(200);
        }
    }
}