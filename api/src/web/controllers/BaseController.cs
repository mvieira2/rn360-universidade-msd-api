
using Microsoft.AspNetCore.Mvc;
using UniversidadeMsd.Web.Services;

namespace UniversidadeMsd.Web.Controllers
{
    public class BaseController : Controller
    {
        protected ISessionManager sessionManager;

        public BaseController(ISessionManager sessionManager) {
            this.sessionManager = sessionManager;
        }
    }
}