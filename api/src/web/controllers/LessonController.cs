using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/lessons")]
    public class LessonController : Controller
    {
        ILessonRepository lessonRepository;

        public LessonController(ILessonRepository lessonRepository) {
            this.lessonRepository = lessonRepository;
        }

        [HttpGet]
        public JsonResult List(SearchForm form) {
            return JsonBase.ResponsePaginated(
                lessonRepository.FindAll(form.Terms, form.Offset, form.Limit).OrderByDescending(n => n.StartedAt),
                form.Offset, form.Limit,
                lessonRepository.FindCount(form.Terms)
            );
        }

        [HttpPost]
        public JsonResult Add([FromBody] AddLessonForm form) {
           if (!form.IsValid) {
               return JsonBase.ResponseError(form.Errors);
           }
           var lesson = form.Data;
           lessonRepository.SaveOrUpdate(lesson);
           return JsonBase.ResponseObject(lesson);
        }

        [Route("/lessons/{identifier}")]
        [HttpPut]
        public IActionResult Edit(String identifier, [FromBody] EditLessonForm form) {
            if (!form.IsValid)
                return JsonBase.ResponseError(form.Errors);

            var lesson = lessonRepository.FindOne(new { Identifier = identifier });
            if (lesson == null)
                return StatusCode(StatusCodes.Status404NotFound);

            lessonRepository.Update(lesson, form.Data);
            return JsonBase.ResponseObject(lesson);
        }

        [Route("/lessons/{identifier}")]
        [HttpDelete]
        public StatusCodeResult Delete(String identifier) {
           var lesson = lessonRepository.FindOne(new { Identifier = identifier });
           if (lesson == null)
               return StatusCode(StatusCodes.Status404NotFound);

           lessonRepository.Delete(lesson);
           return StatusCode(StatusCodes.Status204NoContent);
        }
    }
}