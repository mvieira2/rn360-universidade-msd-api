﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/gamefication-user-history")]
    public class GameficationUserHistoryController : Controller
    {
        IGameficationUserHistoryRepository gameficationUserHistoryRepository;
        IGameficationActionRepository gameficationActionRepository;
        IUserRepository userRepository;

        public GameficationUserHistoryController(IGameficationUserHistoryRepository gameficationUserHistoryRepository, IGameficationActionRepository gameficationActionRepository, IUserRepository userRepository)
        {
            this.gameficationUserHistoryRepository = gameficationUserHistoryRepository;
            this.gameficationActionRepository = gameficationActionRepository;
            this.userRepository = userRepository;
        }

        [HttpGet]
        public JsonResult List(SearchForm form)
        {
            return JsonBase.ResponsePaginated(
                gameficationUserHistoryRepository.FindAll(form.Terms, form.Offset, form.Limit),
                form.Offset, form.Limit,
                gameficationUserHistoryRepository.FindCount(form.Terms)
            );
        }

        [HttpPost]
        public JsonResult Add([FromBody] AddGameficationUserHistoryForm form)
        {
            if (!form.IsValid)
            {
                return JsonBase.ResponseError(form.Errors);
            }

            try
            {
                var gameficationUserHistory = form.Data;

                // Regra de Negócio para Verificar Ação Única
                var action = gameficationActionRepository.FindOne(new { Identifier = gameficationUserHistory.Action.Identifier.ToString() });
                var user = userRepository.FindOne(new { Identifier = gameficationUserHistory.User.Identifier.ToString() });

                if (action.UniqueUserPoint == true)
                {
                    var userHistory = gameficationUserHistoryRepository.RawSql<GameficationUserHistory>("SELECT Identifier from GameficationUserHistory where UserId =" + user.Id + " and GameficationActionId = " + action.Id);

                    if (userHistory.Count > 0)
                        return JsonBase.ResponseError("GameficationUserHistory", "Points already counted");
                }

                gameficationUserHistory.Points = action.Points;


                gameficationUserHistoryRepository.SaveOrUpdate(gameficationUserHistory);
                return JsonBase.ResponseObject(gameficationUserHistory);
            }
            catch (EntityNotFoundException ex)
            {
                return JsonBase.ResponseError(ex.EntityName, ex.Message);
            }
        }

        [Route("/gamefication-user-history/{identifier}")]
        [HttpPut]
        public JsonResult Edit(String identifier, [FromBody] EditGameficationUserHistoryForm form)
        {
            if (!form.IsValid)
            {
                return JsonBase.ResponseError(form.Errors);
            }
            var GameficationUserHistory = gameficationUserHistoryRepository.FindOne(new { Identifier = identifier });
            if (GameficationUserHistory == null)
                return JsonBase.Response(statusCode: StatusCodes.Status404NotFound);
            try
            {
                gameficationUserHistoryRepository.Update(GameficationUserHistory, form.Data);
                return JsonBase.ResponseObject(GameficationUserHistory);
            }
            catch (EntityNotFoundException ex)
            {
                return JsonBase.ResponseError(ex.EntityName, ex.Message);
            }
        }

        [Route("/gamefication-user-history/{identifier}")]
        [HttpDelete]
        public StatusCodeResult Delete(String identifier)
        {
            var GameficationUserHistory = gameficationUserHistoryRepository.FindOne(new { Identifier = identifier });
            if (GameficationUserHistory == null)
                return StatusCode(StatusCodes.Status404NotFound);

            gameficationUserHistoryRepository.Delete(GameficationUserHistory);
            return StatusCode(StatusCodes.Status204NoContent);
        }
    }
}