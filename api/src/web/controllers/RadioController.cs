using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Core.Services;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Web.Exceptions;
using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/radios")]
    public class RadioController : Controller
    {
        IRadioRepository radioRepository;
        IUploadService uploadService;

        public RadioController(IRadioRepository radioRepository, IUploadService uploadService) {
            this.radioRepository = radioRepository;
            this.uploadService = uploadService;
        }

        [HttpGet]
        public JsonResult List(SearchForm form) {
            return JsonBase.ResponsePaginated(
                radioRepository.FindAll(form.Terms, form.Offset, form.Limit).OrderByDescending(n => n.CreatedAt),
                form.Offset, form.Limit,
                radioRepository.FindCount(form.Terms)
            );
        }

        [HttpPost]
        public JsonResult Add([FromBody] AddRadioForm form)
        {
            if (!form.IsValid) {
                return JsonBase.ResponseError(form.Errors);
            }

            try {
                var radio = form.Data;

                if (form.HasFile)
                    radio.AudioFile = uploadService.SaveFile(radio.Identifier, form.AudioFile);
                radioRepository.SaveOrUpdate(radio);
                return JsonBase.ResponseObject(radio);
            } catch (EntityNotFoundException ex) {
                return JsonBase.ResponseError(ex.EntityName, ex.Message);
           } catch (FileNotSupportedException ex) {
               return JsonBase.ResponseError("audioFile", ex.Message);
           }
        }

        [Route("/radios/{identifier}")]
        [HttpPut]
        public JsonResult Edit(String identifier, [FromBody] EditRadioForm form) {
            if (!form.IsValid)
                return JsonBase.ResponseError(form.Errors);

            var radio = radioRepository.FindOne(new { Identifier = identifier });
            if (radio == null)
                return JsonBase.Response(statusCode: StatusCodes.Status404NotFound);

            try {
                var newRadio = form.Data;
                if (form.HasFile)
                    radio.AudioFile = uploadService.SaveFile(identifier, newRadio.AudioFile);

                radioRepository.Update(radio, newRadio);
                return JsonBase.ResponseObject(radio);
            }
            catch (EntityNotFoundException ex) {
                return JsonBase.ResponseError(ex.EntityName, ex.Message);
            } catch (FileNotSupportedException ex) {
                return JsonBase.ResponseError("audioFile", ex.Message);
            }
        }

        [Route("/radios/{identifier}")]
        [HttpDelete]
        public StatusCodeResult Delete(String identifier) {
           var radio = radioRepository.FindOne(new { Identifier = identifier });
           if (radio == null)
               return StatusCode(StatusCodes.Status404NotFound);

           radioRepository.Delete(radio);
           return StatusCode(StatusCodes.Status204NoContent);
        }
    }
}