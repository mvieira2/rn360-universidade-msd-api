﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/lessonExam")]
    public class LessonExamController : Controller
    {
        ILessonExamRepository lessonExamRepository;
        ILessonQuestionRepository lessonQuestionRepository;
        ILessonQuestionAnswerRepository lessonQuestionAnswerRepository;
        IUserRepository userRepository;
        ILessonRepository lessonRepository;

        public LessonExamController(ILessonExamRepository lessonExamRepository, ILessonQuestionRepository lessonQuestionRepository, ILessonQuestionAnswerRepository lessonQuestionAnswerRepository, IUserRepository userRepository, ILessonRepository lessonRepository)
        {
            this.lessonExamRepository = lessonExamRepository;
            this.lessonQuestionRepository = lessonQuestionRepository;
            this.lessonQuestionAnswerRepository = lessonQuestionAnswerRepository;
            this.userRepository = userRepository;
            this.lessonRepository = lessonRepository;
        }

        [HttpGet]
        public JsonResult List(SearchForm form)
        {
            return JsonBase.ResponsePaginated(
                lessonExamRepository.FindAll(form.Terms, form.Offset, form.Limit),
                form.Offset, form.Limit,
                lessonExamRepository.FindCount(form.Terms)
            );
        }

        [HttpPost]
        public JsonResult Add([FromBody] AddLessonExamForm form)
        {
            int qtyTotal = 0;
            int qtySuccess = 0;
            int qtyWrong = 0;

            if (!form.IsValid)
            {
                return JsonBase.ResponseError(form.Errors);
            }
            var lessonExam = form.Data;

            // Regra de Negócios de Calculo de Resultado da Prova
            for (int i = 0; i < lessonExam.Details.Count; i++)
            {
                var lessonQuestion = lessonQuestionRepository.FindOne(new { Identifier = lessonExam.Details[i].Question.ToString() });
                lessonExam.Details[i].Question = lessonQuestion.Title;

                var lessonQuestionAnswer = lessonQuestionAnswerRepository.FindOne(new { Identifier = lessonExam.Details[i].Answer.ToString() });
                lessonExam.Details[i].Answer = lessonQuestionAnswer.Value;

                if (lessonQuestionAnswer.IsRight == true)
                {
                    lessonExam.Details[i].IsRight = true;
                    qtySuccess++;
                } else
                {
                    lessonExam.Details[i].IsRight = false;
                    qtyWrong++;
                }
                qtyTotal++;
            }

            lessonExam.QtyQuestions = qtyTotal;
            lessonExam.QtySuccess = qtySuccess;
            lessonExam.QtyWrong = qtyWrong;

            lessonExamRepository.SaveOrUpdate(lessonExam);
            return JsonBase.ResponseObject(lessonExam);
        }

        [Route("/lessonExam/{identifier}")]
        [HttpPut]
        public IActionResult Edit(String identifier, [FromBody] EditLessonExamForm form)
        {
            if (!form.IsValid)
                return JsonBase.ResponseError(form.Errors);

            var lessonExam = lessonExamRepository.FindOne(new { Identifier = identifier });
            if (lessonExam == null)
                return StatusCode(StatusCodes.Status404NotFound);

            lessonExamRepository.Update(lessonExam, form.Data);
            return JsonBase.ResponseObject(lessonExam);
        }

        [Route("/lessonExam/{identifier}")]
        [HttpDelete]
        public StatusCodeResult Delete(String identifier)
        {
            var lessonExam = lessonExamRepository.FindOne(new { Identifier = identifier });
            if (lessonExam == null)
                return StatusCode(StatusCodes.Status404NotFound);

            lessonExamRepository.Delete(lessonExam);
            return StatusCode(StatusCodes.Status204NoContent);
        }

        [Route("/lessonExam/check-exam/{userIdentifier}/{lessonIdentifier}")]
        [HttpGet]
        public JsonResult CheckExam(String userIdentifier, String lessonIdentifier)
        {
            var user = userRepository.FindOne(new { Identifier = userIdentifier });
            var lesson = lessonRepository.FindOne(new { Identifier = lessonIdentifier });
            var lessonExam = lessonExamRepository.FindOne(new { User = user, Lesson = lesson });

            if (lessonExam == null)
                return JsonBase.Response("false",200);
            else
                return JsonBase.Response("true", 200);
        }

        [Route("/lessonExam/export/{lessonIdentifier}")]
        [HttpGet]
        public IActionResult Export(String lessonIdentifier)
        {
            var lesson = lessonRepository.FindOne(new { Identifier = lessonIdentifier });
            var lessonExam = lessonExamRepository.FindAll(new { Lesson = lesson });

            StringBuilder strLessonExam = new StringBuilder();

            strLessonExam.Append("<table>");
            strLessonExam.Append("<tr>");
            strLessonExam.Append("<td>Name</td>");
            strLessonExam.Append("<td>Email</td>");
            strLessonExam.Append("<td>QtyQuestions</td>");
            strLessonExam.Append("<td>QtySuccess</td>");
            strLessonExam.Append("<td>QtyWrong</td>");
            strLessonExam.Append("<td>CreatedAt</td>");
            strLessonExam.Append("</tr>");

            for (int i = 0; i < lessonExam.Count; i++)
            {
                strLessonExam.Append("<tr>");
                strLessonExam.Append("<td>" + lessonExam[i].User.Name + "</td>");
                strLessonExam.Append("<td>" + lessonExam[i].User.Email + "</td>");
                strLessonExam.Append("<td>" + lessonExam[i].QtyQuestions + "</td>");
                strLessonExam.Append("<td>" + lessonExam[i].QtySuccess + "</td>");
                strLessonExam.Append("<td>" + lessonExam[i].QtyWrong + "</td>");
                strLessonExam.Append("<td>" + lessonExam[i].CreatedAt + "</td>");
                strLessonExam.Append("</tr>");
            }

            strLessonExam.Append("</table>");

            byte[] temp = System.Text.Encoding.GetEncoding(1252).GetBytes(strLessonExam.ToString());
            return File(temp, "application/octet-stream");
        }
    }
}