using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Core.Services;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Web.Exceptions;
using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/schools")]
    public class SchoolController : Controller
    {
        ISchoolRepository schoolRepository;
        IUploadService uploadService;
        ISegmentRepository segmentRepository;

        public SchoolController(ISchoolRepository schoolRepository, IUploadService uploadService, ISegmentRepository segmentRepository) {
            this.schoolRepository = schoolRepository;
            this.uploadService = uploadService;
            this.segmentRepository = segmentRepository;
        }

        [HttpGet]
        public JsonResult List(SearchForm form) {
            return JsonBase.ResponsePaginated(
                schoolRepository.FindAll(form.Terms, form.Offset, form.Limit),
                form.Offset, form.Limit,
                schoolRepository.FindCount(form.Terms)
            );
        }

        [Route("/schoolsBySegment/{segmentIdentifier}")]
        [HttpGet]
        public ActionResult List(String segmentIdentifier)
        {
            var segment = segmentRepository.FindOne(new { Identifier = segmentIdentifier });

            if (segment == null)
                return StatusCode(StatusCodes.Status404NotFound);

            var schools = schoolRepository.FindAll(new { Segment = segment});

            return JsonBase.ResponseObject(schools);
        }

        [HttpPost]
        public JsonResult Add([FromBody] AddSchoolForm form) {
           if (!form.IsValid) {
               return JsonBase.ResponseError(form.Errors);
           }
           try {
               var school = form.Data;
               if (form.HasFile) {
                   school.Photo = uploadService.SaveFile(school.Identifier, form.Photo);
               }
               schoolRepository.SaveOrUpdate(school);
               return JsonBase.ResponseObject(school);
           } catch (EntityNotFoundException ex) {
               return JsonBase.ResponseError(ex.EntityName, ex.Message);
           } catch (FileNotSupportedException ex) {
               return JsonBase.ResponseError("photo", ex.Message);
           }
        }

        [Route("/schools/{identifier}")]
        [HttpPut]
        public JsonResult Edit(String identifier, [FromBody] EditSchoolForm form) {
           if (!form.IsValid) {
               return JsonBase.ResponseError(form.Errors);
           }
           try {
               var school = schoolRepository.FindOne(new { Identifier = identifier });
               if (school == null)
                   return JsonBase.Response(statusCode: StatusCodes.Status404NotFound);

               if (form.HasFile) {
                   school.Photo = uploadService.SaveFile(school.Identifier, form.Photo);
               }
               schoolRepository.Update(school, form.Data);
               return JsonBase.ResponseObject(school);
           } catch (EntityNotFoundException ex) {
               return JsonBase.ResponseError(ex.EntityName, ex.Message);
           } catch (FileNotSupportedException ex) {
               return JsonBase.ResponseError("photo", ex.Message);
           }
        }

        [Route("/schools/{identifier}")]
        [HttpDelete]
        public StatusCodeResult Delete(String identifier) {
           var school = schoolRepository.FindOne(new { Identifier = identifier });
           if (school == null)
               return StatusCode(StatusCodes.Status404NotFound);

           schoolRepository.Delete(school);
           return StatusCode(StatusCodes.Status204NoContent);
        }
    }
}