﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/lessonQuestion")]
    public class LessonQuestionController : Controller
    {
        ILessonQuestionRepository lessonQuestionRepository;
        ILessonRepository lessonRepository;

        public LessonQuestionController(ILessonQuestionRepository lessonQuestionRepository, ILessonRepository lessonRepository)
        {
            this.lessonQuestionRepository = lessonQuestionRepository;
            this.lessonRepository = lessonRepository;
        }

        [HttpGet]
        public JsonResult List(SearchForm form)
        {
            return JsonBase.ResponsePaginated(
                lessonQuestionRepository.FindAll(form.Terms, form.Offset, form.Limit),
                form.Offset, form.Limit,
                lessonQuestionRepository.FindCount(form.Terms)
            );
        }

        [HttpPost]
        public JsonResult Add([FromBody] AddLessonQuestionForm form)
        {
            if (!form.IsValid)
            {
                return JsonBase.ResponseError(form.Errors);
            }
            var lessonQuestion = form.Data;
            for (int i=0; i < lessonQuestion.Answers.Count; i++)
            {
                lessonQuestion.Answers[i].Identifier = Uuid.NewUuid();
            }

            lessonQuestionRepository.SaveOrUpdate(lessonQuestion);
            return JsonBase.ResponseObject(lessonQuestion);
        }

        [Route("/lessonQuestion/{identifier}")]
        [HttpPut]
        public IActionResult Edit(String identifier, [FromBody] EditLessonQuestionForm form)
        {
            if (!form.IsValid)
                return JsonBase.ResponseError(form.Errors);

            var lessonQuestion = lessonQuestionRepository.FindOne(new { Identifier = identifier });
            if (lessonQuestion == null)
                return StatusCode(StatusCodes.Status404NotFound);

            lessonQuestionRepository.Update(lessonQuestion, form.Data);
            return JsonBase.ResponseObject(lessonQuestion);
        }

        [Route("/lessonQuestion/{lessonIdentifier}")]
        [HttpGet]
        public ActionResult List(String lessonIdentifier)
        {
            var lesson = lessonRepository.FindOne(new { Identifier = lessonIdentifier });

            if (lesson == null)
                return StatusCode(StatusCodes.Status204NoContent);
            else
            {
                var lessonQuestion = lessonQuestionRepository.FindAll(new { Lesson = lesson });
                return JsonBase.ResponseObject(lessonQuestion);
            }
        }

        [Route("/lessonQuestion/{identifier}")]
        [HttpDelete]
        public StatusCodeResult Delete(String identifier)
        {
            var lessonQuestion = lessonQuestionRepository.FindOne(new { Identifier = identifier });
            if (lessonQuestion == null)
                return StatusCode(StatusCodes.Status404NotFound);

            lessonQuestionRepository.Delete(lessonQuestion);
            return StatusCode(StatusCodes.Status204NoContent);
        }
    }
}