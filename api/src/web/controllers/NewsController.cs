using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Utils;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/news")]
    public class NewsController : Controller
    {
        INewsRepository newsRepository;

        public NewsController(INewsRepository newsRepository) {
            this.newsRepository = newsRepository;
        }

        [HttpGet]
        public JsonResult List(SearchForm form) {
            return JsonBase.ResponsePaginated(
                newsRepository.FindAll(form.Terms, form.Offset, form.Limit).OrderByDescending(n => n.CreatedAt),
                form.Offset, form.Limit,
                newsRepository.FindCount(form.Terms)
            );
        }

        [HttpPost]
        public JsonResult Add([FromBody] AddNewsForm form)
        {
            if (!form.IsValid) {
                return JsonBase.ResponseError(form.Errors);
            }

            try {
                var news = form.Data;
                newsRepository.SaveOrUpdate(news);
                return JsonBase.ResponseObject(news);
            } catch (EntityNotFoundException ex) {
                return JsonBase.ResponseError(ex.EntityName, ex.Message);
            }
        }

        [Route("/news/{identifier}")]
        [HttpPut]
        public JsonResult Edit(String identifier, [FromBody] EditNewsForm form) {
           if (!form.IsValid) {
               return JsonBase.ResponseError(form.Errors);
           }
            var news = newsRepository.FindOne(new { Identifier = identifier });
            if (news == null)
                return JsonBase.Response(statusCode: StatusCodes.Status404NotFound);
            try
            {
                newsRepository.Update(news, form.Data);
                return JsonBase.ResponseObject(news);
            }
            catch (EntityNotFoundException ex)
            {
                return JsonBase.ResponseError(ex.EntityName, ex.Message);
            }
        }

        [Route("/news/{identifier}")]
        [HttpDelete]
        public StatusCodeResult Delete(String identifier) {
           var news = newsRepository.FindOne(new { Identifier = identifier });
           if (news == null)
               return StatusCode(StatusCodes.Status404NotFound);

           newsRepository.Delete(news);
           return StatusCode(StatusCodes.Status204NoContent);
        }
    }
}