﻿using System;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Web.Services;
using UniversidadeMsd.Core.Services;
using UniversidadeMsd.Web.Exceptions;
using System.Text;

namespace UniversidadeMsd.Web.Controllers
{
    [Route("/uploadfile")]
    public class UploadFileController : Controller
    {
        private readonly IUploadService uploadService;

        public UploadFileController(IUploadService uploadService) {
            this.uploadService = uploadService;
        }

        [HttpPost]
        public JsonResult Add([FromBody] UploadFileForm form)
        {
            String filename = "";

            if (!form.IsValid)
            {
                return JsonBase.ResponseError(form.Errors);
            }

            try
            {
                if (form.HasFile)
                {
                    filename = uploadService.SaveFile(Guid.NewGuid().ToString(), form.File);

                    return JsonBase.Response(filename, 200);
                }
            }
            catch
            {
                return JsonBase.ResponseError(form.Errors);
            }

            return JsonBase.Response("", 200);
        }
    }
}
