using System;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Web.Services
{
    public class SessionManager : ISessionManager
    {
        public void Set<T>(HttpContext context, T entity) where T : Entity
        {
            context.Session.Set(typeof(T).Name, Encoding.ASCII.GetBytes(entity.Identifier));
        }

        public String Get<T>(HttpContext context) where T : Entity
        {
            var identifier = context.Session.Get(typeof(T).Name);
            if (identifier == null) return null;
            return Encoding.ASCII.GetString(identifier);
        }
    }
}