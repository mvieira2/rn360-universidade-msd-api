using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Services;
using UniversidadeMsd.Web.Exceptions;

namespace UniversidadeMsd.Web.Services
{
    public class UploadService : IUploadService
    {
        private IPathProvider pathProvider;

        public UploadService(IPathProvider pathProvider) {
            this.pathProvider = pathProvider;
        }

        private String GetExtension(String fileType) {
            Regex pattern = new Regex(@"data:([a-zA-Z0-9]+)\/([a-zA-Z0-9-.+]+).*");
            Match match = pattern.Match(fileType);
            if (!match.Success) {
                throw new FileNotSupportedException("Unknown format.");
            }
            return match.Groups[2].Value;
        }

        private StaticFile GetPaths(String identifier, String fileType) {
            var staticFile = new StaticFile {
                Filename = String.Format("{0}.{1}", identifier, GetExtension(fileType))
            };
            staticFile.StaticPath = Path.Combine("static", identifier.Substring(0, 5));
            staticFile.RelativePath = Path.Combine(staticFile.StaticPath, staticFile.Filename);
            staticFile.CompletePath = pathProvider.MapPath(staticFile.RelativePath);
            return staticFile;
        }

        public String SaveFile(String identifier, String file)
        {
            var fileInfo = file.Split(",");
            var staticFile = GetPaths(identifier, fileInfo.First());

            var dir = new DirectoryInfo(staticFile.StaticPath);
            if (!dir.Exists)
                dir.Create();

            File.WriteAllBytes(staticFile.CompletePath, Convert.FromBase64String(fileInfo.Last()));
            return staticFile.RelativePath;
        }
    }

    internal struct StaticFile {
        public String Filename { get; set; }
        public String StaticPath { get; set; }
        public String RelativePath { get; set; }
        public String CompletePath { get; set; }
    }
}