using System;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.Services;

namespace UniversidadeMsd.Web.Services
{
    public class PathProvider : IPathProvider
    {
        private IHostingEnvironment hostingEnvironment;

        public PathProvider(IHostingEnvironment hostingEnvironment) {
            this.hostingEnvironment = hostingEnvironment;
        }

        public string MapPath(string path)
        {
            return Path.Combine(hostingEnvironment.ContentRootPath, path);
        }
    }
}