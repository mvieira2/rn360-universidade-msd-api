using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using UniversidadeMsd.Core.Services;

namespace UniversidadeMsd.Web.Services
{
    public class MailService : IMailService
    {
        private IHostingEnvironment _hostingEnvironment;

        public MailService(IHostingEnvironment environment)
        {
            _hostingEnvironment = environment;
        }

        public void SendForgetPassword(string name, string email, string hash)
        {
            // The subject line of the email
            const String Subject =
                "Recuperar Senha. Universidade MSD.";

            // The body of the email
            //String Body =
            //    "<p>Caro(a) " + name + ",<br><br>" +
            //    "Recebemos sua solicita��o de Recupera��o de Senha.<br><br>" +
            //    "Para recuper�-la, acesse o link abaixo e redefina a sua senha.<br><br>" +
            //    "<a href='http://msd.embed.com.br/login?hashForgot="+ hash +"'>Link de redefini��o de senha</a><br><br><br>" +
            //    "Obrigado! <br> Universidade MSD</p>";

            String Body = File.ReadAllText(_hostingEnvironment.ContentRootPath + "/Utils/forgetPassword.html");
            Body = Body.Replace("##NOME DO USU�RIO##", name);
            // MUDAR PARA CONFIG -> Body = Body.Replace("##LINK PARA SITE##", "http://msd.embed.com.br/login?hashForgot=" + hash);
            Body = Body.Replace("##LINK PARA SITE##", "http://unimsd.com.br/login?hashForgot=" + hash);


            SendMail(Subject, Body, email);
        }

        public void SendNewUser(string name, string email, string password)
        {
            // The subject line of the email
            const String Subject =
                "Cadastro Realizado. Universidade MSD.";

            // The body of the email
            //String Body =
            //    "<p>Caro(a) " + name + ",<br><br>" +
            //    "Seu cadastro foi realizado com sucesso. Abaixo seus dados para acesso ao sistema.<br><br>" +
            //    "E-mail: " + email + "<br>" +
            //    "Senha: " + password + "<br><br>" +
            //    "Voc� pode realizar a troca de sua senha atrav�s do Meu Perfil, Alterar Senha. <br>" +
            //    "<a href='http://msd.embed.com.br/login'>Clique aqui para acessar o sistema.</a><br><br><br>" +
            //    "Seja Bem Vindo! <br> Universidade MSD</p>";

            String Body = File.ReadAllText(_hostingEnvironment.ContentRootPath + "/Utils/register.html");
            Body = Body.Replace("##NOME DO USU�RIO##", name);
            Body = Body.Replace("##EMAIL DO USUARIO##", email);
            Body = Body.Replace("##SENHA DO USUARIO##", password);
            // MUDAR PARA CONFIG -> Body = Body.Replace("##LINK PARA SITE##", "http://msd.embed.com.br/");
            Body = Body.Replace("##LINK PARA SITE##", "http://unimsd.com.br");

            SendMail(Subject, Body, email);
        }

        private void SendMail(string Subject, string Body, string To)
        {
            // Replace sender@example.com with your "From" address. 
            // This address must be verified with Amazon SES.
            const String FROM = "contato@msd.embed.com.br";
            const String FROMNAME = "Universidade MSD";

            // Replace recipient@example.com with a "To" address. If your account 
            // is still in the sandbox, this address must be verified.
            //const String TO = To;

            // Replace smtp_username with your Amazon SES SMTP user name.
            const String SMTP_USERNAME = "AKIAJZ3CUFM36JEJS3OA";

            // Replace smtp_password with your Amazon SES SMTP user name.
            const String SMTP_PASSWORD = "AoXNg5pdlD7zVT3KVtoshV26UYu/36nTSUqlTA0IfCNE";

            // (Optional) the name of a configuration set to use for this message.
            // If you comment out this line, you also need to remove or comment out
            // the "X-SES-CONFIGURATION-SET" header below.
            //const String CONFIGSET = "ConfigSet";

            // If you're using Amazon SES in a region other than US West (Oregon), 
            // replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP  
            // endpoint in the appropriate Region.
            const String HOST = "email-smtp.us-west-2.amazonaws.com";

            // The port you will connect to on the Amazon SES SMTP endpoint. We
            // are choosing port 587 because we will use STARTTLS to encrypt
            // the connection.
            const int PORT = 587;

            // Create and build a new MailMessage object
            MailMessage message = new MailMessage();
            message.IsBodyHtml = true;
            message.From = new MailAddress(FROM, FROMNAME);
            message.To.Add(new MailAddress(To));
            message.Subject = Subject;
            message.Body = Body;
            // Comment or delete the next line if you are not using a configuration set
            //message.Headers.Add("X-SES-CONFIGURATION-SET", CONFIGSET);

            // Create and configure a new SmtpClient
            SmtpClient client =
                new SmtpClient(HOST, PORT);
            // Pass SMTP credentials
            client.Credentials =
                new NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
            // Enable SSL encryption
            client.EnableSsl = true;

            // Send the email. 
            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("The email was not sent.");
                Console.WriteLine("Error message: " + ex.Message);
            }
        }
    }
}