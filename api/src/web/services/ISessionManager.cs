using System;
using Microsoft.AspNetCore.Http;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Web.Services {

    public interface ISessionManager {
        void Set<T>(HttpContext context, T entity) where T : Entity;
        String Get<T>(HttpContext context) where T : Entity;
    }
}