using System;

namespace UniversidadeMsd.Web.Utils {

    public static class Uuid {

        public static String NewUuid() {
            return Guid.NewGuid().ToString().Replace("-", string.Empty);
        }
    }

}