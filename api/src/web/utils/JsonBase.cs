using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace UniversidadeMsd.Web.Utils
{
    public class JsonBase
    {
        public object Data { get; set; }

        public static JsonResult Response(string content = "", int statusCode = StatusCodes.Status200OK) {
            return new JsonResult(content) { StatusCode = statusCode};
        }

        public static JsonResult Response(JsonBase json, int statusCode = StatusCodes.Status200OK) {
            return new JsonResult(json) { StatusCode = statusCode};
        }

        public static JsonResult ResponseObject<T>(T entity, int statusCode = StatusCodes.Status200OK) where T : class
        {
            return Response(new JsonBase { Data = entity }, statusCode);
        }

        public static JsonResult ResponseList<T>(IEnumerable<T> entities, int statusCode = StatusCodes.Status200OK) where T : class
        {
            return Response(new JsonBase { Data = entities });
        }

        public static JsonResult ResponsePaginated<T>(IEnumerable<T> entities, int? offset, int? limit, int count, int statusCode = 200) where T : class
        {
            return Response(new JsonBase
            {
                Data = new Page
                {
                    Entries = entities,
                    Offset = offset,
                    Limit = limit,
                    Count = count
                }
            }, statusCode);
        }

        public static JsonResult ResponseError(String field, String message, int statusCode = StatusCodes.Status400BadRequest) {
            var parameters = new Dictionary<String, IList<String>>
            {
                {field, new List<String> { message }}
            };
            return ResponseError(parameters, statusCode);
        }

        public static JsonResult ResponseError(IDictionary<String, IList<String>> errors, int statusCode = StatusCodes.Status400BadRequest)
        {
            return Response(new JsonBase {  Data = errors }, statusCode);
        }
    }
}