using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Web.Utils
{
    public class Page
    {
        public int Count { get; set; }
        public int? Offset { get; set; }
        public int? Limit { get; set; }
        public object Entries { get; set; }
    }
}