using System;

namespace UniversidadeMsd.Web.Exceptions {

    public class FileNotSupportedException : Exception {

            public FileNotSupportedException(String message): base(message) { }
            public FileNotSupportedException() { }
    }
}