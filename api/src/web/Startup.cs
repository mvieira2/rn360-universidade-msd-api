using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Persistence.Repositories;
using UniversidadeMsd.Web.Services;
using UniversidadeMsd.Core.Services;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace UniversidadeMsd.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddDistributedMemoryCache();
            services.AddSession(options => {
                options.Cookie.HttpOnly = true;
                options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                options.Cookie.Name = ".UniversidadeMsd.Session";
            });

            // Repositories
            services.AddTransient<IUserRepository, UserRepository>()
                    .AddTransient<IProfileRepository, ProfileRepository>()
                    .AddTransient<ISchoolRepository, SchoolRepository>()
                    .AddTransient<ISegmentRepository, SegmentRepository>()
                    .AddTransient<IAlertRepository, AlertRepository>()
                    .AddTransient<INewsRepository, NewsRepository>()
                    .AddTransient<IRadioRepository, RadioRepository>()
                    .AddTransient<IDownloadRepository, DownloadRepository>()
                    .AddTransient<ILessonRepository, LessonRepository>()
                    .AddTransient<ILessonCommentsRepository, LessonCommentsRepository>()
                    .AddTransient<ILessonExamRepository, LessonExamRepository>()
                    .AddTransient<ILessonQuestionRepository, LessonQuestionRepository>()
                    .AddTransient<ILessonQuestionAnswerRepository, LessonQuestionAnswerRepository>()
                    .AddTransient<IGameficationActionRepository, GameficationActionRepository>()
                    .AddTransient<IGameficationUserHistoryRepository, GameficationUserHistoryRepository>()
                    .AddTransient<ILessonWatchRepository, LessonWatchRepository>();

            // Services
            services.AddSingleton<ISessionManager, SessionManager>()
                    .AddSingleton<IPathProvider, PathProvider>()
                    .AddSingleton<IUploadService, UploadService>()
                    .AddSingleton<IMailService, MailService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseStaticFiles(new StaticFileOptions {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "static")
                ),
                RequestPath = "/static"
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseSession();
            app.UseMvcWithDefaultRoute();
        }
    }
}
