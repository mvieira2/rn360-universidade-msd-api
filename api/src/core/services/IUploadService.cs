using System;

namespace UniversidadeMsd.Core.Services {
    public interface IUploadService
    {
        String SaveFile(String identifier, String file);
    }
}