namespace UniversidadeMsd.Core.Services
{
    public interface IPathProvider
    {
        string MapPath(string path);
    }
}