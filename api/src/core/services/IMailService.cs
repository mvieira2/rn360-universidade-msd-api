using System;

namespace UniversidadeMsd.Core.Services {

    public interface IMailService
    {
        void SendForgetPassword(string name, string email, string hash);
        void SendNewUser(string name, string email, string password);
    }
}