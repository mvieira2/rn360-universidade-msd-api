﻿using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Core.Repositories
{
    public interface IRankRepository : IBaseRepository<Rank>
    {
        List<Rank> Ranking();
    }
}