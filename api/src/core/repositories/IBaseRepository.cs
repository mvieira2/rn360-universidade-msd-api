using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;

using Core = UniversidadeMsd.Core.Repositories.Order;

namespace UniversidadeMsd.Core.Repositories
{
    public interface IBaseRepository<T> where T : Entity
    {
        void SaveOrUpdate(T entity);
        bool Insert(T entity);

        bool Update(T entity);
        bool Update(T oldEntity, T newEntity);

        bool Delete(T entity);
        bool Delete(String identifier);

        T Get(int id);
        IList<T> GetAll(params Order[] orders);
        IList<T> GetAll(int? limit, int? offset, params Order[] orders);

        T FindOne(object properties);
        IList<T> FindAll(object properties, params Order[] orders);
        IList<T> FindAll(object properties, int? offset, int? limit, params Order[] orders);
        int FindCount(object properties);

        IList<TU> RawSql<TU>(string query);

        int Count { get; }
    }
}