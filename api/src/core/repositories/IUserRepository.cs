using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Core.Repositories
{
    public interface IUserRepository : IBaseRepository<User>
    {
        new bool Update(User oldEntity, User newEntity);
        new bool Delete(String registrationNumber);
        User Authenticate(IDictionary<String, object> data);
    }
}