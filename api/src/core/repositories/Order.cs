

namespace UniversidadeMsd.Core.Repositories
{
    public struct Order
    {
        public string Column { get; set; }
        public OrderDirection Direction { get; set; }
    }

    public enum OrderDirection
    {
        Descending,
        Ascending
    }
}