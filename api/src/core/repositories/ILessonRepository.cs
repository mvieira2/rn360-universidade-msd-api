using System;
using System.Collections.Generic;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Core.Repositories
{
    public interface ILessonRepository : IBaseRepository<Lesson>
    {

    }
}