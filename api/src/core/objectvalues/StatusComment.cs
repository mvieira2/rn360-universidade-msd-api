﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace UniversidadeMsd.Core.ObjectValues
{
    [Flags]
    public enum StatusComment
    {
        Pending = 0,
        Approved = 1,
        Disapproved = 2
    }
}
