using System;
using Newtonsoft.Json;

namespace UniversidadeMsd.Core.ObjectValues
{
    public enum LessonOptions
    {
        None = 0,
        FinalTest = 1
    }
}