using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace UniversidadeMsd.Core.ObjectValues
{
    [Flags]
    public enum Profile
    {
        Employee = 0,
        Distributor = 1,
        Administrator = 2048
    }
}