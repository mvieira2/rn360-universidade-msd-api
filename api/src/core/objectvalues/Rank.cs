
namespace UniversidadeMsd.Core.ObjectValues
{
    public struct Rank
    {
        public int Segment { get; set; }
        public int School { get; set; }
    }
}