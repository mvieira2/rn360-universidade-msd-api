using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace UniversidadeMsd.Core.Models
{
    public class News : Entity
    {
        public virtual String Title { get; set; }
        public virtual String Content { get; set; }
        public virtual String Description { get; set; }
        public virtual DateTime PublishedAt { get; set; }
        public virtual Segment Segment { get; set; }
        public virtual IList<School> Schools { get; set; }
        public virtual String Photo { get; set; }
    }
}
