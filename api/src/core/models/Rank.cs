﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace UniversidadeMsd.Core.Models
{
    public class Rank : Entity
    {
        public virtual String UserIdentifier { get; set; }
        public virtual int UserId { get; set; }
        public virtual String UserName { get; set; }
        public virtual String SegmentIdentifier { get; set; }
        public virtual int SegmentId { get; set; }
        public virtual String SegmentName { get; set; }
        public virtual String SchoolIdentifier { get; set; }
        public virtual int SchoolId { get; set; }
        public virtual String SchoolName { get; set; }
        public virtual int Points { get; set; }
    }
}