using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace UniversidadeMsd.Core.Models
{
    public class Download : Entity
    {
        public virtual String Name { get; set; }
        public virtual String Description { get; set; }
        public virtual String DownloadFile { get; set; }
        public virtual Segment Segment { get; set; }
        public virtual IList<School> Schools { get; set; }
    }
}
