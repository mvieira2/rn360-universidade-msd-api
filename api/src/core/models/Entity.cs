using System;
using Newtonsoft.Json;

namespace UniversidadeMsd.Core.Models
{
    public abstract class Entity
    {
        [JsonIgnore]
        public virtual int Id { get; set; }

        [JsonIgnore]
        public virtual DateTime CreatedAt { get; set; }

        public virtual String Identifier { get; set; }

        public override bool Equals(object obj) {
            if (obj == null) return false;
            if (!(obj is Entity)) return false;
            return this.Identifier == ((Entity)obj).Identifier;
        }
    }
}
