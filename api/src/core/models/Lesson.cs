using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UniversidadeMsd.Core.ObjectValues;

namespace UniversidadeMsd.Core.Models
{
    public class Lesson : Entity
    {
        public virtual String Name { get; set; }
        public virtual String Teachers { get; set; }
        public virtual String Description { get; set; }
        public virtual DateTime? StartedAt { get; set; }
        public virtual String Link { get; set; }
        public virtual LessonOptions Options { get; set; }
        public virtual Segment Segment { get; set; }
        public virtual IList<School> Schools { get; set; }
        public virtual String ProfileAccess { get; set; }
        public virtual bool Active { get; set; }
    }

    public class LessonQuestion : Entity
    {
        public virtual String Title { get; set; }
        public virtual Lesson Lesson { get; set; }
        public virtual IList<LessonQuestionAnswer> Answers { get; set; }
    }

    public class LessonQuestionAnswer : Entity
    {
        public virtual String Value { get; set; }
        public virtual bool IsRight { get; set; }
        //public virtual LessonQuestion Question { get; set; }
    }

    public class LessonComments : Entity
    {
        public virtual String Comments { get; set; }
        public virtual int StatusComment { get; set; }
        public virtual Lesson Lesson { get; set; }
        public virtual User User { get; set; }
        public virtual DateTime CreatedAt { get; set; }
    }

    public class LessonExam : Entity
    {
        public virtual int QtyQuestions { get; set; }
        public virtual int QtySuccess { get; set; }
        public virtual int QtyWrong { get; set; }
        public virtual Lesson Lesson { get; set; }
        public virtual User User { get; set; }
        public virtual IList<LessonExamDetail> Details { get; set; }
    }

    public class LessonExamDetail: Entity
    {
        public virtual String Question { get; set; }
        public virtual String Answer { get; set; }
        public virtual bool IsRight { get; set; }
        public virtual LessonExam LessonExam { get; set; }
    }

    public class LessonWatch: Entity
    {
        public virtual Lesson Lesson { get; set; }
        public virtual User User { get; set; }
        public virtual DateTime CreatedAt { get; set; }
    }
}
