﻿using System;
using Newtonsoft.Json;
using UniversidadeMsd.Core.ObjectValues;

namespace UniversidadeMsd.Core.Models
{
    public class User : Entity
    {
        public virtual String Name { get; set; }
        public virtual String Email { get; set; }
        public virtual String Phone { get; set; }
        public virtual Profile Profile { get; set; }
        public virtual String Photo { get; set; }
        public virtual School School { get; set; }
        public virtual Segment Segment { get; set; }
        public virtual String Password { get; set; }
        public virtual String HashNewPassword { get; set; }
        public virtual bool Active { get; set; }

        public virtual Rank Rank { get; set; }
    }

    public class UserSummary : Entity
    {
        public virtual String LessonName { get; set; }
        public virtual String LessonIdentifier { get; set; }
        public virtual Int32 LessonId { get; set; }
        public virtual String SegmentName { get; set; }
        public virtual String SegmentIdentifier { get; set; }
        public virtual Int32 SegmentId { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual String exam { get; set; }
        public virtual Int32 QtyQuestions { get; set; }
        public virtual Int32 QtySuccess { get; set; }

    }
}
