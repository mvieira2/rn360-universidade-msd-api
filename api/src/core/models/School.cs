using System;
using Newtonsoft.Json;

namespace UniversidadeMsd.Core.Models
{
    public class School : Entity
    {
        public virtual String Name { get; set; }
        public virtual String Photo { get; set; }
        public virtual Segment Segment { get; set; }
    }
}
