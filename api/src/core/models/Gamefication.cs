﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace UniversidadeMsd.Core.Models
{
    public class GameficationAction : Entity
    {
        public virtual String ActionName { get; set; }
        public virtual int Points { get; set; }
        public virtual Boolean UniqueUserPoint { get; set; }
    }

    public class GameficationUserHistory : Entity
    {
        public virtual int Points { get; set; }
        public virtual GameficationAction Action { get; set; }
        public virtual User User { get; set; }
        public virtual DateTime CreatedAt { get; set; }
    }

}
