using System;
using NUnit.Framework;
using Moq;

using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Tests.Utils;
using UniversidadeMsd.Core.Services;

namespace UniversidadeMsd.Tests.Web.Controllers
{
    [TestFixture]
    public class DownloadControllerTest
    {
        Mock<IDownloadRepository> downloadRepository;
        Mock<IUploadService> uploadService;
        DownloadController controller;

        const String Identifier = "974ac35c";


        [SetUp]
        public void SetUp() {
            this.downloadRepository = new Mock<IDownloadRepository>();
            this.uploadService = new Mock<IUploadService>();
            this.controller = new DownloadController(this.downloadRepository.Object, this.uploadService.Object);
        }

        /* New Download Methods */

        [Test]
        public void DownloadController_Add_WhenDataIsCorrectShouldPersistTheDownload() {
            // Given
            var form = new AddDownloadForm
            {
                Name = "UniversidadeMsd",
                Description = "<h1>My Content</h1>",
                DownloadFile = "data;image/jpeg;base64",
                Segment = "9cfed",
                Schools = new List<String> { "8cfea" }
            };

            // When
            var result = controller.Add(form);
            var json = result.Value as JsonBase;
            var download = json.Data as Download;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(download.Name, form.Data.Name);

            this.downloadRepository.Verify(m => m.SaveOrUpdate(It.IsAny<Download>()), Times.Once());
        }

        [Test]
        public void DownloadController_Add_WhenInvalidPayloadShouldReturnListOfErrors() {
            // When
            var result = controller.Add(new AddDownloadForm()) as JsonResult;
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            // Then
            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
        }

        [Test]
        public void DownloadController_Add_WhenEntityIsNotFound() {
            // Given
            var form = new AddDownloadForm
            {
                Name = "UniversidadeMsd",
                Description = "<h1>My Content</h1>",
                DownloadFile = "data;image/jpeg;base64",
                Segment = "9cfed",
                Schools = new List<String> { "8cfea" }
            };

            var entityException = new EntityNotFoundException(typeof(Segment));
            downloadRepository.Setup(m => m.SaveOrUpdate(It.IsAny<Download>())).Throws(entityException);

            // When
            var result = controller.Add(form) as JsonResult;
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            // Then
            Assert.AreEqual(400, result.StatusCode);
            Assert.AreEqual("Segment does not exist.", errors["segment"][0]);
        }

        /* Find All Download */
        [Test]
        public void DownloadController_List_ShouldReturnListOfDownload() {
            // Given
            var download = new List<Download>();
            download.Add(new Download {
                Name = "UniversidadeMsd",
                Description = "<h1>My Content</h1>",
                Segment = null,
                Schools = null
            });

            downloadRepository.Setup(m => m.FindAll(It.IsAny<object>(), It.IsAny<int?>(), It.IsAny<int?>()))
                            .Returns(download);
            downloadRepository.Setup(m => m.FindCount(It.IsAny<object>())).Returns(download.Count);

            var form = new SearchForm();

            // When
            var result = controller.List(form);
            var json = result.Value as JsonBase;
            var data = json.Data as Page;
            var entries = data.Entries as IList<Download>;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(1, data.Count);
        }

        /* Delete Download */

        [Test]
        public void DownloadController_Delete_ShouldReturnNotFound() {
            // When
            var result = controller.Delete(Identifier) as StatusCodeResult;

            // Then
            Assert.AreEqual(404, result.StatusCode);
        }

        [Test]
        public void DownloadController_Delete_ShouldDeleteDownload() {
            // Given
            downloadRepository.Setup(m => m.FindOne(It.IsAny<object>())).Returns(new Download());

            // When
            var result = controller.Delete(Identifier) as StatusCodeResult;

            // Then
            Assert.AreEqual(204, result.StatusCode);
            downloadRepository.Verify(m => m.Delete(It.IsAny<Download>()), Times.Once());
        }

        /* Edit Download */
        [Test]
        public void DownloadController_Edit_ShouldReturnNotFoundIfDownloadDoesNotExist() {
            // Given
            var form = new EditDownloadForm
            {
                Name = "UniversidadeMsd",
                Description = "<h1>My Content</h1>",
                DownloadFile = "data;image/jpeg;base64",
                Segment = "9cfed",
                Schools = new List<String> { "8cfea" }
            };

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            Assert.AreEqual(404, result.StatusCode);
            this.downloadRepository.Verify(m => m.Update(It.IsAny<Download>(), It.IsAny<Download>()), Times.Never());
        }

        [Test]
        public void DownloadController_Edit_WhenInvalidPayloadShouldReturnListOfErrors() {
            // Given
            var form = new EditDownloadForm();

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
            this.downloadRepository.Verify(m => m.Update(It.IsAny<Download>(), It.IsAny<Download>()), Times.Never());
        }

        [Test]
        public void DownloadController_Edit_ShouldUpdateObject() {
            // Given
            this.downloadRepository.Setup(m => m.FindOne(It.IsAny<object>())).Returns(new Download());
            var form = new EditDownloadForm
            {
                Name = "UniversidadeMsd",
                Description = "<h1>My Content</h1>",
                DownloadFile = "data;image/jpeg;base64",
                Segment = "9cfed",
                Schools = new List<String> { "8cfea" }
            };

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            var json = result.Value as JsonBase;

            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOf<Download>(json.Data);
            this.downloadRepository.Verify(m => m.Update(It.IsAny<Download>(), It.IsAny<Download>()), Times.Once());
        }
    }
}