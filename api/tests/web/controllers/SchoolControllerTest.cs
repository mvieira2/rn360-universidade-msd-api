using System;
using NUnit.Framework;
using Moq;

using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Tests.Utils;
using UniversidadeMsd.Core.Services;

namespace UniversidadeMsd.Tests.Web.Controllers
{
    [TestFixture]
    public class SchoolControllerTest
    { 
        Mock<ISchoolRepository> schoolRepository;
        Mock<ISegmentRepository> segmentRepository;
        Mock<IUploadService> uploadService;
        SchoolController controller;

        const String Identifier = "974ac35c";

        [SetUp]
        public void SetUp() {
            this.schoolRepository = new Mock<ISchoolRepository>();
            this.uploadService = new Mock<IUploadService>();
            this.segmentRepository = new Mock<ISegmentRepository>();
            this.controller = new SchoolController(this.schoolRepository.Object, this.uploadService.Object, this.segmentRepository.Object);
        }

        /* New School Methods */

        [Test]
        public void SchoolController_Add_WhenDataIsCorrectShouldPersistTheSchool() {
            // Given
            var form = new AddSchoolForm
            {
                Name = "UniversidadeMsd",
                Photo = "base64",
                Segment = "974aadz"
            };

            // When
            var result = controller.Add(form);
            var json = result.Value as JsonBase;
            var school = json.Data as School;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(school.Name, form.Data.Name);

            this.uploadService.Verify(m => m.SaveFile(school.Identifier, form.Photo), Times.Once());
            this.schoolRepository.Verify(m => m.SaveOrUpdate(It.IsAny<School>()), Times.Once());
        }

        [Test]
        public void SchoolController_Add_WhenInvalidPayloadShouldReturnListOfErrors() {
            // When
            var result = controller.Add(new AddSchoolForm()) as JsonResult;
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            // Then
            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
        }

        [Test]
        public void SchoolController_Add_ReturnErrorWhenSegmentWasNotFound() {
            // Given
            var entityNotFoundException = new EntityNotFoundException(typeof(Segment));
            schoolRepository.Setup(m => m.SaveOrUpdate(It.IsAny<School>())).Throws(entityNotFoundException);
            var form = new AddSchoolForm
            {
                Name = "UniversidadeMsd",
                Photo = "base64",
                Segment = "974aadz"
            };

            // When
            var result = controller.Add(form);
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            // Then
            Assert.AreEqual(400, result.StatusCode);
            Assert.AreEqual(1, errors.Count);
            Assert.AreEqual("Segment does not exist.", errors["segment"][0]);
        }

        /* Find All Schools */
        [Test]
        public void SchoolController_List_ShouldReturnListOfSchools() {
            // Given
            var schools = new List<School>
            {
                new School { Name = "UniversidadeMsd" }
            };

            schoolRepository.Setup(m => m.FindAll(It.IsAny<object>(), It.IsAny<int?>(), It.IsAny<int?>()))
                            .Returns(schools);
            schoolRepository.Setup(m => m.FindCount(It.IsAny<object>())).Returns(schools.Count);

            var form = new SearchForm();

            // When
            var result = controller.List(form);
            var json = result.Value as JsonBase;
            var data = json.Data as Page;
            var entries = data.Entries as IList<School>;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(1, data.Count);
            Assert.AreEqual(schools[0].Name, entries[0].Name);
        }

        /* Delete School */

        [Test]
        public void SchoolController_Delete_ShouldReturnNotFound() {
            // When
            var result = controller.Delete(Identifier) as StatusCodeResult;

            // Then
            Assert.AreEqual(404, result.StatusCode);
        }

        [Test]
        public void SchoolController_Delete_ShouldDeleteSchool() {
            // Given
            schoolRepository.Setup(m => m.FindOne(It.IsAny<object>())).Returns(new School());

            // When
            var result = controller.Delete(Identifier) as StatusCodeResult;

            // Then
            Assert.AreEqual(204, result.StatusCode);
            schoolRepository.Verify(m => m.Delete(It.IsAny<School>()), Times.Once());
        }

        /* Edit School */
        [Test]
        public void SchoolController_Edit_ShouldReturnNotFoundIfSchoolDoesNotExist() {
            // Given
            var form = new EditSchoolForm
            {
                Name = "UniversidadeMsd",
                Photo = "base64",
                Segment = "974aadz"
            };

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            Assert.AreEqual(404, result.StatusCode);
            this.uploadService.Verify(m => m.SaveFile(It.IsAny<String>(), form.Photo), Times.Never());
            this.schoolRepository.Verify(m => m.Update(It.IsAny<School>(), It.IsAny<School>()), Times.Never());
        }

        [Test]
        public void SchoolController_Edit_WhenInvalidPayloadShouldReturnListOfErrors() {
            // Given
            var form = new EditSchoolForm();

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
            this.uploadService.Verify(m => m.SaveFile(It.IsAny<String>(), form.Photo), Times.Never());
            this.schoolRepository.Verify(m => m.Update(It.IsAny<School>(), It.IsAny<School>()), Times.Never());
        }

        [Test]
        public void SchoolController_Edit_ShouldUpdateObject() {
            // Given
            this.schoolRepository.Setup(m => m.FindOne(It.IsAny<object>())).Returns(new School());
            var form = new EditSchoolForm
            {
                Name = "UniversidadeMsd",
                Photo = "base64",
                Segment = "974aadz"
            };

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            var json = result.Value as JsonBase;

            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOf<School>(json.Data);
            this.uploadService.Verify(m => m.SaveFile(It.IsAny<String>(), form.Photo), Times.Once());
            this.schoolRepository.Verify(m => m.Update(It.IsAny<School>(), It.IsAny<School>()), Times.Once());
        }
    }
}