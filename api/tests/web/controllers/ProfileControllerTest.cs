using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

using NUnit.Framework;
using Moq;

using UniversidadeMsd.Web.Controllers;
using UniversidadeMsd.Core.Repositories;


namespace UniversidadeMsd.Tests.Web.Controllers
{
    [TestFixture]
    public class ProfileControllerTest
    {
        Mock<IProfileRepository> profileRepository;
        ProfileController controller;

        [SetUp]
        public void SetUp() {
            this.profileRepository = new Mock<IProfileRepository>();
            this.controller = new ProfileController(this.profileRepository.Object);
        }

        /*
            List Methods
         */

        [Test]
        public void ProfileController_List_ShouldShowAllProfiles() {
            // When
            var result = controller.List() as JsonResult;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            profileRepository.Verify(m => m.GetAll(), Times.Once());
        }
    }
}