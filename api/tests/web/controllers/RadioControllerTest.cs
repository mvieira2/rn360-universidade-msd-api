using System;
using NUnit.Framework;
using Moq;

using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Tests.Utils;
using UniversidadeMsd.Core.Services;

namespace UniversidadeMsd.Tests.Web.Controllers
{
    [TestFixture]
    public class RadioControllerTest
    {
        Mock<IRadioRepository> radioRepository;
        Mock<IUploadService> uploadService;
        RadioController controller;

        const String Identifier = "974ac35c";


        [SetUp]
        public void SetUp() {
            this.radioRepository = new Mock<IRadioRepository>();
            this.uploadService = new Mock<IUploadService>();
            this.controller = new RadioController(this.radioRepository.Object, this.uploadService.Object);
        }

        /* New Radio Methods */

        [Test]
        public void RadioController_Add_WhenDataIsCorrectShouldPersistTheRadio() {
            // Given
            var form = new AddRadioForm
            {
                Name = "UniversidadeMsd",
                Description = "<h1>My Content</h1>",
                AudioFile = "data;image/jpeg;base64",
                Segment = "9cfed",
                Schools = new List<String> { "8cfea" }
            };

            // When
            var result = controller.Add(form);
            var json = result.Value as JsonBase;
            var radio = json.Data as Radio;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(radio.Name, form.Data.Name);

            this.radioRepository.Verify(m => m.SaveOrUpdate(It.IsAny<Radio>()), Times.Once());
        }

        [Test]
        public void RadioController_Add_WhenInvalidPayloadShouldReturnListOfErrors() {
            // When
            var result = controller.Add(new AddRadioForm()) as JsonResult;
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            // Then
            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
        }

        [Test]
        public void RadioController_Add_WhenEntityIsNotFound() {
            // Given
            var form = new AddRadioForm
            {
                Name = "UniversidadeMsd",
                Description = "<h1>My Content</h1>",
                AudioFile = "data;image/jpeg;base64",
                Segment = "9cfed",
                Schools = new List<String> { "8cfea" }
            };

            var entityException = new EntityNotFoundException(typeof(Segment));
            radioRepository.Setup(m => m.SaveOrUpdate(It.IsAny<Radio>())).Throws(entityException);

            // When
            var result = controller.Add(form) as JsonResult;
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            // Then
            Assert.AreEqual(400, result.StatusCode);
            Assert.AreEqual("Segment does not exist.", errors["segment"][0]);
        }

        /* Find All Radio */
        [Test]
        public void RadioController_List_ShouldReturnListOfRadio() {
            // Given
            var radio = new List<Radio>();
            radio.Add(new Radio {
                Name = "UniversidadeMsd",
                Description = "<h1>My Content</h1>",
                Segment = null,
                Schools = null
            });

            radioRepository.Setup(m => m.FindAll(It.IsAny<object>(), It.IsAny<int?>(), It.IsAny<int?>()))
                            .Returns(radio);
            radioRepository.Setup(m => m.FindCount(It.IsAny<object>())).Returns(radio.Count);

            var form = new SearchForm();

            // When
            var result = controller.List(form);
            var json = result.Value as JsonBase;
            var data = json.Data as Page;
            var entries = data.Entries as IList<Radio>;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(1, data.Count);
        }

        /* Delete Radio */

        [Test]
        public void RadioController_Delete_ShouldReturnNotFound() {
            // When
            var result = controller.Delete(Identifier) as StatusCodeResult;

            // Then
            Assert.AreEqual(404, result.StatusCode);
        }

        [Test]
        public void RadioController_Delete_ShouldDeleteRadio() {
            // Given
            radioRepository.Setup(m => m.FindOne(It.IsAny<object>())).Returns(new Radio());

            // When
            var result = controller.Delete(Identifier) as StatusCodeResult;

            // Then
            Assert.AreEqual(204, result.StatusCode);
            radioRepository.Verify(m => m.Delete(It.IsAny<Radio>()), Times.Once());
        }

        /* Edit Radio */
        [Test]
        public void RadioController_Edit_ShouldReturnNotFoundIfRadioDoesNotExist() {
            // Given
            var form = new EditRadioForm
            {
                Name = "UniversidadeMsd",
                Description = "<h1>My Content</h1>",
                AudioFile = "data;image/jpeg;base64",
                Segment = "9cfed",
                Schools = new List<String> { "8cfea" }
            };

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            Assert.AreEqual(404, result.StatusCode);
            this.radioRepository.Verify(m => m.Update(It.IsAny<Radio>(), It.IsAny<Radio>()), Times.Never());
        }

        [Test]
        public void RadioController_Edit_WhenInvalidPayloadShouldReturnListOfErrors() {
            // Given
            var form = new EditRadioForm();

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
            this.radioRepository.Verify(m => m.Update(It.IsAny<Radio>(), It.IsAny<Radio>()), Times.Never());
        }

        [Test]
        public void RadioController_Edit_ShouldUpdateObject() {
            // Given
            this.radioRepository.Setup(m => m.FindOne(It.IsAny<object>())).Returns(new Radio());
            var form = new EditRadioForm
            {
                Name = "UniversidadeMsd",
                Description = "<h1>My Content</h1>",
                AudioFile = "data;image/jpeg;base64",
                Segment = "9cfed",
                Schools = new List<String> { "8cfea" }
            };

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            var json = result.Value as JsonBase;

            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOf<Radio>(json.Data);
            this.radioRepository.Verify(m => m.Update(It.IsAny<Radio>(), It.IsAny<Radio>()), Times.Once());
        }
    }
}