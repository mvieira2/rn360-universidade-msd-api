using System;
using NUnit.Framework;
using Moq;

using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Tests.Utils;

namespace UniversidadeMsd.Tests.Web.Controllers
{
    [TestFixture]
    public class NewsControllerTest
    {
        Mock<INewsRepository> newsRepository;
        NewsController controller;

        const String Identifier = "974ac35c";


        [SetUp]
        public void SetUp() {
            this.newsRepository = new Mock<INewsRepository>();
            this.controller = new NewsController(this.newsRepository.Object);
        }

        /* New News Methods */

        [Test]
        public void NewsController_Add_WhenDataIsCorrectShouldPersistTheNews() {
            // Given
            var form = new AddNewsForm
            {
                Title = "UniversidadeMsd",
                Content = "<h1>My Content</h1>",
                Segment = "9cfed",
                Schools = new List<String> { "8cfea" }
            };

            // When
            var result = controller.Add(form);
            var json = result.Value as JsonBase;
            var news = json.Data as News;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(news.Title, form.Data.Title);

            this.newsRepository.Verify(m => m.SaveOrUpdate(It.IsAny<News>()), Times.Once());
        }

        [Test]
        public void NewsController_Add_WhenInvalidPayloadShouldReturnListOfErrors() {
            // When
            var result = controller.Add(new AddNewsForm()) as JsonResult;
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            // Then
            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
        }

        [Test]
        public void NewsController_Add_WhenEntityIsNotFound() {
            // Given
            var form = new AddNewsForm
            {
                Title = "UniversidadeMsd",
                Content = "<h1>My Content</h1>",
                Segment = "9cfed",
                Schools = new List<String> { "8cfea" }
            };

            var entityException = new EntityNotFoundException(typeof(Segment));
            newsRepository.Setup(m => m.SaveOrUpdate(It.IsAny<News>())).Throws(entityException);

            // When
            var result = controller.Add(form) as JsonResult;
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            // Then
            Assert.AreEqual(400, result.StatusCode);
            Assert.AreEqual("Segment does not exist.", errors["segment"][0]);
        }

        /* Find All News */
        [Test]
        public void NewsController_List_ShouldReturnListOfNews() {
            // Given
            var news = new List<News>();
            news.Add(new News {
                Title = "UniversidadeMsd",
                Content = "<h1>My Content</h1>",
                Segment = null,
                Schools = null
            });

            newsRepository.Setup(m => m.FindAll(It.IsAny<object>(), It.IsAny<int?>(), It.IsAny<int?>()))
                            .Returns(news);
            newsRepository.Setup(m => m.FindCount(It.IsAny<object>())).Returns(news.Count);

            var form = new SearchForm();

            // When
            var result = controller.List(form);
            var json = result.Value as JsonBase;
            var data = json.Data as Page;
            var entries = data.Entries as IList<News>;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(1, data.Count);
        }

        /* Delete News */

        [Test]
        public void NewsController_Delete_ShouldReturnNotFound() {
            // When
            var result = controller.Delete(Identifier) as StatusCodeResult;

            // Then
            Assert.AreEqual(404, result.StatusCode);
        }

        [Test]
        public void NewsController_Delete_ShouldDeleteNews() {
            // Given
            newsRepository.Setup(m => m.FindOne(It.IsAny<object>())).Returns(new News());

            // When
            var result = controller.Delete(Identifier) as StatusCodeResult;

            // Then
            Assert.AreEqual(204, result.StatusCode);
            newsRepository.Verify(m => m.Delete(It.IsAny<News>()), Times.Once());
        }

        /* Edit News */
        [Test]
        public void NewsController_Edit_ShouldReturnNotFoundIfNewsDoesNotExist() {
            // Given
            var form = new EditNewsForm
            {
                Title = "UniversidadeMsd",
                Content = "<h1>My Content</h1>",
                Segment = "9cfed",
                Schools = new List<String> { "8cfea" }
            };

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            Assert.AreEqual(404, result.StatusCode);
            this.newsRepository.Verify(m => m.Update(It.IsAny<News>(), It.IsAny<News>()), Times.Never());
        }

        [Test]
        public void NewsController_Edit_WhenInvalidPayloadShouldReturnListOfErrors() {
            // Given
            var form = new EditNewsForm();

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
            this.newsRepository.Verify(m => m.Update(It.IsAny<News>(), It.IsAny<News>()), Times.Never());
        }

        [Test]
        public void NewsController_Edit_ShouldUpdateObject() {
            // Given
            this.newsRepository.Setup(m => m.FindOne(It.IsAny<object>())).Returns(new News());
            var form = new EditNewsForm
            {
                Title = "UniversidadeMsd",
                Content = "<h1>My Content</h1>",
                Segment = "9cfed",
                Schools = new List<String> { "8cfea" }
            };

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            var json = result.Value as JsonBase;

            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOf<News>(json.Data);
            this.newsRepository.Verify(m => m.Update(It.IsAny<News>(), It.IsAny<News>()), Times.Once());
        }
    }
}