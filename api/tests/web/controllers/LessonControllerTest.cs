using System;
using NUnit.Framework;
using Moq;

using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Tests.Utils;

namespace UniversidadeMsd.Tests.Web.Controllers
{
    [TestFixture]
    public class LessonControllerTest
    {
        Mock<ILessonRepository> lessonRepository;
        LessonController controller;

        const String Identifier = "974ac35c";


        [SetUp]
        public void SetUp() {
            this.lessonRepository = new Mock<ILessonRepository>();
            this.controller = new LessonController(this.lessonRepository.Object);
        }

        /* New Lesson Methods */

        [Test]
        public void LessonController_Add_WhenDataIsCorrectShouldPersistTheLesson() {
            // Given
            var form = new AddLessonForm
            {
                Name = "UniversidadeMsd",
                Options = 1,
                Segment = "87efc",
                Schools = new List<String> { "87efc" },
                ProfileAccess = new List<int> { 0 , 2048 }
            };

            // When
            var result = controller.Add(form);
            var json = result.Value as JsonBase;
            var lesson = json.Data as Lesson;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(lesson.Name, form.Data.Name);

            this.lessonRepository.Verify(m => m.SaveOrUpdate(It.IsAny<Lesson>()), Times.Once());
        }

        [Test]
        public void LessonController_Add_WhenInvalidPayloadShouldReturnListOfErrors() {
            // When
            var result = controller.Add(new AddLessonForm()) as JsonResult;
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            // Then
            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
        }

        /* Find All Lessons */
        [Test]
        public void LessonController_List_ShouldReturnListOfLessons() {
            // Given
            var lessons = new List<Lesson>
            {
                new Lesson { Name = "UniversidadeMsd" }
            };

            lessonRepository.Setup(m => m.FindAll(It.IsAny<object>(), It.IsAny<int?>(), It.IsAny<int?>()))
                            .Returns(lessons);
            lessonRepository.Setup(m => m.FindCount(It.IsAny<object>())).Returns(lessons.Count);

            var form = new SearchForm();

            // When
            var result = controller.List(form);
            var json = result.Value as JsonBase;
            var data = json.Data as Page;
            var entries = data.Entries as IList<Lesson>;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(1, data.Count);
            Assert.AreEqual(lessons[0].Name, entries[0].Name);
        }

        /* Delete Lesson */

        [Test]
        public void LessonController_Delete_ShouldReturnNotFound() {
            // When
            var result = controller.Delete(Identifier) as StatusCodeResult;

            // Then
            Assert.AreEqual(404, result.StatusCode);
        }

        [Test]
        public void LessonController_Delete_ShouldDeleteLesson() {
            // Given
            lessonRepository.Setup(m => m.FindOne(It.IsAny<object>())).Returns(new Lesson());

            // When
            var result = controller.Delete(Identifier) as StatusCodeResult;

            // Then
            Assert.AreEqual(204, result.StatusCode);
            lessonRepository.Verify(m => m.Delete(It.IsAny<Lesson>()), Times.Once());
        }

        // /* Edit Lesson */
        [Test]
        public void LessonController_Edit_ShouldReturnNotFoundIfLessonDoesNotExist() {
            // Given
            var form = new EditLessonForm
            {
                Name = "UniversidadeMsd",
                Options = 1,
                Segment = "87efc",
                Schools = new List<String> { "87efc" },
                ProfileAccess = new List<int> { 0 , 2048 }
            };

            // When
            var result = controller.Edit(Identifier, form) as StatusCodeResult;

            // Then
            Assert.AreEqual(404, result.StatusCode);
            this.lessonRepository.Verify(m => m.Update(It.IsAny<Lesson>(), It.IsAny<Lesson>()), Times.Never());
        }

        [Test]
        public void LessonController_Edit_WhenInvalidPayloadShouldReturnListOfErrors() {
            // Given
            var form = new EditLessonForm();

            // When
            var result = controller.Edit(Identifier, form) as JsonResult;

            // Then
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
            this.lessonRepository.Verify(m => m.Update(It.IsAny<Lesson>(), It.IsAny<Lesson>()), Times.Never());
        }

        [Test]
        public void LessonController_Edit_ShouldUpdateObject() {
            // Given
            this.lessonRepository.Setup(m => m.FindOne(It.IsAny<object>())).Returns(new Lesson());
            var form = new EditLessonForm
            {
                Name = "UniversidadeMsd",
                Options = 1,
                Segment = "87efc",
                Schools = new List<String> { "87efc" },
                ProfileAccess = new List<int> { 0 , 2048 }
            };

            // When
            var result = controller.Edit(Identifier, form) as JsonResult;

            // Then
            var json = result.Value as JsonBase;

            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOf<Lesson>(json.Data);
            this.lessonRepository.Verify(m => m.Update(It.IsAny<Lesson>(), It.IsAny<Lesson>()), Times.Once());
        }
    }
}