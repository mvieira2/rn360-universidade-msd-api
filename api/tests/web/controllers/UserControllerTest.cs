using System;
using NUnit.Framework;
using Moq;

using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Tests.Utils;
using Microsoft.AspNetCore.Http;
using System.Text;
using UniversidadeMsd.Web.Services;
using UniversidadeMsd.Core.ObjectValues;
using UniversidadeMsd.Core.Services;

namespace UniversidadeMsd.Tests.Web.Controllers
{
    [TestFixture]
    public class UserControllerTest
    {
        Mock<ISessionManager> sessionManager;
        Mock<IUploadService> uploadService;
        Mock<IUserRepository> userRepository;
        Mock<IMailService> mailService;
        UserController controller;
        Mock<ISegmentRepository> segmentRepository;
        Mock<ISchoolRepository> schoolRepository;

        const String Identifier = "974ac35c";

        [SetUp]
        public void SetUp() {
            userRepository = new Mock<IUserRepository>();
            sessionManager = new Mock<ISessionManager>();
            uploadService = new Mock<IUploadService>();
            mailService = new Mock<IMailService>();
            segmentRepository = new Mock<ISegmentRepository>();
            schoolRepository = new Mock<ISchoolRepository>();

            controller = new UserController(userRepository.Object, sessionManager.Object, uploadService.Object, mailService.Object, segmentRepository.Object, schoolRepository.Object);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
        }

        /* New User Methods */

        [Test]
        public void UserController_Add_WhenInvalidPayloadShouldReturnListOfErrors() {
            // When
            var result = controller.Add(new AddUserForm()) as JsonResult;
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            // Then
            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
        }

        [Test]
        public void UserController_Add_WhenDataIsCorrectShouldPersistTheUser() {
            // Given
            var form = new AddUserForm {
                Name = "UniversidadeMsd", Email = "universidade@gmail.com",
                Phone = "11999999999", Profile = (int)Profile.Administrator
            };

            // When
            var result = controller.Add(form);
            var json = result.Value as JsonBase;
            var user = json.Data as User;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(user.Name, form.Data.Name);

            this.userRepository.Verify(m => m.SaveOrUpdate(It.IsAny<User>()), Times.Once());
        }

        [Test]
        public void UserController_Add_SaveFileIfPhotoIsProvided() {
            // Given
            var form = new AddUserForm {
                Name = "UniversidadeMsd", Email = "universidade@gmail.com",
                Phone = "11999999999", Profile = (int)Profile.Administrator,
                Photo = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAeAB4AAD"
            };

            // When
            var result = controller.Add(form);
            var json = result.Value as JsonBase;
            var user = json.Data as User;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(user.Name, form.Data.Name);

            this.uploadService.Verify(m => m.SaveFile(user.Identifier, form.Photo), Times.Once());
            this.userRepository.Verify(m => m.SaveOrUpdate(It.IsAny<User>()), Times.Once());
        }

        [Test]
        public void UserController_Add_ReturnErrorWhenUserAlreadyExists() {
            // Given
            userRepository.Setup(m => m.SaveOrUpdate(It.IsAny<User>())).Throws<DuplicatedConstraintException>();
            var form = new AddUserForm {
                Name = "UniversidadeMsd", Email = "universidade@gmail.com",
                Phone = "11999999999", Profile = (int)Profile.Administrator
            };

            // When
            var result = controller.Add(form);
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            // Then
            Assert.AreEqual(400, result.StatusCode);
            Assert.AreEqual(1, errors.Count);
            Assert.AreEqual("Email already exists.", errors["email"][0]);
        }

        [Test]
        public void UserController_Add_ReturnErrorWhenEntityWasNotFound() {
            // Given
            var entityException = new EntityNotFoundException(typeof(School));
            userRepository.Setup(m => m.SaveOrUpdate(It.IsAny<User>())).Throws(entityException);
            var form = new AddUserForm {
                Name = "UniversidadeMsd", Email = "universidade@gmail.com",
                Phone = "11999999999", Profile = (int)Profile.Administrator
            };

            // When
            var result = controller.Add(form);
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            // Then
            Assert.AreEqual(400, result.StatusCode);
            Assert.AreEqual(1, errors.Count);
            Assert.AreEqual("School does not exist.", errors["school"][0]);
        }

        /* Find All Users */
        [Test]
        public void UserController_List_ShouldReturnListOfUsers() {
            // Given
            var users = new List<User>
            {
                new User { Name = "UniversidadeMsd" }
            };

            userRepository.Setup(m => m.FindAll(It.IsAny<object>(), It.IsAny<int?>(), It.IsAny<int?>()))
                          .Returns(users);
            userRepository.Setup(m => m.FindCount(It.IsAny<object>())).Returns(users.Count);

            var form = new SearchForm();

            // When
            var result = controller.List(form);
            var json = result.Value as JsonBase;
            var data = json.Data as Page;
            var entries = data.Entries as IList<User>;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(1, data.Count);
            Assert.AreEqual(users[0].Name, entries[0].Name);
        }

        /* Delete User */

        [Test]
        public void UserController_Delete_ShouldReturnNotFound() {
            // When
            var result = controller.Delete(Identifier) as StatusCodeResult;

            // Then
            Assert.AreEqual(404, result.StatusCode);
        }

        [Test]
        public void UserController_Delete_ShouldDeleteUser() {
            // Given
            userRepository.Setup(m => m.FindOne(It.IsAny<object>())).Returns(new User());

            // When
            var result = controller.Delete(Identifier) as StatusCodeResult;

            // Then
            Assert.AreEqual(204, result.StatusCode);
            userRepository.Verify(m => m.Delete(It.IsAny<User>()), Times.Once());
        }

        /*
            Edit User
         */
        [Test]
        public void UserController_Edit_ShouldReturnNotFoundIfUserDoesNotExist() {
            // Given
            var form = new EditUserForm {
                Name = "UniversidadeMsd",
                Phone = "11999999999", Password = "mysekret",
                Email = "universidademsd@gmail.com",
                Profile = (int)Profile.Administrator
            };

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            Assert.AreEqual(404, result.StatusCode);
            this.userRepository.Verify(m => m.Update(It.IsAny<User>(), It.IsAny<User>()), Times.Never());
        }

        [Test]
        public void UserController_Edit_WhenInvalidPayloadShouldReturnListOfErrors() {
            // Given
            var form = new EditUserForm {
                Name = "UniversidadeMsd",
                Phone = "11999999999", Profile = 3,
                Password = "mysekret"
            };

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
            this.userRepository.Verify(m => m.Update(It.IsAny<User>(), It.IsAny<User>()), Times.Never());
        }

        [Test]
        public void UserController_Edit_ShouldUpdateObject() {
            // Given
            this.userRepository.Setup(m => m.FindOne(It.IsAny<object>())).Returns(new User());
            var form = new EditUserForm {
                Name = "UniversidadeMsd",
                Phone = "11999999999", Profile = (int)Profile.Administrator,
                Password = "mysekret", Email = "univerisademsd@gmail.com"
            };

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            var json = result.Value as JsonBase;

            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOf<User>(json.Data);
            this.userRepository.Verify(m => m.Update(It.IsAny<User>(), It.IsAny<User>()), Times.Once());
        }

        /* Authentication */
        [Test]
        public void UserController_Auth_ShouldLogin() {
            // Given
            this.userRepository.Setup(m => m.Authenticate(It.IsAny<IDictionary<String, object>>()))
                               .Returns(new User { Identifier = Identifier});
            var form = new AuthForm { Password = "mysekret", Email = "univerisademsd@gmail.com" };

            // When
            var result = controller.Auth(form) as JsonResult;

            // Then
            var json = result.Value as JsonBase;

            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOf<User>(json.Data);
            sessionManager.Verify(s => s.Set(It.IsAny<HttpContext>(), It.IsAny<User>()));
        }

        [Test]
        public void UserController_Auth_ShouldReturnErrors() {
            // Given
            var form = new AuthForm();

            // When
            var result = controller.Auth(form) as JsonResult;

            // Then
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
            sessionManager.Verify(s => s.Set(It.IsAny<HttpContext>(), It.IsAny<User>()), Times.Never());
        }

        [Test]
        public void UserController_Auth_ShouldReturnUnauthorized() {
            // Given
            var form = new AuthForm { Password = "mysekret", Email = "univerisademsd@gmail.com" };

            // When
            var result = controller.Auth(form) as StatusCodeResult;

            // Then
            Assert.AreEqual(401, result.StatusCode);
            sessionManager.Verify(s => s.Set(It.IsAny<HttpContext>(), It.IsAny<User>()), Times.Never());
        }

        [Test]
        public void UserController_Auth_IfHasSessionShouldReturnUser() {
            // Given
            sessionManager.Setup(s => s.Get<User>(It.IsAny<HttpContext>())).Returns(Identifier);

            // When
            var result = controller.Auth() as JsonResult;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            userRepository.Verify(u => u.FindOne(It.IsAny<Object>()), Times.Once());
        }

        [Test]
        public void UserController_Auth_IfHasNotSessionShouldReturnUser() {
            // When
            var result = controller.Auth() as StatusCodeResult;

            // Then
            Assert.AreEqual(401, result.StatusCode);
            userRepository.Verify(u => u.FindOne(It.IsAny<object>()), Times.Never());
        }
    }
}