using System;
using NUnit.Framework;
using Moq;

using UniversidadeMsd.Web.Forms;
using UniversidadeMsd.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using UniversidadeMsd.Web.Utils;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Persistence.Exceptions;
using UniversidadeMsd.Tests.Utils;

namespace UniversidadeMsd.Tests.Web.Controllers
{
    [TestFixture]
    public class AlertControllerTest
    {
        Mock<IAlertRepository> alertRepository;
        AlertController controller;

        const String Identifier = "974ac35c";


        [SetUp]
        public void SetUp() {
            this.alertRepository = new Mock<IAlertRepository>();
            this.controller = new AlertController(this.alertRepository.Object);
        }

        /* New Alert Methods */

        [Test]
        public void AlertController_Add_WhenDataIsCorrectShouldPersistTheAlert() {
            // Given
            var form = new AddAlertForm
            {
                Name = "UniversidadeMsd",
                Segment = "9cfed",
                Schools = new List<String> { "8cfea" }
            };

            // When
            var result = controller.Add(form);
            var json = result.Value as JsonBase;
            var alert = json.Data as Alert;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(alert.Name, form.Data.Name);

            this.alertRepository.Verify(m => m.SaveOrUpdate(It.IsAny<Alert>()), Times.Once());
        }

        [Test]
        public void AlertController_Add_WhenInvalidPayloadShouldReturnListOfErrors() {
            // When
            var result = controller.Add(new AddAlertForm()) as JsonResult;
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            // Then
            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
        }

        [Test]
        public void AlertController_Add_WhenEntityIsNotFound() {
            // Given
            var form = new AddAlertForm
            {
                Name = "UniversidadeMsd",
                Segment = "9cfed",
                Schools = new List<String> { "8cfea" }
            };

            var entityException = new EntityNotFoundException(typeof(Segment));
            alertRepository.Setup(m => m.SaveOrUpdate(It.IsAny<Alert>())).Throws(entityException);

            // When
            var result = controller.Add(form) as JsonResult;
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            // Then
            Assert.AreEqual(400, result.StatusCode);
            Assert.AreEqual("Segment does not exist.", errors["segment"][0]);
        }

        /* Find All Alerts */
        [Test]
        public void AlertController_List_ShouldReturnListOfAlerts() {
            // Given
            var alerts = new List<Alert>
            {
                new Alert { Name = "UniversidadeMsd" }
            };

            alertRepository.Setup(m => m.FindAll(It.IsAny<object>(), It.IsAny<int?>(), It.IsAny<int?>()))
                            .Returns(alerts);
            alertRepository.Setup(m => m.FindCount(It.IsAny<object>())).Returns(alerts.Count);

            var form = new SearchForm();

            // When
            var result = controller.List(form);
            var json = result.Value as JsonBase;
            var data = json.Data as Page;
            var entries = data.Entries as IList<Alert>;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(1, data.Count);
            Assert.AreEqual(alerts[0].Name, entries[0].Name);
        }

        /* Delete Alert */

        [Test]
        public void AlertController_Delete_ShouldReturnNotFound() {
            // When
            var result = controller.Delete(Identifier) as StatusCodeResult;

            // Then
            Assert.AreEqual(404, result.StatusCode);
        }

        [Test]
        public void AlertController_Delete_ShouldDeleteAlert() {
            // Given
            alertRepository.Setup(m => m.FindOne(It.IsAny<object>())).Returns(new Alert());

            // When
            var result = controller.Delete(Identifier) as StatusCodeResult;

            // Then
            Assert.AreEqual(204, result.StatusCode);
            alertRepository.Verify(m => m.Delete(It.IsAny<Alert>()), Times.Once());
        }

        /* Edit Alert */
        [Test]
        public void AlertController_Edit_ShouldReturnNotFoundIfAlertDoesNotExist() {
            // Given
            var form = new EditAlertForm
            {
                Name = "UniversidadeMsd",
                Segment = "9ecf",
                Schools = new List<String> { "8abcd" }
            };

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            Assert.AreEqual(404, result.StatusCode);
            this.alertRepository.Verify(m => m.Update(It.IsAny<Alert>(), It.IsAny<Alert>()), Times.Never());
        }

        [Test]
        public void AlertController_Edit_WhenInvalidPayloadShouldReturnListOfErrors() {
            // Given
            var form = new EditAlertForm();

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            var json = result.Value as JsonBase;
            var errors = json.Data as IDictionary<String, IList<String>>;

            Assert.AreEqual(400, result.StatusCode);
            Assert.Greater(errors.Count, 0);
            this.alertRepository.Verify(m => m.Update(It.IsAny<Alert>(), It.IsAny<Alert>()), Times.Never());
        }

        [Test]
        public void AlertController_Edit_ShouldUpdateObject() {
            // Given
            this.alertRepository.Setup(m => m.FindOne(It.IsAny<object>())).Returns(new Alert());
            var form = new EditAlertForm
            {
                Name = "UniversidadeMsd",
                Segment = "9ecf",
                Schools = new List<String> { "8abcd" }
            };

            // When
            var result = controller.Edit(Identifier, form);

            // Then
            var json = result.Value as JsonBase;

            Assert.AreEqual(200, result.StatusCode);
            Assert.IsInstanceOf<Alert>(json.Data);
            this.alertRepository.Verify(m => m.Update(It.IsAny<Alert>(), It.IsAny<Alert>()), Times.Once());
        }
    }
}