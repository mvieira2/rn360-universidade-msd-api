using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

using NUnit.Framework;
using Moq;

using UniversidadeMsd.Web.Controllers;
using UniversidadeMsd.Core.Repositories;
using UniversidadeMsd.Core.Models;

namespace UniversidadeMsd.Tests.Web.Controllers
{
    [TestFixture]
    public class SegmentControllerTest
    {
        Mock<ISegmentRepository> segmentRepository;
        SegmentController controller;

        [SetUp]
        public void SetUp() {
            this.segmentRepository = new Mock<ISegmentRepository>();
            this.controller = new SegmentController(this.segmentRepository.Object);
        }

        /* List Methods */
        [Test]
        public void SegmentController_List_ShouldReturnAllSegments() {
            // Given
            segmentRepository.Setup(m => m.GetAll()).Returns(new List<Segment>());

            // When
            var result = controller.List() as JsonResult;

            // Then
            Assert.AreEqual(200, result.StatusCode);
            segmentRepository.Verify(m => m.GetAll(), Times.Once());
        }
    }
}