using System;
using NUnit.Framework;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.ObjectValues;
using UniversidadeMsd.Web.Forms;

namespace UniversidadeMsd.Tests.Web.Forms
{
    [TestFixture]
    public class NewSchoolFormTest
    {
        [TestCase("Name",  "9cfed7", 0)]
        [TestCase("",  "9cfed7", 1)]
        [TestCase("Name", "", 1)]
        [TestCase("", "", 2)]
        public void NewSchoolForm_ShouldHasErrorIfValuesWereNotProvided(String name, String segment, Int32 expected) {
            var form = new AddSchoolForm
            {
                Name = name,
                Segment = segment,
            };
            Assert.AreEqual(form.IsValid, expected == 0);
            Assert.AreEqual(expected, form.Errors.Count);
        }

        [Test]
        public void NewSchoolForm_ShouldReturnValidForm() {
            var form = new AddSchoolForm
            {
                Name = "Name",
                Photo = "photo",
                Segment = "9cfed7",
            };
            Assert.IsTrue(form.IsValid);
        }
    }
}