using System;
using NUnit.Framework;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.ObjectValues;
using UniversidadeMsd.Web.Forms;

namespace UniversidadeMsd.Tests.Web.Forms
{
    [TestFixture]
    public class NewUserFormTest
    {
        [TestCase("UserName", "", "", 2)]
        [TestCase("", "email@gmail.com", "", 2)]
        [TestCase("", "", "UserPhone", 2)]
        [TestCase("", "", "", 3)]
        public void NewUserForm_ShouldHasErrorIfValuesWereNotProvided(String name, String email, String phone, Int32 expected) {
            var form = new AddUserForm { Name = name, Email = email, Phone = phone, Profile = (int)Profile.Administrator };
            Assert.IsFalse(form.IsValid);
            Assert.AreEqual(expected, form.Errors.Count);
        }

        [Test]
        public void NewUserForm_ShouldReturnValidForm() {
            var form = new AddUserForm {
                Name = "Name",
                Email = "email@gmail.com",
                Phone = "11999999999",
                Segment = "9efc752",
                School = "9efc752",
            };
            Assert.IsTrue(form.IsValid);
        }

        [Test]
        public void NewUserForm_ShouldReturnFalseWhenEmailIsInvalid() {
            var form = new AddUserForm { Email = "email" };

            Assert.IsFalse(form.IsValid);
            Assert.AreEqual("Email is not valid.", form.Errors["email"][0]);
        }

        [Test]
        public void NewUserForm_ShouldReturnFalseWhenSegmentAndSchoolWasNotSupplied() {
            var form = new AddUserForm { Profile = (int)Profile.Employee };

            Assert.IsFalse(form.IsValid);
            Assert.AreEqual("Segment is required.", form.Errors["segment"][0]);
            Assert.AreEqual("School is required.", form.Errors["school"][0]);
        }

        [Test]
        public void NewUserForm_ShouldIgnoreSegmentIfProfileIsAdmin() {
            var form = new AddUserForm {
                Name = "Name",
                Email = "email@gmail.com",
                Phone = "11999999999",
                Segment = "9efc752",
                School = "9efc752",
                Profile = (int)Profile.Administrator
            };

            Assert.IsTrue(form.IsValid);
            Assert.IsNull(form.Data.School);
            Assert.IsNull(form.Data.Segment);
        }

    }
}