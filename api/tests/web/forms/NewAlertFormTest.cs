using System;
using System.Collections.Generic;
using NUnit.Framework;
using UniversidadeMsd.Core.Models;
using UniversidadeMsd.Core.ObjectValues;
using UniversidadeMsd.Web.Forms;

namespace UniversidadeMsd.Tests.Web.Forms
{
    [TestFixture]
    public class NewAlertFormTest
    {
        [TestCase("Name", "9cfed6", "9cfed7", 0)]
        [TestCase("", "9cfed6", "9cfed7", 1)]
        [TestCase("Name", "", "9cfed7", 1)]
        [TestCase("Name", "9cfed6", "", 1)]
        [TestCase("", "", "", 3)]
        public void NewAlertForm_ShouldHasErrorIfValuesWereNotProvided(String name, String segment, String school, Int32 expected) {
            var form = new AddAlertForm
            {
                Name = name,
                Segment = segment,
                Schools = new List<String> { school }
            };
            Assert.AreEqual(form.IsValid, expected == 0);
            Assert.AreEqual(expected, form.Errors.Count);
        }
    }
}