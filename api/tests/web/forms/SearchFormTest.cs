using System;
using NUnit.Framework;

using UniversidadeMsd.Web.Forms;

namespace UniversidadeMsd.Tests.Web.Forms
{
    [TestFixture]
    public class SearchFormTest
    {
        [Test]
        public void SearchForm_ShouldReturnTermForQuery() {
            var form = new SearchForm { Query = "Name:UniversidadeMsd" };
            Assert.AreEqual("UniversidadeMsd", form.Terms["Name"]);
        }
    }
}