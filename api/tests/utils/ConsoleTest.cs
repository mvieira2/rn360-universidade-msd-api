using System;
using System.Collections.Generic;
using System.Text;

namespace UniversidadeMsd.Tests.Utils {

    public class ConsoleTest {

        public static void WriteErrors(IDictionary<String, IList<String>> errors) {
            foreach (var entry in errors) {
                var info = String.Join(",", entry.Value);
                Console.WriteLine($"{entry.Key}: [{info}]");
            }
        }
    }
}