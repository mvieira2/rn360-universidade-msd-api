using FluentMigrator;

namespace UniversidadeMsd.Migrations.Schemas
{
    [Migration(0003)]
    public class CreateSchoolTable : Migration
    {
        public override void Up()
        {
            Create.Table("School")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("CreatedAt").AsDateTime().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("Identifier").AsString(32).NotNullable().Unique("UX_School_Identifier")
                .WithColumn("Name").AsString().NotNullable()
                .WithColumn("Photo").AsString(255).Nullable()
                .WithColumn("SegmentId").AsInt32().Nullable().ForeignKey("Segment", "Id");
        }
        public override void Down()
        {
            Delete.Table("School");
        }
    }
}