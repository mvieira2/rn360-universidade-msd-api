using System;
using FluentMigrator;

namespace UniversidadeMsd.Migrations.Schemas
{
    [Migration(0001)]
    public class CreateUserTable : Migration
    {
        public override void Up()
        {
            Create.Table("User")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("CreatedAt").AsDateTime().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("Name").AsString().NotNullable()
                .WithColumn("Password").AsString().NotNullable()
                .WithColumn("Identifier").AsString(32).NotNullable().Unique("UX_User_Identifier")
                .WithColumn("Email").AsString().NotNullable().Unique("UX_User_Email")
                .WithColumn("Phone").AsString().NotNullable()
                .WithColumn("Profile").AsInt32().NotNullable()
                .WithColumn("SegmentId").AsInt32().Nullable().ForeignKey("Segment", "Id")
                .WithColumn("HashNewPassword").AsString(32).NotNullable().Unique("UX_User_HashNewPassword");
        }
        public override void Down()
        {
            Delete.Table("User");
        }
    }
}
