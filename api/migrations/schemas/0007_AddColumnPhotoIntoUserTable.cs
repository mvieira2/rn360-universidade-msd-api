
using FluentMigrator;
namespace UniversidadeMsd.Migrations.Schemas
{
    [Migration(0007)]
    public class AddColumnPhotoIntoUserTable : Migration
    {
        public override void Up()
        {
            Alter.Table("User")
                 .AddColumn("Photo").AsString(255).Nullable();
        }

        public override void Down()
        {
            Execute.Sql("ALTER TABLE User DROP COLUMN Photo");
        }
    }
}