
using FluentMigrator;
namespace UniversidadeMsd.Migrations.Schemas
{
    [Migration(0010)]
    public class CreateDownloadTable : Migration
    {
        public override void Up()
        {
            Create.Table("Download")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("CreatedAt").AsDateTime().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("Identifier").AsString(32).NotNullable().Unique("UX_Download_Identifier")
                .WithColumn("Name").AsString().NotNullable()
                .WithColumn("DownloadFile").AsString(255).Nullable()
                .WithColumn("Description").AsString(1000).Nullable()
                .WithColumn("SegmentId").AsInt32().Nullable().ForeignKey("Segment", "Id");

            Create.Table("DownloadSchool")
                  .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                  .WithColumn("DownloadId").AsInt32().NotNullable().ForeignKey("Download", "Id")
                  .WithColumn("SchoolId").AsInt32().NotNullable().ForeignKey("School", "Id");
        }
        public override void Down()
        {
            Delete.Table("DownloadSchool");
            Delete.Table("Download");
        }
    }
}