
using FluentMigrator;
namespace UniversidadeMsd.Migrations.Schemas
{
    [Migration(0005)]
    public class CreateAlertTable : Migration
    {
        public override void Up()
        {
            Create.Table("Alert")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("CreatedAt").AsDateTime().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("Identifier").AsString(32).NotNullable().Unique("UX_Alert_Identifier")
                .WithColumn("Name").AsString().NotNullable()
                .WithColumn("Description").AsString(1000).Nullable()
                .WithColumn("SegmentId").AsInt32().Nullable().ForeignKey("Segment", "Id");

            Create.Table("AlertSchool")
                  .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                  .WithColumn("AlertId").AsInt32().NotNullable().ForeignKey("Alert", "Id")
                  .WithColumn("SchoolId").AsInt32().NotNullable().ForeignKey("School", "Id");
        }
        public override void Down()
        {
            Delete.Table("AlertSchool");
            Delete.Table("Alert");
        }
    }
}