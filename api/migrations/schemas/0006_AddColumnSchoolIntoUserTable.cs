
using FluentMigrator;
namespace UniversidadeMsd.Migrations.Schemas
{
    [Migration(0006)]
    public class AddColumnSchoolIntoUserTable : Migration
    {
        public override void Up()
        {
            Alter.Table("User")
                 .AddColumn("SchoolId").AsInt32().Nullable().ForeignKey("School", "Id");
        }

        public override void Down()
        {
            Execute.Sql("ALTER TABLE User DROP COLUMN SchoolId");
        }
    }
}