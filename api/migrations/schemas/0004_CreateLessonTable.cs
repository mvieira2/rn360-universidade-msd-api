using FluentMigrator;

namespace UniversidadeMsd.Migrations.Schemas
{
    [Migration(0004)]
    public class CreateLessonTable : Migration
    {
        public override void Up()
        {
            Create.Table("Lesson")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("CreatedAt").AsDateTime().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("Identifier").AsString(32).NotNullable().Unique("UX_Lesson_Identifier")
                .WithColumn("Name").AsString().NotNullable()
                .WithColumn("Teachers").AsString().Nullable()
                .WithColumn("Description").AsString(1000).Nullable()
                .WithColumn("StartedAt").AsDateTime().Nullable()
                .WithColumn("Link").AsString().Nullable()
                .WithColumn("Options").AsInt32().WithDefaultValue(0)
                .WithColumn("ProfileAccess").AsString().Nullable()
                .WithColumn("SegmentId").AsInt32().Nullable().ForeignKey("Segment", "Id");

            Create.Table("LessonSchool")
                  .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                  .WithColumn("LessonId").AsInt32().NotNullable().ForeignKey("Lesson", "Id")
                  .WithColumn("SchoolId").AsInt32().NotNullable().ForeignKey("School", "Id");

            Create.Table("LessonQuestion")
                  .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                  .WithColumn("Identifier").AsString(32).NotNullable().Unique("UX_LessonQuestion_Identifier")
                  .WithColumn("LessonId").AsInt32().NotNullable().ForeignKey("Lesson", "Id")
                  .WithColumn("Title").AsString(1000).NotNullable();

            Create.Table("LessonQuestionAnswer")
                  .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                  .WithColumn("Identifier").AsString(32).NotNullable().Unique("UX_LessonQuestionAnswer_Identifier")
                  .WithColumn("LessonQuestionId").AsInt32().NotNullable().ForeignKey("LessonQuestion", "Id")
                  .WithColumn("Value").AsString(1000).NotNullable()
                  .WithColumn("IsRight").AsBoolean().NotNullable();

            Create.Table("LessonComments")
                  .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                  .WithColumn("CreatedAt").AsDateTime().WithDefault(SystemMethods.CurrentDateTime)
                  .WithColumn("Identifier").AsString(32).NotNullable().Unique("UX_LessonComments_Identifier")
                  .WithColumn("Comments").AsString(1000).NotNullable()
                  .WithColumn("StatusComment").AsInt32().NotNullable()
                  .WithColumn("LessonId").AsInt32().NotNullable().ForeignKey("Lesson", "Id")
                  .WithColumn("UserId").AsInt32().NotNullable().ForeignKey("User", "Id");

            Create.Table("LessonExam")
                  .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                  .WithColumn("CreatedAt").AsDateTime().WithDefault(SystemMethods.CurrentDateTime)
                  .WithColumn("Identifier").AsString(32).NotNullable().Unique("UX_LessonExam_Identifier")
                  .WithColumn("QtyQuestions").AsInt32().Nullable()
                  .WithColumn("QtySuccess").AsInt32().Nullable()
                  .WithColumn("QtyWrong").AsInt32().Nullable()
                  .WithColumn("LessonId").AsInt32().NotNullable().ForeignKey("Lesson", "Id")
                  .WithColumn("UserId").AsInt32().NotNullable().ForeignKey("UserId", "Id");

            Create.Table("LessonExamDetail")
                  .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                  .WithColumn("Question").AsString(1000).NotNullable()
                  .WithColumn("Answer").AsString(1000).NotNullable()
                  .WithColumn("IsRight").AsBoolean().NotNullable()
                  .WithColumn("LessonExamId").AsInt32().NotNullable().ForeignKey("LessonExam", "Id");

        }
        public override void Down()
        {
            Delete.Table("LessonExamDetail");
            Delete.Table("LessonExam");
            Delete.Table("LessonComments");
            Delete.Table("LessonQuestionAnswer");
            Delete.Table("LessonQuestion");
            Delete.Table("LessonSchool");
            Delete.Table("Lesson");
        }
    }
}