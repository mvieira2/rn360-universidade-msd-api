using FluentMigrator;

namespace UniversidadeMsd.Migrations.Schemas
{
    [Migration(0000)]
    public class CreateSegmentTable : Migration
    {
        public override void Up()
        {
            Create.Table("Segment")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("CreatedAt").AsDateTime().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("Name").AsString().NotNullable()
                .WithColumn("Identifier").AsString(32).NotNullable().Unique("UX_Segment_Identifier");
        }
        public override void Down()
        {
            Delete.Table("Segment");
        }
    }
}