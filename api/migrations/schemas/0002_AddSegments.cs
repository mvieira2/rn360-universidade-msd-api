using FluentMigrator;

namespace UniversidadeMsd.Migrations.Schemas
{
    [Migration(0002)]
    public class AddSegments : Migration
    {
        public override void Up()
        {
            Insert.IntoTable("Segment")
                  .Row(new { Name = "Saúde Animal Brasil", Identifier = "2a6139a40ddd4d11b020661ea47dfbbd" })
                  .Row(new { Name = "Valée", Identifier = "f31e6f0ad89f4f319d27ac64c5e97e90" })
                  .Row(new { Name = "Latam", Identifier = "a5a5c132d3474119ab68b8d890955928" })
                  .Row(new { Name = "Profissional do amanhã", Identifier = "21301ea7fcf24a288cffbdba4f4ab7fc" });
        }
        public override void Down()
        {
            Delete.FromTable("Segment")
                  .Row(new { Identifier = "2a6139a40ddd4d11b020661ea47dfbbd" })
                  .Row(new { Identifier = "f31e6f0ad89f4f319d27ac64c5e97e90" })
                  .Row(new { Identifier = "a5a5c132d3474119ab68b8d890955928" })
                  .Row(new { Identifier = "21301ea7fcf24a288cffbdba4f4ab7fc" });
        }
    }
}