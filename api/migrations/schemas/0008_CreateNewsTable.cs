
using FluentMigrator;
namespace UniversidadeMsd.Migrations.Schemas
{
    [Migration(0008)]
    public class CreateNewsTable : Migration
    {
        public override void Up()
        {
            Create.Table("News")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("CreatedAt").AsDateTime().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("PublishedAt").AsDateTime().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("Identifier").AsString(32).NotNullable().Unique("UX_News_Identifier")
                .WithColumn("Title").AsString().NotNullable()
                .WithColumn("Description").AsString(1000).Nullable()
                .WithColumn("Content").AsString().Nullable()
                .WithColumn("SegmentId").AsInt32().Nullable().ForeignKey("Segment", "Id");

            Create.Table("NewsSchool")
                  .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                  .WithColumn("NewsId").AsInt32().NotNullable().ForeignKey("News", "Id")
                  .WithColumn("SchoolId").AsInt32().NotNullable().ForeignKey("School", "Id");
        }
        public override void Down()
        {
            Delete.Table("NewsSchool");
            Delete.Table("News");
        }
    }
}