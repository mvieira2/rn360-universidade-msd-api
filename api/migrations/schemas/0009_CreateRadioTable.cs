
using FluentMigrator;
namespace UniversidadeMsd.Migrations.Schemas
{
    [Migration(0009)]
    public class CreateRadioTable : Migration
    {
        public override void Up()
        {
            Create.Table("Radio")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("CreatedAt").AsDateTime().WithDefault(SystemMethods.CurrentDateTime)
                .WithColumn("Identifier").AsString(32).NotNullable().Unique("UX_Radio_Identifier")
                .WithColumn("Name").AsString().NotNullable()
                .WithColumn("AudioFile").AsString(255).Nullable()
                .WithColumn("Description").AsString(1000).Nullable()
                .WithColumn("Edition").AsString().Nullable()
                .WithColumn("SegmentId").AsInt32().Nullable().ForeignKey("Segment", "Id");

            Create.Table("RadioSchool")
                  .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                  .WithColumn("RadioId").AsInt32().NotNullable().ForeignKey("Radio", "Id")
                  .WithColumn("SchoolId").AsInt32().NotNullable().ForeignKey("School", "Id");
        }
        public override void Down()
        {
            Delete.Table("RadioSchool");
            Delete.Table("Radio");
        }
    }
}