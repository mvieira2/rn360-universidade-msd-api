/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
    id: string;
}
interface JQuery {
    mMenu(options: any): JQuery;
    animateClass(options: any): JQuery;
    setActiveItem(item: any): JQuery;
    getPageTitle(item: any): JQuery;
    getBreadcrumbs(item: any): JQuery;
    validate(options: any): JQuery;
    valid(): JQuery;
    resetForm(): JQuery;
    moment(): JQuery;
    markdown(): JQuery;
    slick(options: any): JQuery;
    fullCalendar(options: any): JQuery;
    rules(action?: any, options?: any): JQuery;
}