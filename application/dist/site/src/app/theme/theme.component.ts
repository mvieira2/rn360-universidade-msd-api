import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Helpers } from '../helpers';
import { ScriptLoaderService } from '../_services/script-loader.service';
import { SegmentsService } from "../_services/segments.service";
import { SchoolsService } from "../_services/schools.service";
import { AlertsService } from "../_services/alerts.service";
import { HeaderNavService } from "./layouts/header-nav/header-nav.service";
import { _globals } from '../globals';

declare let mApp: any;
declare let mUtil: any;
declare let mLayout: any;

@Component({
    selector: ".m-grid.m-grid--hor.m-grid--root.m-page",
    templateUrl: "./theme.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class ThemeComponent implements OnInit {

    currentUser: any;

    constructor(
        private _script: ScriptLoaderService,
        private _router: Router,
        private _segmentsService: SegmentsService,
        private _schoolsService: SchoolsService,
        private _alertsService: AlertsService,
        private _headerNavService: HeaderNavService) {
        this.currentUser = _globals.currentUser;
        this.getAll();
    }


    ngOnInit() {
        this._script.loadScripts('body', ['assets/vendors/base/vendors.bundle.js', 'assets/site/base/scripts.bundle.js'], true)
            .then(result => {
                Helpers.setLoading(false);
                // optional js to be loaded once
                this._script.loadScripts('head', ['assets/vendors/custom/fullcalendar/fullcalendar.bundle.js'], true);
            });
        this._router.events.subscribe((route) => {
            if (route instanceof NavigationStart) {
                (<any>mLayout).closeMobileAsideMenuOffcanvas();
                (<any>mLayout).closeMobileHorMenuOffcanvas();
                (<any>mApp).scrollTop();
                Helpers.setLoading(true);
                // hide visible popover
                (<any>$('[data-toggle="m-popover"]')).popover('hide');
            }
            if (route instanceof NavigationEnd) {
                // init required js
                (<any>mApp).init();
                (<any>mUtil).init();
                Helpers.setLoading(false);
                // content m-wrapper animation
                let animation = 'm-animate-fade-in-up';
                $('.m-wrapper').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(e) {
                    $('.m-wrapper').removeClass(animation);
                }).removeClass(animation).addClass(animation);
            }
        });
    }

    getAll() {
        this._segmentsService.getAll().subscribe(
            response => {
                console.log(response.data.entries);
                _globals.segments = response.data.entries;
            },
            error => {
                console.log(error);
            });

        this._schoolsService.getAll().subscribe(
            response => {
                console.log(response.data.entries);
                _globals.schools = response.data.entries;
            },
            error => {
                console.log(error);
            });

        this._alertsService.getAll().subscribe(
            response => {
                let alerts: any[] = response.data.entries
                let list: any[] = [];

                if (this.currentUser.profile === 2048) {
                    list = alerts;
                } else {
                    alerts.forEach((alert) => {
                        if (alert.segment && alert.segment.identifier === this.currentUser.segment.identifier) {
                            if (this.currentUser.school && alert.schools && alert.schools.length > 0) {
                                alert.schools.forEach((school) => {
                                    if (school.identifier === this.currentUser.school.identifier) {
                                        list.push(alert);
                                    }
                                });
                            } else {
                                list.push(alert);
                            }
                        }
                    });
                }

                let localAlerts = JSON.parse(localStorage.getItem('MsdAlerts' + this.currentUser.identifier));
                console.log(('MsdAlerts' + this.currentUser.identifier), localAlerts);

                if (localAlerts) {
                    localAlerts.forEach((local) => {
                        list.forEach((item) => {
                            if (item.identifier === local.identifier && local.read) item.read = true;
                        });
                    });
                }

                _globals.alerts = list;
                this._headerNavService.update(list);

            },
            error => {
                console.log(error);
            });

    }

}