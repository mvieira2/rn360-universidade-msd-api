import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { LessonsService } from '../../../../_services/lessons.service';
import { NewsService } from '../../../../_services/news.service';
import { _globals } from '../../../../globals';

declare var moment: any;
declare var mApp: any;

@Component({
    selector: "app-index",
    templateUrl: "./index.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class IndexComponent implements OnInit, AfterViewInit {

    dataText: any;
    profiles: any;
    currentUser: any = {};
    showCarousel: boolean = false;
    segments: any = [];
    schools: any = [];
    list: any[] = [];
    newsList: any[] = [];
    rank: string = "school";
    rankingList: any = [];

    rankingSchool: any[] =
    [
        {
            position: 1,
            name: "Leonardo de Lima",
            points: 2000
        },
        {
            position: 2,
            name: "Luciana Barros",
            points: 1500
        },
        {
            position: 3,
            name: "Frank Nandes",
            points: 1300
        },
        {
            position: 4,
            name: "Guilherme teste",
            points: 1000
        },
        {
            position: 5,
            name: "Antonio Hélio",
            points: 500
        }
    ];

    rankingSegment: any[] =
    [
        {
            position: 3,
            name: "Frank Nandes",
            points: 1300
        },
        {
            position: 4,
            name: "Guilherme teste",
            points: 1000
        },
        {
            position: 5,
            name: "Antonio Hélio",
            points: 500
        },
        {
            position: 1,
            name: "Leonardo de Lima",
            points: 2000
        },
        {
            position: 2,
            name: "Luciana Barros",
            points: 1500
        }
    ];

    calendar: any[] =
    [
        {
            month: "Abril",
            year: "2018",
            themes:
            [
                { date: "10/04", name: "Aula 1", teachers: "Professor 1 e Professor 2" },
                { date: "15/04", name: "Aula 2", teachers: "Professor 1 e Professor 2" },
                { date: "20/04", name: "Aula 3", teachers: "Professor 1 e Professor 2" },
                { date: "28/04", name: "Aula 4", teachers: "Professor 1 e Professor 2" },
                { date: "29/04", name: "Aula 5", teachers: "Professor 1 e Professor 2" }
            ]
        },
        {
            month: "Maio",
            year: "2018",
            themes:
            [
                { date: "10/05", name: "Aula 1", teachers: "Professor 1 e Professor 2" },
                { date: "15/05", name: "Aula 2", teachers: "Professor 1 e Professor 2" },
                { date: "20/05", name: "Aula 3", teachers: "Professor 1 e Professor 2" },
                { date: "28/05", name: "Aula 4", teachers: "Professor 1 e Professor 2" },
                { date: "29/05", name: "Aula 5", teachers: "Professor 1 e Professor 2" }
            ]
        },
        {
            month: "Junho",
            year: "2018",
            themes:
            [
                { date: "10/06", name: "Aula 1", teachers: "Professor 1 e Professor 2" },
                { date: "15/06", name: "Aula 2", teachers: "Professor 1 e Professor 2" },
                { date: "20/06", name: "Aula 3", teachers: "Professor 1 e Professor 2" },
                { date: "28/06", name: "Aula 4", teachers: "Professor 1 e Professor 2" },
                { date: "29/06", name: "Aula 5", teachers: "Professor 1 e Professor 2" }
            ]
        },
        {
            month: "Julho",
            year: "2018",
            themes:
            [
                { date: "10/07", name: "Aula 1", teachers: "Professor 1 e Professor 2" },
                { date: "15/07", name: "Aula 2", teachers: "Professor 1 e Professor 2" },
                { date: "20/07", name: "Aula 3", teachers: "Professor 1 e Professor 2" },
                { date: "28/07", name: "Aula 4", teachers: "Professor 1 e Professor 2" },
                { date: "29/07", name: "Aula 5", teachers: "Professor 1 e Professor 2" }
            ]
        },
        {
            month: "Agosto",
            year: "2018",
            themes:
            [
                { date: "10/08", name: "Aula 1", teachers: "Professor 1 e Professor 2" },
                { date: "15/08", name: "Aula 2", teachers: "Professor 1 e Professor 2" },
                { date: "20/08", name: "Aula 3", teachers: "Professor 1 e Professor 2" },
                { date: "28/08", name: "Aula 4", teachers: "Professor 1 e Professor 2" },
                { date: "29/08", name: "Aula 5", teachers: "Professor 1 e Professor 2" }
            ]
        },
        {
            month: "Setembro",
            year: "2018",
            themes:
            [
                { date: "10/09", theme: "Aula 1", teachers: "Professor 1 e Professor 2" },
                { date: "15/09", theme: "Aula 2", teachers: "Professor 1 e Professor 2" },
                { date: "20/09", theme: "Aula 3", teachers: "Professor 1 e Professor 2" },
                { date: "28/09", theme: "Aula 4", teachers: "Professor 1 e Professor 2" },
                { date: "29/09", theme: "Aula 5", teachers: "Professor 1 e Professor 2" }
            ]
        }
    ];

    constructor(private _script: ScriptLoaderService, private _lessonsService: LessonsService, private _newsService: NewsService) {

    }

    ngOnInit() {
        this.dataText = _globals.dataText.lang[_globals.language];
        this.currentUser = _globals.currentUser;
        this.profiles = _globals.profiles;
        this.segments = _globals.segments;
        this.schools = _globals.schools;
        this.rankingList = this.rankingSchool;
        this.getAllLessons();
        this.getNews();
    }

    ngAfterViewInit() {
        Helpers.bodyClass('m-page--wide m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default');
    }

    getAllLessons() {
        this.block('Carregando...');
        this._lessonsService.getAll().subscribe(
            response => {
                this.unblock();
                this.list = this.filterResults(response.data.entries);

                setTimeout(() => {
                    this.applyCarousel();
                    this.showCarousel = true;
                    this.appendCalendar();
                }, 100);
            },
            error => {
                this.unblock();
                console.log(error);
            });
    }

    getNews() {
        this.block('Carregando...');
        this._newsService.getAll("?limit=3").subscribe(
            response => {
                this.unblock();
                this.newsList = response.data.entries;
                console.log(this.newsList);
            },
            error => {
                this.unblock();
                console.log(error);
            });
    }

    appendCalendar() {
        var todayDate = moment().startOf('day');
        var YM = todayDate.format('YYYY-MM');
        var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
        var TODAY = todayDate.format('YYYY-MM-DD');
        var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');
        var lang = (_globals.language === "br") ? "pt-br" : _globals.language;

        let events: any = [];

        this.list.forEach((item) => {
            events.push(
                {
                    title: item.name,
                    start: item.startedAt,
                    description: item.description,
                    className: "m-fc-event--primary"
                });
        });

        $('#m_calendar').fullCalendar({
            locale: lang,
            customButtons:
            {
                myCustomButton:
                {
                    text: 'Próximas Aulas'
                }
            },
            header: {
                left: "myCustomButton",
                center: "title",
                //right: 'month,agendaWeek,agendaDay,listWeek'
                //right: 'month,agendaWeek,agendaDay,listWeek'
                right: 'prev,next today'
            },

            editable: true,
            eventLimit: true, // allow "more" link when too many events
            navLinks: true,
            defaultView: 'listWeek',
            events: events /*[
              {
                  title: 'Nome da Aula 1',
                  start: YM + '-01',
                  description: 'Nome da Aula 1',
                  className: "m-fc-event--danger m-fc-event--solid-warning"  
              },
              {
                  title: 'Nome da Aula 2',
                  start: YM + '-14T13:30:00',
                  description: 'Nome da Aula 2',
                  end: YM + '-14',
                  className: "m-fc-event--accent"
              },
              {
                  title: 'Nome da Aula 3',
                  start: YM + '-02',
                  description: 'Nome da Aula 3',
                  end: YM + '-03',
                  className: "m-fc-event--primary"
              },
              {
                  title: 'Nome da Aula 4',
                  start: YM + '-03',
                  description: 'Nome da Aula 4',
                  end: YM + '-05',
                  className: "m-fc-event--light m-fc-event--solid-primary"
              }
          ]*/,

            eventRender: function(event, element) {
                if (element.hasClass('fc-day-grid-event')) {
                    element.data('content', event.description);
                    element.data('placement', 'top');
                    mApp.initPopover(element);
                } else if (element.hasClass('fc-time-grid-event')) {
                    element.find('.fc-title').append('<div class="fc-description">' + event.description + '</div>');
                } else if (element.find('.fc-list-item-title').lenght !== 0) {
                    element.find('.fc-list-item-title').append('<div class="fc-description">' + event.description + '</div>');
                }
            }
        });
    }

    filterResults(list) {
        let response: any[] = [];

        if (this.currentUser.segment.identifier === "admin") {
            response = list;
        } else {
            list.forEach((item) => {
                if (item.segment.identifier === this.currentUser.segment.identifier) {
                    if (this.currentUser.school && item.schools && item.schools.length > 0) {
                        item.schools.forEach((school) => {
                            if (school.identifier === this.currentUser.school.identifier) {
                                response.push(item);
                            }
                        });
                    } else {
                        response.push(item);
                    }
                }
            });
        }

        console.log(response);
        return response;
    }

    changeRank(list) {
        this.rank = list;
        if (list === "segment") {
            this.rankingList = this.rankingSegment;
        } else {
            this.rankingList = this.rankingSchool;
        }

    }

    applyCarousel() {
        let intSlides = (this.list.length >= 3) ? 3 : this.list.length;
        $("#slider-classrooms").slick({
            infinite: true,
            slidesToShow: intSlides,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            centerMode: true,
            dots: true
        });
    }

    getThumbUrl(url) {
        var i, r, rx = /^.*(?:(?:youtu\.be\/|v\/|vi\/|u\/\w\/|embed\/)|(?:(?:watch)?\?v(?:i)?=|\&v(?:i)?=))([^#\&\?]*).*/;
        r = url.match(rx);
        return 'https://img.youtube.com/vi/' + r[1] + '/mqdefault.jpg';
    }

    getFormatDate(date) {
        return moment(date).format("DD/MM/YYYY HH:mm");
    }

    saveNews(news) {
        _globals.currentNews = news;
    }

    block(msg) {
        mApp.block('.m-page', {
            overlayColor: '#000000',
            type: 'loader',
            state: 'success',
            message: msg
        });
    }

    unblock() {
        mApp.unblock('.m-page');
    }

}