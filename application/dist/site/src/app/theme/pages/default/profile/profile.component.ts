import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { _globals } from '../../../../globals';
import { HeaderNavService } from "../../../layouts/header-nav/header-nav.service";

declare var swal: any;

@Component({
    selector: "app-profile",
    templateUrl: "./profile.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class ProfileComponent implements OnInit, AfterViewInit {

    currentUser: any = {};
    selectedAlert: any = {};
    dataText: any;
    frmPassword: any;
    model: any = {};
    loading = false;
    boolPass: boolean = false;
    alerts: any[] = [];
    rankingSchool: any[] =
    [
        {
            position: 1,
            name: "Leonardo de Lima",
            points: 2000
        },
        {
            position: 2,
            name: "Luciana Barros",
            points: 1500
        },
        {
            position: 3,
            name: "Frank Nandes",
            points: 1300
        },
        {
            position: 4,
            name: "Guilherme teste",
            points: 1000
        },
        {
            position: 5,
            name: "Antonio Hélio",
            points: 500
        }
    ];

    evaluations: any[] =
    [
        {
            name: "Aula 1",
            school: "Pecuária",
            date: "10/05/2018",
            status: "OK",
            points: "8/10",
        },
        {
            name: "Aula 2",
            school: "Pecuária",
            date: "10/05/2018",
            status: "OK",
            points: "8/10",
        },
        {
            name: "Aula 3",
            school: "Pecuária",
            date: "10/05/2018",
            status: "OK",
            points: "8/10",
        },
        {
            name: "Aula 4",
            school: "Pecuária",
            date: "10/05/2018",
            status: "OK",
            points: "8/10",
        },
        {
            name: "Aula 5",
            school: "Pecuária",
            date: "10/05/2018",
            status: "OK",
            points: "8/10",
        }
    ];

    constructor(private _headerService: HeaderNavService) {

    }

    ngOnInit() {
        this.dataText = _globals.dataText.lang[_globals.language];
        this.currentUser = _globals.currentUser;
        this.alerts = _globals.alerts;
        this._headerService.change.subscribe(alerts => {
            this.alerts = alerts;
        });
    }

    ngAfterViewInit() {
        this.handleFormSubmit();
    }

    change() {
        this.loading = true;

        if (this.frmPassword.valid()) {
            this.boolPass = false;
            this.model = {};
            swal("Sucesso!", "Sua senha foi alterada.", "success");
            setTimeout(() => { this.frmPassword.validate().resetForm(); }, 300);
        }
    }

    showAlert(itemAlert) {
        this.selectedAlert = itemAlert;
        this.alerts.forEach((item) => {
            if (itemAlert.identifier === item.identifier) {
                item.read = true;
            }
        });

        _globals.alerts = this.alerts;
        this._headerService.update(this.alerts);
    }

    handleFormSubmit() {
        this.frmPassword = $("#frmPassword");

        this.frmPassword.validate({
            rules: {
                password: {
                    required: true,
                },
                rpassword: {
                    required: true,
                    equalTo: "#password"
                }
            },
        });
    }

}