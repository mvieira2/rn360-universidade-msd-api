import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router } from "@angular/router";
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { LessonsService } from '../../../../_services/lessons.service';
import { _globals } from '../../../../globals';

declare var swal: any;
declare var mApp: any;

@Component({
    selector: "app-inner",
    templateUrl: "./classrooms.component.html",
    encapsulation: ViewEncapsulation.None
})
export class ClassroomsComponent implements OnInit, AfterViewInit {

    currentUser: any;
    searchText: any;
    dataText: any;
    model: any = {};
    filterSchool: any;
    loading = false;
    profiles: any;
    segments: any = [];
    schools: any = [];
    list: any[] = [];

    constructor(private _router: Router, private _script: ScriptLoaderService, private _lessonsService: LessonsService) {

    }

    ngOnInit() {
        this.dataText = _globals.dataText.lang[_globals.language];
        this.currentUser = _globals.currentUser;
        this.profiles = _globals.profiles;
        this.segments = _globals.segments;
        this.getAll();
    }

    ngAfterViewInit() {

    }

    getAll() {
        this.block('Carregando...');
        this._lessonsService.getAll().subscribe(
            response => {
                this.unblock();
                console.log(response.data.entries);
                this.list = response.data.entries;
                this.schools = _globals.schools;
            },
            error => {
                console.log(error);
                this.unblock();
            });
    }

    filterTab(txt) {
        this.filterSchool = txt;
    }

    saveLesson(lesson) {
        _globals.currentLesson = lesson;
    }

    block(msg) {
        mApp.block('.m-page', {
            overlayColor: '#000000',
            type: 'loader',
            state: 'success',
            message: msg
        });
    }

    unblock() {
        mApp.unblock('.m-page');
    }


}