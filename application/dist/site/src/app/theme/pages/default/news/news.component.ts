import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { NewsService } from '../../../../_services/news.service';
import { _globals } from '../../../../globals';

declare var swal: any;
declare var moment: any;
declare var mApp: any;

@Component({
    selector: "app-inner",
    templateUrl: "./news.component.html",
    encapsulation: ViewEncapsulation.None
})
export class NewsComponent implements OnInit, AfterViewInit {
    dataText: any;
    apiURL: string;
    currentUser: any;
    searchText: any;
    model: any = {};
    filterSchool: any;
    loading = false;
    profiles: any;
    segments: any = [];
    schools: any = [];
    list: any[] = [];

    constructor(private _script: ScriptLoaderService, private _newsService: NewsService) {

    }

    ngOnInit() {
        this.dataText = _globals.dataText.lang[_globals.language];
        this.apiURL = _globals.apiURL;
        this.currentUser = _globals.currentUser;
        this.profiles = _globals.profiles;
        this.segments = _globals.segments;
        this.getAll();
    }

    ngAfterViewInit() {

    }

    getAll() {
        this.block('Carregando...');
        this._newsService.getAll().subscribe(
            response => {
                console.log(response.data.entries);
                this.list = response.data.entries;
                this.schools = _globals.schools;
                this.unblock();
            },
            error => {
                console.log(error);
                this.unblock();
            });
    }

    filterTab(txt) {
        this.filterSchool = txt;
    }

    saveNews(news) {
        _globals.currentNews = news;
    }

    getDate(date) {
        return moment(date).format("DD/MM/YYYY");
    }

    block(msg) {
        mApp.block('.m-page', {
            overlayColor: '#000000',
            type: 'loader',
            state: 'success',
            message: msg
        });
    }

    unblock() {
        mApp.unblock('.m-page');
    }


}