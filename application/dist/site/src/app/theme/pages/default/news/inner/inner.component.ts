import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { Helpers } from '../../../../../helpers';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { NewsService } from '../../../../../_services/news.service';
import { _globals } from '../../../../../globals';

declare var swal: any;
declare var mApp: any;
declare var moment: any;

@Component({
    selector: "app-inner",
    templateUrl: "./inner.component.html",
    encapsulation: ViewEncapsulation.None
})
export class InnerComponent implements OnInit, AfterViewInit {

    dataText: any;
    frmNewPost: any;
    frmEvaluation: any;
    loading = false;
    searchText: any;
    urlVideo: any;
    model: any = {};
    data: any = {};
    /*data: any = 
    {
       id:1,
       name:"Notícia 1", 
       area:"Saúde Animal Brasil",
       date:"29/03/2018",
       description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.",
       schools:["Pecuária","Avicultura","PET"],
       content:'<p><strong>Conteúdo 1</strong></p><p><img src="./assets/site/media/img/cow.jpg" /></p><p>Ao contrário do que se acredita, Lorem Ipsum não é simplesmente um texto randômico. Com mais de 2000 anos, suas raízes podem ser encontradas em uma obra de literatura latina clássica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre citações da palavra na literatura clássica, descobriu a sua indubitável origem. Lorem Ipsum vem das seções 1.10.32 e 1.10.33 do "de Finibus Bonorum et Malorum" (Os Extremos do Bem e do Mal), de Cícero, escrito em 45 AC. Este livro é um tratado de teoria da ética muito popular na época da Renascença. A primeira linha de Lorem Ipsum, "Lorem Ipsum dolor sit amet..." vem de uma linha na seção 1.10.32.</p><p>Existem muitas variações disponíveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de alteração, seja por inserção de passagens com humor, ou palavras aleatórias que não parecem nem um pouco convincentes. Se você pretende usar uma passagem de Lorem Ipsum, precisa ter certeza de que não há algo embaraçoso escrito escondido no meio do texto. Todos os geradores de Lorem Ipsum na internet tendem a repetir pedaços predefinidos conforme necessário, fazendo deste o primeiro gerador de Lorem Ipsum autêntico da internet. Ele usa um dicionário com mais de 200 palavras em Latim combinado com um punhado de modelos de estrutura de frases para gerar um Lorem Ipsum com aparência razoável, livre de repetições, inserções de humor, palavras não características, etc.</p>'
    };*/

    constructor(private _script: ScriptLoaderService, private route: ActivatedRoute, private sanitizer: DomSanitizer, private _newsService: NewsService) {

    }

    ngOnInit() {
        this.dataText = _globals.dataText.lang[_globals.language];
        console.log(_globals.currentNews);
        if (_globals.currentNews) {
            this.appendData(_globals.currentNews);
        } else {
            this.route.params.subscribe(params => {
                this.getNews(params["id"]);
            });
        }
    }

    getNews(id) {
        this.block('Carregando...');
        this._newsService.getById(id).subscribe(
            response => {
                console.log(response.data.entries[0]);
                this.appendData(response.data.entries[0]);
                this.unblock();
            },
            error => {
                console.log(error);
                this.unblock();
            });
    }

    appendData(data) {
        this.data = data;
        this.data.date = moment(this.data.startedAt).format("DD/MM/YYYY");
    }

    ngAfterViewInit() {
        this.handleFormSubmit();
    }

    handleFormSubmit() {
        this.frmNewPost = $("#frmNewPost");
        this.frmEvaluation = $("#frmEvaluation");

        this.frmNewPost.validate({
            rules: {
                comment: {
                    required: true
                }
            },
        });

        this.frmEvaluation.validate({
            rules: {
                fullname: {
                    required: true
                },
                email: {
                    required: true,
                    email: true,
                },
                phone: {
                    required: true
                },
                user_type: {
                    required: true
                },
                area: {
                    required: true
                },
                school: {
                    required: true
                }
            },
        });
    }

    create() {
        this.loading = true;

        if (this.frmNewPost.valid()) {
            var post = Object.assign({}, this.model);

            this.data.comments.push({
                name: "Leonardo",
                date: this.getDateNow(),
                pic: "./assets/site/media/img/user.jpg",
                post: post.comment
            });

            swal("Sucesso!", "Sua avaliação foi salva com sucesso.", "success");


            this.model = {};
            $("html, body").animate({ scrollTop: $(document).height() - $(window).height() }, () => {
                this.frmNewPost.validate().resetForm();
            });
        }

        /*this._authService.login(this.model.email, this.model.password).subscribe(
            data => {
              this._router.navigate([this.returnUrl]);
            },
            error => {
              this.showAlert('alertSignin');
              this._alertService.error(error);
              this.loading = false;
            });*/
    }

    sendResponses() {
        this.loading = true;

        if (this.frmEvaluation.valid()) {
            $("#btn-close-modal").trigger("click");
            swal("Sucesso!", "Sua avaliação foi salva com sucesso.", "success");
        }
    }

    getDateNow() {
        var date = new Date();
        var month = String(date.getMonth());
        if (month.length == 1) month = "0" + month;
        var literal_date = date.getDate() + "/" + month + "/" + date.getFullYear() + " às " + date.getHours() + ":" + date.getMinutes();
        return literal_date;
    }

    block(msg) {
        mApp.block('.m-page', {
            overlayColor: '#000000',
            type: 'loader',
            state: 'success',
            message: msg
        });
    }

    unblock() {
        mApp.unblock('.m-page');
    }


}