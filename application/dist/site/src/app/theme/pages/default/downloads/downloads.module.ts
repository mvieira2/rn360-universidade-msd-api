import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DownloadsComponent } from './downloads.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { MainPipeModule } from '../../../../_pipes/main-pipe/main-pipe.module';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": DownloadsComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, MainPipeModule
    ], exports: [
        RouterModule
    ], declarations: [
        DownloadsComponent
    ]
})
export class DownloadsModule {



}