import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { Helpers } from '../../../../../helpers';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { LessonsService } from '../../../../../_services/lessons.service';
import { _globals } from '../../../../../globals';

declare var swal: any;
declare var moment: any;
declare var toastr: any;
declare var mApp: any;

@Component({
    selector: "app-inner",
    templateUrl: "./inner.component.html",
    encapsulation: ViewEncapsulation.None
})
export class InnerComponent implements OnInit, AfterViewInit {
    currentUser: any;
    dataText: any;
    frmNewPost: any;
    frmEvaluation: any;
    loading = false;
    searchText: any;
    urlVideo: any;
    idLesson: string;
    model: any = {};
    data: any = {};
    interval: any;
    comments: any[] = [];
    questions: any[] = [];


    constructor(private _script: ScriptLoaderService, private route: ActivatedRoute, private sanitizer: DomSanitizer, private _lessonsService: LessonsService) {

    }

    ngOnInit() {
        this.dataText = _globals.dataText.lang[_globals.language];
        this.currentUser = _globals.currentUser;

        if (_globals.currentLesson) {
            this.idLesson = _globals.currentLesson.identifier;
            this.appendData(_globals.currentLesson);
        } else {
            this.route.params.subscribe(params => {
                this.idLesson = params["id"];
                this.getLesson(params["id"]);
            });
        }

        this.getComments();
        this.getQuestions();
    }

    ngAfterViewInit() {
        toastr.options =
            {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-full-width",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "500",
                "hideDuration": "500",
                "timeOut": "2000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

        this.handleFormSubmit();
    }

    getLesson(id) {
        this.block('Carregando...');
        this._lessonsService.getById(id).subscribe(
            response => {
                this.appendData(response.data.entries[0]);
                this.unblock();
            },
            error => {
                console.log(error);
                this.unblock();
            });
    }

    getComments() {
        console.log("========== Get All Comments ==========");

        this._lessonsService.getComments().subscribe(
            response => {
                console.log(response.data.entries);
                let entries = response.data.entries;
                //let sortedItems = entries.sort((a: any, b: any) => { new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime(); });
                this.comments = entries.filter((item) => { return this.idLesson === item.lesson.identifier; });
                this.comments.reverse();
                this.interval = setTimeout(() => { this.getComments(); }, 10000);
            },
            error => {
                console.log(error);
            });
    }

    getQuestions() {
        this._lessonsService.getQuestions().subscribe(
            response => {
                let entries = response.data.entries;
                this.questions = entries.filter((item) => { return this.idLesson === item.lesson.identifier; });
                //let entries = JSON.parse('[{"title":"Title Question 1","lesson":{"name":"Aula 2","teachers":"Professores","description":"Teste","startedAt":"2018-06-20T13:00:00","link":"https://www.youtube.com/watch?v=Ra8IbQ7-vcc","options":0,"segment":{"name":"Saúde Animal Brasil","identifier":"2a6139a40ddd4d11b020661ea47dfbbd"},"schools":[{"name":"Avicultura","photo":"static/e76e1/e76e133f3fcf42c58878f1f2ffd0ce58.jpeg","segment":{"name":"Saúde Animal Brasil","identifier":"2a6139a40ddd4d11b020661ea47dfbbd"},"identifier":"e76e133f3fcf42c58878f1f2ffd0ce58"},{"name":"Pecuária","photo":"static/a2f7f/a2f7f555cc224f7d964edae14e79bd94.jpeg","segment":{"name":"Saúde Animal Brasil","identifier":"2a6139a40ddd4d11b020661ea47dfbbd"},"identifier":"a2f7f555cc224f7d964edae14e79bd94"}],"profileAccess":"[0,1]","identifier":"1d295f90917c4c1e9eaec0e67b5c7225"},"answers":[{"value":"Answer 1","isRight":true,"identifier":"9e0bcef4f323405796a97bf352456fc2"},{"value":"Answer 2","isRight":false,"identifier":"195152b7718c4ea687e8bf847bff7eaa"},{"value":"Answer 3","isRight":false,"identifier":"c0580fda0cd8441b88a2b63c5496d6bf"},{"value":"Answer 4","isRight":false,"identifier":"8e635952e075415482e3b5fe0f1bafb4"},{"value":"Answer 5","isRight":false,"identifier":"ff369d20362f47c783a3c25243b73c65"}],"identifier":"e22389e76e2e41efa0ee968d4a02c2b6"},{"title":"Title Question 2","lesson":{"name":"Aula 2","teachers":"Professores","description":"Teste","startedAt":"2018-06-20T13:00:00","link":"https://www.youtube.com/watch?v=Ra8IbQ7-vcc","options":0,"segment":{"name":"Saúde Animal Brasil","identifier":"2a6139a40ddd4d11b020661ea47dfbbd"},"schools":[{"name":"Avicultura","photo":"static/e76e1/e76e133f3fcf42c58878f1f2ffd0ce58.jpeg","segment":{"name":"Saúde Animal Brasil","identifier":"2a6139a40ddd4d11b020661ea47dfbbd"},"identifier":"e76e133f3fcf42c58878f1f2ffd0ce58"},{"name":"Pecuária","photo":"static/a2f7f/a2f7f555cc224f7d964edae14e79bd94.jpeg","segment":{"name":"Saúde Animal Brasil","identifier":"2a6139a40ddd4d11b020661ea47dfbbd"},"identifier":"a2f7f555cc224f7d964edae14e79bd94"}],"profileAccess":"[0,1]","identifier":"6670147eddd74d5cb51af9032afa9339"},"answers":[{"value":"Answer 6","isRight":true,"identifier":"818dbe0747124177b836c28deecb65c6"},{"value":"Answer 7","isRight":false,"identifier":"bad4537e66664684b6725e056c3e5a20"},{"value":"Answer 8","isRight":false,"identifier":"88542ba6f46a4827b7d6993b99048f53"},{"value":"Answer 9","isRight":false,"identifier":"718555c9bcd54feba695e758478dd869"},{"value":"Answer 10","isRight":false,"identifier":"95f753f4e0794250ad88ae51a2f79f08"}],"identifier":"818a1f1593ce4053bc992f9dfb8d3735"},{"title":"Title Question 1","lesson":{"name":"Aula 1 ","teachers":"Professor 1","description":"Teste Descrição","startedAt":"2018-06-20T16:00:00","link":"https://www.youtube.com/watch?v=yf7p-uWcQ-4","options":0,"segment":{"name":"Latam","identifier":"a5a5c132d3474119ab68b8d890955928"},"schools":[{"name":"Suínios","photo":"static/5a303/5a303757ab8345ceb0295cdcd56f956c.jpeg","segment":{"name":"Latam","identifier":"a5a5c132d3474119ab68b8d890955928"},"identifier":"5a303757ab8345ceb0295cdcd56f956c"}],"profileAccess":"[0]","identifier":"1d295f90917c4c1e9eaec0e67b5c7225"},"answers":[{"value":"Answer 1","isRight":true,"identifier":"7f902d50b16847a9a268e961b2a74f97"},{"value":"Answer 2","isRight":false,"identifier":"be47c3539d4f4e9c8940f24a17ea0836"},{"value":"Answer 3","isRight":false,"identifier":"0430551e8c574089b4bd2ae8018bd14b"},{"value":"Answer 4","isRight":false,"identifier":"c35b8bc789ae444598e367f0bf7c5692"},{"value":"Answer 5","isRight":false,"identifier":"7e3813d9c6fe4ee9a0dfdb1016775ea2"}],"identifier":"4921638ee23d45a9918e78891167f1c3"},{"title":"Title Question 1","lesson":{"name":"Aula 1 ","teachers":"Professor 1","description":"Teste Descrição","startedAt":"2018-06-20T16:00:00","link":"https://www.youtube.com/watch?v=yf7p-uWcQ-4","options":0,"segment":{"name":"Latam","identifier":"a5a5c132d3474119ab68b8d890955928"},"schools":[{"name":"Suínios","photo":"static/5a303/5a303757ab8345ceb0295cdcd56f956c.jpeg","segment":{"name":"Latam","identifier":"a5a5c132d3474119ab68b8d890955928"},"identifier":"5a303757ab8345ceb0295cdcd56f956c"}],"profileAccess":"[0]","identifier":"bea2b18a9bf746deb2e9d0f406232af6"},"answers":[{"value":"Answer 1","isRight":true,"identifier":"10db6edede6041e58669504d433dd51a"},{"value":"Answer 2","isRight":false,"identifier":"d0e476d143f34d8cae9e950028b255b7"},{"value":"Answer 3","isRight":false,"identifier":"737f7b4b51be4e7ca3a929b1cfa7ce21"},{"value":"Answer 4","isRight":false,"identifier":"71d2c4aeb3394f8c90161ebff3a499cf"},{"value":"Answer 5","isRight":false,"identifier":"fb3f67c48bd04de4a3ceccbd351eee41"}],"identifier":"6bd1675b4760485ebc507ea49b679189"}]');
                //this.questions = entries;
                console.log(this.questions);
            },
            error => {
                console.log(error);
            });
    }

    appendData(data) {
        this.data = data;
        console.log(this.data);
        this.urlVideo = this.getEmbedVideo(this.data.link);
        this.data.date = moment(this.data.startedAt).format("DD/MM/YYYY");
        this.data.time = moment(this.data.startedAt).format('H:mm');
    }

    getDate(createdAt: string) {
        let newDate = new Date(createdAt.substring(0, 16));
        //2018-06-20T03:44:20.8202154Z
        let date = moment(newDate).format("DD/MM/YYYY");
        let time = moment(newDate).format('HH:mm');
        return String(date + " às " + time);
    }

    handleFormSubmit() {
        this.frmNewPost = $("#frmNewPost");
        this.frmEvaluation = $("#frmEvaluation");

        this.frmNewPost.validate({
            rules: {
                comment: {
                    required: true
                }
            },
        });

        this.frmEvaluation.validate({
            rules: {
                fullname: {
                    required: true
                },
                email: {
                    required: true,
                    email: true,
                },
                phone: {
                    required: true
                },
                user_type: {
                    required: true
                },
                area: {
                    required: true
                },
                school: {
                    required: true
                }
            },
        });
    }

    create() {
        this.loading = true;

        if (this.frmNewPost.valid()) {
            var post = Object.assign({}, this.model);
            post.statusComment = 1;
            post.user = this.currentUser.identifier;
            post.lesson = this.idLesson;
            this.appendComment(post);
        }
    }

    appendComment(comment) {
        this.block('Enviando dados...');
        this._lessonsService.saveComment(comment).subscribe(
            response => {
                console.log(response);
                toastr.success("Dúvida adicionada com sucesso!");
                this.comments.unshift(response.data);
                this.model = {};
                this.unblock();
                setTimeout(() => { this.frmNewPost.validate().resetForm(); }, 400);
                /*$("html, body").animate({ scrollTop: $(document).height()-$(window).height() } , ()=>
                {
                  
                });*/

            },
            error => {
                toastr.error("Ocorreu erro ao enviar os dados.!");
                this.loading = false;
                this.unblock();
            });

    }

    sendResponses() {
        this.loading = true;

        $('.field-answer').each(function() {
            $(this).rules("add", { required: true });
        });

        if (this.frmEvaluation.validate().form()) {
            let data: any = {};
            var answersList: any[] = [];

            $("#modal_evaluation #btn-close-modal").trigger("click");

            $("#frmEvaluation input:radio:checked").each(function(index) {
                let value: string = String($(this).val());
                let arr = value.split("|");
                answersList.push({ question: arr[0], answer: arr[1] });
            });

            data.lesson = this.idLesson;
            data.user = this.currentUser.identifier;
            data.details = answersList;

            setTimeout(() => { this.saveExame(data); }, 300);
        }
    }

    saveExame(data) {
        this.block('Enviando dados...');
        this._lessonsService.saveExam(data).subscribe(
            response => {
                console.log(response);
                swal("Sucesso!", "Sua avaliação foi salva com sucesso.", "success");
                this.unblock();
            },
            error => {
                swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
                this.unblock();
            });
    }

    getEmbedVideo(url) {
        var i, r, rx = /^.*(?:(?:youtu\.be\/|v\/|vi\/|u\/\w\/|embed\/)|(?:(?:watch)?\?v(?:i)?=|\&v(?:i)?=))([^#\&\?]*).*/;
        r = url.match(rx);
        return this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + r[1]);
    }

    getDateNow() {
        var date = new Date();
        var month = String(date.getMonth());
        if (month.length == 1) month = "0" + month;
        var literal_date = date.getDate() + "/" + month + "/" + date.getFullYear() + " às " + date.getHours() + ":" + date.getMinutes();
        return literal_date;
    }

    block(msg) {
        mApp.block('.m-page', {
            overlayColor: '#000000',
            type: 'loader',
            state: 'success',
            message: msg
        });
    }

    unblock() {
        mApp.unblock('.m-page');
    }


}