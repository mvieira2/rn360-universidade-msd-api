import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { _globals } from '../../../globals';
import { HeaderNavService } from "./header-nav.service";


declare let mLayout: any;
declare let mDropdown: any;

@Component({
    selector: "app-header-nav",
    templateUrl: "./header-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {

    currentUser: any = {};
    totalAlerts: number = 0;
    selectedAlert: any = {};
    alerts: any[] = [];
    dataText: any;

    constructor(private _headerService: HeaderNavService) {

    }

    ngOnInit() {
        this.currentUser = _globals.currentUser;
        this.dataText = _globals.dataText.lang[_globals.language];
        this.alerts = _globals.alerts;
        this.getTotalAlerts();

        this._headerService.change.subscribe(alerts => {
            this.alerts = alerts;
            this.saveLocal();
            this.getTotalAlerts();
        });
    }

    ngAfterViewInit() {
        mLayout.initHeader();
        var dropdown1 = new mDropdown('m_dropdown_alert');
        var dropdown2 = new mDropdown('m_dropdown_profile');
    }

    showAlert(itemAlert) {
        this.selectedAlert = itemAlert;
        this.alerts.forEach((item) => {
            if (itemAlert.identifier === item.identifier) {
                item.read = true;
            }
        });
        this.saveLocal();
        this.getTotalAlerts();
    }

    getTotalAlerts() {
        let count: number = 0;
        this.alerts.forEach((item) => {
            if (item.read === undefined || item.read === null) count++;
        });

        this.totalAlerts = count;
        _globals.alerts = this.alerts;
    }

    saveLocal() {
        let label = 'MsdAlerts' + this.currentUser.identifier;
        localStorage.setItem(label, JSON.stringify(this.alerts));
        //console.log( ('MsdAlerts' + this.currentUser.identifier) , _globals.alerts );
    }
}