import { Injectable } from "@angular/core";
import { Output, EventEmitter } from '@angular/core';

@Injectable()
export class HeaderNavService {

    @Output() change: EventEmitter<boolean> = new EventEmitter();

    update(alerts) {
        this.change.emit(alerts);
    }

}