import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Helpers } from '../../../helpers';
import { _globals } from '../../../globals';

@Component({
    selector: "app-footer",
    templateUrl: "./footer.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class FooterComponent implements OnInit {

    dataText: any;

    constructor() {

    }

    ngOnInit() {
        this.dataText = _globals.dataText.lang[_globals.language];
    }

}