import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, Response } from "@angular/http";

import { Download } from "../_models/download";

@Injectable()
export class DownloadsService {

    headers;
    options;

    constructor(private http: Http) {
        this.headers = new Headers({ 'Content-Type': 'application/json' });
        this.options = new RequestOptions({ headers: this.headers });
    }

    getAll() {
        return this.http.get('/api/downloads/').map((response: Response) => response.json());
    }

    create(download: Download) {
        return this.http.post('/api/downloads/', JSON.stringify(download), this.options).map((response: Response) => response.json());
    }

    update(download: Download) {
        return this.http.put('/api/downloads/' + download.identifier, JSON.stringify(download), this.options).map((response: Response) => response.json());
    }

    delete(id: number) {
        return this.http.delete('/api/downloads/' + id).map((response: Response) => response.json());
    }

}