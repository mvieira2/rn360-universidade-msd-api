import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, Response } from "@angular/http";

import { Alert } from "../_models/alert";

@Injectable()
export class AlertsService {

    headers;
    options;

    constructor(private http: Http) {
        this.headers = new Headers({ 'Content-Type': 'application/json' });
        this.options = new RequestOptions({ headers: this.headers });
    }

    getAll() {
        return this.http.get('/api/alerts/').map((response: Response) => response.json());
    }

    create(alert: Alert) {
        return this.http.post('/api/alerts/', JSON.stringify(alert), this.options).map((response: Response) => response.json());
    }

    update(alert: Alert) {
        return this.http.put('/api/alerts/' + alert.identifier, JSON.stringify(alert), this.options).map((response: Response) => response.json());
    }

    delete(id: number) {
        return this.http.delete('/api/alerts/' + id).map((response: Response) => response.json());
    }

}