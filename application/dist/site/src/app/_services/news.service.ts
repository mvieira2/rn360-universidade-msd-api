import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, Response } from "@angular/http";

import { News } from "../_models/news";

@Injectable()
export class NewsService {

    headers;
    options;

    constructor(private http: Http) {
        this.headers = new Headers({ 'Content-Type': 'application/json' });
        this.options = new RequestOptions({ headers: this.headers });
    }

    getAll(query: string = "") {
        return this.http.get('/api/news/' + query).map((response: Response) => response.json());
    }

    getById(id) {
        return this.http.get('/api/news/?query=Identifier:' + id).map((response: Response) => response.json());
    }

    create(news: News) {
        return this.http.post('/api/news/', JSON.stringify(news), this.options).map((response: Response) => response.json());
    }

    update(news: News) {
        return this.http.put('/api/news/' + news.identifier, JSON.stringify(news), this.options).map((response: Response) => response.json());
    }

    delete(id: number) {
        return this.http.delete('/api/news/' + id).map((response: Response) => response.json());
    }

}