import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, Response } from "@angular/http";

import { Lesson } from "../_models/lesson";

@Injectable()
export class LessonsService {

    headers;
    options;

    constructor(private http: Http) {
        this.headers = new Headers({ 'Content-Type': 'application/json' });
        this.options = new RequestOptions({ headers: this.headers });
    }

    getAll() {
        return this.http.get('/api/lessons/').map((response: Response) => response.json());
    }

    getComments() {
        return this.http.get('/api/lessonComments/').map((response: Response) => response.json());
    }

    getQuestions() {
        return this.http.get('/api/lessonQuestion/').map((response: Response) => response.json());
    }

    getById(id) {
        return this.http.get('/api/lessons/?query=Identifier:' + id).map((response: Response) => response.json());
    }

    create(lesson: Lesson) {
        return this.http.post('/api/lessons/', JSON.stringify(lesson), this.options).map((response: Response) => response.json());
    }

    update(lesson: Lesson) {
        return this.http.put('/api/lessons/' + lesson.identifier, JSON.stringify(lesson), this.options).map((response: Response) => response.json());
    }

    delete(id: number) {
        return this.http.delete('/api/lessons/' + id).map((response: Response) => response.json());
    }

    saveExam(exam: any) {
        return this.http.post('/api/lessonExam/', JSON.stringify(exam), this.options).map((response: Response) => response.json());
    }

    saveComment(comment: any) {
        return this.http.post('/api/lessonComments/', JSON.stringify(comment), this.options).map((response: Response) => response.json());
    }

}