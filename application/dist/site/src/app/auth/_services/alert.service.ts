import { Injectable } from "@angular/core";
import { NavigationStart, Router } from "@angular/router";
import { Observable } from "rxjs";
import { Subject } from "rxjs/Subject";

declare var swal: any;

@Injectable()
export class AlertService {
    private subject = new Subject<any>();
    private keepAfterNavigationChange = false;

    constructor(private _router: Router) { }

    success(message) {

    }

    error(error: any, message?: any) {
        let msg: string = "Ocorreu erro ao enviar os dados.";

        if (message) {
            msg = message;
        } else if (error.status === 401) {
            msg = "Acesso negado";
        }

        swal("Ops", msg, "error");
    }
}