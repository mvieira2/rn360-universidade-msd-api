import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { UserService } from "../_services/user.service";
import { Observable } from "rxjs/Rx";
import { _globals } from '../../globals';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private _router: Router, private _userService: UserService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));

        return this._userService.verify().map(
            response => {
                if (response !== null) {
                    var objUser = response.data;
                    if (objUser.profile === 2048) {
                        objUser.segment = { identifier: "admin" };
                        objUser.school = { identifier: "admin" };
                    }

                    if (objUser.segment.identifier == "a5a5c132d3474119ab68b8d890955928") {
                        _globals.language = "br";
                    } else {
                        _globals.language = "br";
                    }

                    _globals.currentUser = response.data;
                    console.log("CurrentUser", _globals.currentUser);
                    return true;
                }
                // error when verify so redirect to login page with the return url
                this._router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
                return false;
            },
            error => {
                // error when verify so redirect to login page with the return url
                this._router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
                return false;
            }).catch(e => {

                if (e.status === 401) {
                    this._router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
                    return Observable.throw('Unauthorized');
                }
                // do any other checking for statuses here
            });

        //return true;
    }
}