import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'search'
})
export class SearchPipe implements PipeTransform {

    transform(items: any[], searchText: string, prop: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        if (!prop) return items;

        searchText = searchText.toLowerCase();

        return items.filter(it => {
            return it[prop].toLowerCase().includes(searchText);
        });
    }

}