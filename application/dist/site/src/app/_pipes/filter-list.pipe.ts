import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterList'
})
export class FilterListPipe implements PipeTransform {

    transform(items: any[], filterText: string, prop: string): any[] {
        if (!items) return [];
        if (!filterText) return items;
        if (!prop) return items;
        if (filterText === 'admin') return items;

        filterText = filterText.toLowerCase();

        return items.filter(it => {
            for (var i in it[prop]) {
                if (typeof it[prop][i] === "object") {
                    for (var j in it[prop][i]) {
                        if (typeof it[prop][i][j] === "string" && it[prop][i][j].toLowerCase() === filterText) return it;
                    }
                } else {

                    if (it[prop][i].toLowerCase() === filterText) return it;

                }
            }
        });
    }

}