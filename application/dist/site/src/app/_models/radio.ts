export class Radio {
    identifier: string;
    name: string;
    description: string;
    segment: any;
    schools: any[];
}