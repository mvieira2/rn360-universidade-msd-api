export class School {
    identifier: string;
    name: string;
    logo: string;
}