export class Download {
    identifier: string;
    name: string;
    description: string;
    segment: any;
    schools: any[];
}