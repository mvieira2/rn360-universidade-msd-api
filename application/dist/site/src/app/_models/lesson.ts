export class Lesson {
    identifier: string;
    name: string;
    teachers: string;
    description: string;
    startedAt: string;
    link: string;
    options: number;
}