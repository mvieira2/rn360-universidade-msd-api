export class Alert {
    identifier: string;
    name: string;
    description: string;
    segment: any;
    schools: any[];
}