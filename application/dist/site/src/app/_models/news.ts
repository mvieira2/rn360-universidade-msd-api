export class News {
    identifier: string;
    name: string;
    description: string;
    segment: any;
    schools: any[];
}