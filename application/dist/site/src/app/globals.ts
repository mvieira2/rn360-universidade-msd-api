//'use strict';
export var _globals =
    {
        //apiURL:"http://localhost:5000/",
        apiURL: "http://msd.embed.com.br/api/",
        currentUser: {},
        currentLesson: null,
        currentNews: null,
        language: null,
        alerts: [],
        segments: [],
        schools: [],
        profiles: {
            0: "Funcionário",
            1: "Distribuidor",
            2048: "Admin"
        },

        dataText:
        {
            lang:
            {
                br:
                {
                    labels:
                    {
                        load: 'Carregando',
                        hello: "Olá",
                        notifications: "Notificações",
                        all: "Todos",
                        alerts: "Alertas",
                        profile: "Meu Perfil",
                        privacy: "Política de Privacidade",
                        support: "Suporte Técnico",
                        disclaimer: "Disclaimer",
                        description: "Descrição",
                        general: "Geral",
                        school: "Escola",
                        date: "Data",
                        time: "Horário"
                    },

                    menu:
                    {
                        home: "Home",
                        lesson: "Aulas",
                        radio: "Rádio",
                        news: "Notícias",
                        download: "Downloads",
                    },

                    fields:
                    {
                        search: "Pesquisar",
                        password: "Senha",
                        cpassword: "Confirmar a senha",
                        comments: "Envia aqui suas dúvidas"
                    },

                    buttons:
                    {
                        send: "Enviar",
                        cancel: "Cancelar",
                        close: "Fechar",
                        back: "Voltar",
                        watch: "Assistir",
                        view: "Visualizar",
                        changePassword: "Alterar a senha",
                        logout: "Sair",
                        download: "Clique para fazer download",
                        news: "Ver notícia"
                    },

                    pages:
                    {
                        home:
                        {
                            newsTitle: 'Veja abaixo as últimas aulas disponíveis',
                            nextLessons: 'Próximas Aulas',
                            lastNews: "Últimas Notícias"
                        },

                        profile:
                        {
                            evaluations: 'Minhas Avaliações',
                            titleBox: 'Você já assisti a <span class="m--font-primary f600">35</span> aulas e participou de <span class="m--font-primary f600">20</span> avaliações com uma média de acerto de <span class="m--font-primary f600">80%</span>.',
                            table: ["Nome da aula", "Categoria", "Data", "Avaliação", "Acertos"],
                            ranking:
                            {
                                titSchool: "Ranking da sua Escola",
                                titSegment: "Ranking geral do Segmento",
                                table: ["Posição", "Nome completo", "Pontuação"],
                            }
                        },

                        lessons:
                        {
                            title: "Aulas",
                            teachers: 'Professores',
                            txtQuestion: 'Terminou de assistir à aula? Não deixe de testar seus conhecimentos e somar pontos.',
                            linkEvaluation: "Clique aqui para liberar a avaliação.",
                            knowledge: "Teste de conhecimentos:",
                        },

                        radio:
                        {
                            edition: 'Edição',
                        }
                    }
                }
            }
        }
    };