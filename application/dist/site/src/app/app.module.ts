import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme/theme.component';
import { LayoutModule } from './theme/layouts/layout.module';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScriptLoaderService } from "./_services/script-loader.service";
import { ThemeRoutingModule } from "./theme/theme-routing.module";
import { AuthModule } from "./auth/auth.module";
import { MainPipeModule } from "./_pipes/main-pipe/main-pipe.module";
import { SchoolsService } from './_services/schools.service';
import { SegmentsService } from './_services/segments.service';
import { LessonsService } from './_services/lessons.service';
import { AlertsService } from './_services/alerts.service';
import { RadioService } from './_services/radio.service';
import { NewsService } from './_services/news.service';
import { DownloadsService } from './_services/downloads.service';
import { HeaderNavService } from './theme/layouts/header-nav/header-nav.service';

@NgModule({
    declarations: [
        ThemeComponent,
        AppComponent,
    ],
    imports: [
        LayoutModule,
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        ThemeRoutingModule,
        AuthModule,
        MainPipeModule
    ],
    providers: [ScriptLoaderService, SchoolsService, SegmentsService, LessonsService, AlertsService, RadioService, NewsService, DownloadsService, HeaderNavService],
    bootstrap: [AppComponent]
})
export class AppModule { }