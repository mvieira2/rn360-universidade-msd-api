const proxy = [
  {
    context: '/api',
    //target: 'http://localhost:5000',
    target: 'http://msd.embed.com.br/api',
    pathRewrite: {'^/api' : '/'}
  }
];
module.exports = proxy;