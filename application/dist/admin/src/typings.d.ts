/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
    id: string;
}
interface JQuery {
    mMenu(options: any): JQuery;
    animateClass(options: any): JQuery;
    setActiveItem(item: any): JQuery;
    getPageTitle(item: any): JQuery;
    getBreadcrumbs(item: any): JQuery;
    validate(options: any): JQuery;
    valid(): JQuery;
    validator(options?:any): JQuery;
    resetForm(): JQuery;
    markdown(): JQuery;
    toDataURL();
    cropper(options?: any): JQuery;
    selectpicker(options?: any): JQuery;
    mask(callback?: any , options?: any): JQuery;
    dropzone(options?: any): JQuery;
    datepicker(options?: any): JQuery;
    timepicker(options?: any): JQuery;
    modal(options?: any): JQuery;
    summernote(options?: any, markup?: any): JQuery;
    rules(action?: any, options?: any): JQuery;
}