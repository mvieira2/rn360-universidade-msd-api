import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {FilterNamePipe} from "../filter-name.pipe";
import {FilterListPipe} from "../filter-list.pipe";
import { SearchPipe } from '../search.pipe';

@NgModule({
  declarations:[FilterNamePipe , SearchPipe , FilterListPipe],
  imports:[CommonModule],
  exports:[FilterNamePipe , SearchPipe, FilterListPipe]
})

export class MainPipeModule { }