import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterNamePipe implements PipeTransform {
  
  transform(items: any[], filterText: string , prop:string ): any[] 
  {
    	if(!items) return [];
    	if(!filterText) return items;
    	if(!prop) return items;

		filterText = filterText.toLowerCase();
		
		return items.filter( it => 
		{
          return it[prop].toLowerCase().includes(filterText);
    	});
   }
   
}