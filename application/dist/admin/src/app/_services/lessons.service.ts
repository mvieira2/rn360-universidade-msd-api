import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions, Response} from "@angular/http";

import {Lesson} from "../_models/lesson";

@Injectable()
export class LessonsService {
	
	headers;
	options;

	constructor(private http: Http) 
	{
		this.headers = new Headers({ 'Content-Type': 'application/json' });
		this.options = new RequestOptions({ headers: this.headers });
	}

	getAll() {
		return this.http.get('/api/lessons/').map((response: Response) => response.json());
	}

	create(lesson: Lesson) 
	{
		return this.http.post( '/api/lessons/', JSON.stringify(lesson) , this.options ).map((response: Response) => response.json());
	}

	update(lesson: Lesson) {
		return this.http.put('/api/lessons/' + lesson.identifier, JSON.stringify(lesson), this.options ).map((response: Response) => response.json());
	}

	delete(id: number) {
		return this.http.delete( '/api/lessons/' + id ).map((response: Response) => response.json());
	}

	getAllQuestions() {
		return this.http.get('/api/lessonQuestion/').map((response: Response) => response.json());
	}

	updateQuestion(lessonQuestion:any) 
	{
		return this.http.put( '/api/lessonQuestion/'+ lessonQuestion.identifier, JSON.stringify(lessonQuestion) , this.options ).map((response: Response) => response.json());
	}

	createQuestion(lessonQuestion:any) 
	{
		return this.http.post( '/api/lessonQuestion/', JSON.stringify(lessonQuestion) , this.options ).map((response: Response) => response.json());
	}

}