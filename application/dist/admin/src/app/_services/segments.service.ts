import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions, Response} from "@angular/http";

import {Segment} from "../_models/segment";

@Injectable()
export class SegmentsService {
	
	headers;
	options;

	constructor(private http: Http) 
	{
		this.headers = new Headers({ 'Content-Type': 'application/json' });
		this.options = new RequestOptions({ headers: this.headers });
	}

	getAll() {
		return this.http.get('/api/segments/').map((response: Response) => response.json());
	}

	create(segment: Segment) 
	{
		return this.http.post( '/api/segments/', JSON.stringify(segment) , this.options ).map((response: Response) => response.json());
	}

	update(segment: Segment) {
		return this.http.put('/api/segments/' + segment.identifier, JSON.stringify(segment), this.options ).map((response: Response) => response.json());
	}

	delete(id: number) {
		return this.http.delete( '/api/segments/' + id ).map((response: Response) => response.json());
	}

}