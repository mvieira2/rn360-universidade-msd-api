import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions, Response} from "@angular/http";

import {School} from "../_models/school";

@Injectable()
export class SchoolsService {
	
	headers;
	options;

	constructor(private http: Http) 
	{
		this.headers = new Headers({ 'Content-Type': 'application/json' });
		this.options = new RequestOptions({ headers: this.headers });
	}

	getAll() {
		return this.http.get('/api/schools/').map((response: Response) => response.json());
	}

	create(school: School) 
	{
		return this.http.post( '/api/schools/', JSON.stringify(school) , this.options ).map((response: Response) => response.json());
	}

	update(school: School) {
		return this.http.put('/api/schools/' + school.identifier, JSON.stringify(school), this.options ).map((response: Response) => response.json());
	}

	delete(id: number) {
		return this.http.delete( '/api/schools/' + id ).map((response: Response) => response.json());
	}

}