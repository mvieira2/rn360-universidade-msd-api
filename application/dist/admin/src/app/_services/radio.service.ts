import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions, Response} from "@angular/http";

import {Radio} from "../_models/radio";

@Injectable()
export class RadioService {
	
	headers;
	options;

	constructor(private http: Http) 
	{
		this.headers = new Headers({ 'Content-Type': 'application/json' });
		this.options = new RequestOptions({ headers: this.headers });
	}

	getAll() {
		return this.http.get('/api/radios/').map((response: Response) => response.json());
	}

	create(radio: Radio) 
	{
		return this.http.post( '/api/radios/', JSON.stringify(radio) , this.options ).map((response: Response) => response.json());
	}

	update(radio: Radio) {
		return this.http.put('/api/radios/' + radio.identifier, JSON.stringify(radio), this.options ).map((response: Response) => response.json());
	}

	delete(id: number) {
		return this.http.delete( '/api/radios/' + id ).map((response: Response) => response.json());
	}

}