import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Helpers } from "./helpers";
import { SegmentsService } from './_services/segments.service';  
import { SchoolsService } from './_services/schools.service';  
import { _globals } from './globals'; 

@Component({
    selector: 'body',
    templateUrl: './app.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
    title = 'app';
    globalBodyClass = 'm-page--loading-non-block m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default';

    constructor(private _router: Router , private _segmentsService: SegmentsService , private _schoolsService: SchoolsService) 
    {
         this.getAll();
    }

    ngOnInit() {
        this._router.events.subscribe((route) => {
            if (route instanceof NavigationStart) {
                Helpers.setLoading(true);
                Helpers.bodyClass(this.globalBodyClass);
            }
            if (route instanceof NavigationEnd) {
                Helpers.setLoading(false);
            }
        });
    }

    getAll()
    {
      this._segmentsService.getAll().subscribe(
        response => 
        {
          _globals.segments = response.data.entries;
          console.log(_globals.segments);
        },
        error => {
          console.log(error);
        });

      this._schoolsService.getAll().subscribe(
        response => 
        {
          _globals.schools = response.data.entries;
          console.log(_globals.schools);
        },
        error => {
          console.log(error);
        });
    }
}