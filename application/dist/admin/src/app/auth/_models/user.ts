export class User 
{
	identifier: string;
	email: string;
	phone: string;
	profile: number;
	name: string;
}