import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {UserService} from "../_services/user.service";
import {Observable} from "rxjs/Rx";

@Injectable()
export class AuthGuard implements CanActivate {

	constructor(private _router: Router, private _userService: UserService) {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean 
	{
		let currentUser = JSON.parse(localStorage.getItem('currentUser'));
		//console.log( "currentUser", currentUser );
		return this._userService.verify().map(
			data => {
				console.log(data);
				if (data !== null) 
				{
					// logged in so return true
					return true;
				}


				// error when verify so redirect to login page with the return url
				this._router.navigate(['/login']);
				return false;
			},
			error => {
				// error when verify so redirect to login page with the return url
				this._router.navigate(['/login']);
				return false;
			}).catch(e => {
				
	            if (e.status === 401) {
	            	this._router.navigate(['/login']);
	                return Observable.throw('Unauthorized');
	            }
	            // do any other checking for statuses here
	        });

		//return true;
	}
}