import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions, Response} from "@angular/http";

import {User} from "../_models/index";

@Injectable()
export class UserService {
	
	headers;
	options;

	constructor(private http: Http) 
	{
		this.headers = new Headers({ 'Content-Type': 'application/json' });
		this.options = new RequestOptions({ headers: this.headers });
	}

	verify() {
		return this.http.get('/api/users/auth/').map((response: Response) => response.json());
	}

	forgotPassword(value: string) {
		return this.http.post('/api/users/forget-password/', JSON.stringify({email:value}) ).map((response: Response) => response.json());
	}

	getAll() {
		return this.http.get('/api/users/').map((response: Response) => response.json());
	}

	getProfiles() {
		return this.http.get('/api/profiles/').map((response: Response) => response.json());
	}

	getById(id: number) {
		return this.http.get('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
	}

	create(user: User) 
	{
		return this.http.post( '/api/users/', JSON.stringify(user) , this.options ).map((response: Response) => response.json());
	}

	update(user: User) {
		return this.http.put('/api/users/' + user.identifier, JSON.stringify(user), this.options ).map((response: Response) => response.json());
	}

	delete(id: number) {
		return this.http.delete( '/api/users/' + id ).map((response: Response) => response.json());
	}

	// private helper methods

	private jwt() {
		// create authorization header with jwt token
		let currentUser = JSON.parse(localStorage.getItem('currentUser'));
		if (currentUser && currentUser.token) {
			let headers = new Headers({'Authorization': 'Bearer ' + currentUser.token});
			return new RequestOptions({headers: headers});
		}
	}
}