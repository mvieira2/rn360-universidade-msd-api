import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from "../auth/_guards/auth.guard";

const routes: Routes = [
    {
        "path": "",
        "component": ThemeComponent,
        "canActivate": [AuthGuard],
        "children": [
            {
                "path": "index",
                "loadChildren": ".\/pages\/aside\/index\/index.module#IndexModule"
            },
            {
                "path": "schools",
                "loadChildren": ".\/pages\/default\/schools\/schools.module#SchoolsModule"
            },
            {
                "path": "classrooms",
                "loadChildren": ".\/pages\/default\/classrooms\/classrooms.module#ClassroomsModule"
            },
            {
                "path": "alerts",
                "loadChildren": ".\/pages\/default\/alerts\/alerts.module#AlertsModule"
            },
            {
                "path": "news",
                "loadChildren": ".\/pages\/default\/news\/news.module#NewsModule"
            },
            {
                "path": "radio",
                "loadChildren": ".\/pages\/default\/radio\/radio.module#RadioModule"
            },
            {
                "path": "downloads",
                "loadChildren": ".\/pages\/default\/downloads\/downloads.module#DownloadsModule"
            },
            {
                "path": "profile",
                "loadChildren": ".\/pages\/default\/profile\/profile.module#ProfileModule"
            },
            {
                "path": "404",
                "loadChildren": ".\/pages\/default\/not-found\/not-found.module#NotFoundModule"
            },
            {
                "path": "",
                "redirectTo": "index",
                "pathMatch": "full"
            }
        ]
    },
    {
        "path": "**",
        "redirectTo": "404",
        "pathMatch": "full"
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ThemeRoutingModule { }