import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { RadioComponent } from './radio.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { MainPipeModule } from '../../../../_pipes/main-pipe/main-pipe.module';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": RadioComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule , FormsModule , MainPipeModule
    ], exports: [
        RouterModule
    ], declarations: [
        RadioComponent
    ]
})
export class RadioModule {



}