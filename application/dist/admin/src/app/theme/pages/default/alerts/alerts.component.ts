import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { AlertsService } from '../../../../_services/alerts.service';  
import { _globals } from '../../../../globals'; 

declare var swal:any;
declare var mApp:any;

@Component({
    selector: "app-inner",
    templateUrl: "./alerts.component.html",
    encapsulation: ViewEncapsulation.None
})
export class AlertsComponent implements OnInit, AfterViewInit {

    frmNewAlerts:any;
    searchText:any;
    model: any = {};
    comboSchools:any = [];
    filterArea:any;
    profiles: any;
    filterSchool:any;
    loading = false;
    topics=[1,2,3,4,5,6,7,8];
    optsSchools:any = [];
    boolNewItem:boolean = false;
    questions:any = [];
    segments:any = [];
    schools:any[] = [];
    selectedSchools:any[] = [];
    list: any[] = [];

    constructor(private _script: ScriptLoaderService , private _alertsService: AlertsService ) {

    }

    ngOnInit() 
    {
       this.profiles = _globals.profiles;
       this.segments = _globals.segments;
       this.schools = _globals.schools;
       this.getAll();
    }

    ngAfterViewInit() 
    {
        this.frmNewAlerts = $("#frmNewAlerts");
        $('#m_modal_1').on('hidden.bs.modal', (e)=> { this.model = Object.assign({}, {});});
        $('#filter-areas').selectpicker({noneSelectedText:"Filtrar pelo segmento"});
        this.handleFormSubmit();
    }

    getAll()
    {
      this.block('Carregando...');

      this._alertsService.getAll().subscribe(
        response => {
          this.list = response.data.entries;
          this.unblock();
        },
        error => {
          this.loading = false;
          this.unblock();
        });

    }

    selectArea()
    {
      this.comboSchools = [];
      this.filterSchool = null;
      
      if( this.filterArea !== "" )
      {
        for( var i in this.schools )
        {
          if( this.schools[i].segment.identifier === this.filterArea )
          {
            this.comboSchools.push(this.schools[i]);
          }
        }

        if( this.comboSchools.length > 0) this.showSchoolsList();

      } else {
        this.comboSchools = this.schools;
        this.showSchoolsList();
      }
    }

    getIdArea(label)
    {
      for( var i in this.segments )
      {
        if( label.toLowerCase() === this.segments[i].name.toLowerCase() ) return this.segments[i].identifier;
      }
      return null;
    }

    showAreaList()
    {
      setTimeout(()=>
      { 
        $('#filter-areas-modal')
        .selectpicker('destroy')
        .selectpicker({noneSelectedText:"Escolha a qual segmento o alerta pertence"});
      },300);
    }

    showSchoolsList()
    {
      setTimeout(()=>{ $('#filter-schools').selectpicker('destroy').selectpicker({noneSelectedText:"Filtrar por escola"}); },300);    
    }

    handleFormSubmit() 
    {
      this.frmNewAlerts.validate({
        rules: {
          name: {
            required: true
          },
          description:{
            required: true
          },
          segment:{
            required: true
          }
        },
      });
    }

    edit(item)
    {
       this.model = Object.assign({}, {});
       this.boolNewItem = false;
       this.frmNewAlerts.validate().resetForm();
       this.selectedSchools = item.schools;
       this.showAreaList();
       
       setTimeout(()=> { this.model = item; } , 100 );
    }

    new()
    {
      this.boolNewItem = true;
      this.model = Object.assign({}, {});
      this.model.segment = {};
      this.model.schools = [];
      this.frmNewAlerts.validate().resetForm();
      this.showAreaList();
    }

    create() 
    {
      this.loading = true;

      if( this.frmNewAlerts.valid() )
      {
        this.model.segment = this.model.segment.identifier;
        this.model.schools = this.selectedSchools;
        
        if( this.model.schools && this.model.schools.length === 0 ) this.model.schools = null;
        if( this.model.schools ) this.validateSchools();

        $("#btn-close-modal").trigger("click");

        if( this.boolNewItem )
        {
          this.append();  
        } else{
          this.update();
        }

      }

    }

    append()
    {
      this.loading = true;
      this.block('Enviando dados...');
      this._alertsService.create(this.model).subscribe(
        response => 
        {
          console.log(response);
          swal("Sucesso!", "Alerta criado com sucesso.", "success");
          this.list.push( response.data );
          this.model = {};
          this.loading = false;
          this.unblock();
        },
        error => {

          swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
          this.loading = false;
          this.unblock();

        });
    }

    update()
    {
      this.loading = true;
      this.block('Enviando dados...');
      this._alertsService.update(this.model).subscribe(
        response => 
        {
          console.log(response);
          swal("Sucesso!", "Alerta atualizado com sucesso.", "success");
          this.model = {};
          
          for( var i in this.list )
          {
            if( this.list[i].identifier === response.data.identifier )
            {
                let obj = Object.assign({}, response.data);
                obj.segment = { identifier:this.list[i].segment };
                this.list[i] = obj;
            }
          }

          this.loading = false;
          this.unblock();

        },
        error => {
          swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
          this.loading = false;
          this.unblock();
        });
    }

    checkSchoolValue(value)
    {
      for( var i in this.model.schools )
      {
        if( this.model.schools[i].identifier === value ) return true;
      }
      return false;
    }

    onSelectSchool(item)
    {
      if( $(item).is(":checked") )
      {
        this.selectedSchools.push(item.value);
      } else {
        this.selectedSchools = this.selectedSchools.filter(function(value){ return value !== item.value; });
      }
    }

    validateSchools()
    {
      let list = [];

      this.model.schools.forEach((item)=> 
      {
         if(typeof item === "string")
         {
           list.push(item);
         } else {
           list.push(item.identifier);
         }
      });

      this.model.schools = list;
    }

    verifySegment()
    {
      if( this.model.segment && this.model.segment.identifier && this.hasSchollsOnSegment() )
      {
        return true;
      } else {
        return false;
      }
    }

    hasSchollsOnSegment()
    {
      for( var i in this.schools )
      {
        if( this.schools[i].segment.identifier === this.model.segment.identifier ) return true;
      }
      return false;
    }

    openDeleteModal(idItem)
    {
      swal({
          title: 'Você tem certeza que deseja deletar esse alerta?',
          //text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Sim',
          cancelButtonText:'Cancelar'
      }).then((result)=> {
          
          if (result.value) 
          {
              this.loading = true;
              this.block('Enviando dados...');
              this._alertsService.delete(idItem).subscribe(
              response => 
              {
                swal("Sucesso!", "Usuário deletado com sucesso.", "success");
                this.list = this.list.filter(function(item){ return item.identifier !== idItem; });
                this.loading = false;
                this.unblock();
              },
              error => {
                swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
                this.loading = false;
                this.unblock();
              });
          }
      });
    }

    block(msg)
    {
      mApp.block('.m-page', {
          overlayColor: '#000000',
          type: 'loader',
          state: 'success',
          message: msg
      });
    }

    unblock(){
      mApp.unblock('.m-page');
    }

}