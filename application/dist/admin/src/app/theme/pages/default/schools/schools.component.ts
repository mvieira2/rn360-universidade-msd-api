import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { SchoolsService } from '../../../../_services/schools.service';  
import { _globals } from '../../../../globals';  

declare var Dropzone:any;
declare var swal:any;
declare var mApp:any;

@Component({
    selector: "app-inner",
    templateUrl: "./schools.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class SchoolsComponent implements OnInit, AfterViewInit 
{
    apiURL:any;
    frmNewSchool:any;
    model: any = {};
    loading = false;
    boolNewSchool:boolean = false;
    filterSegment:string;
    myDropzone:any;
    file:any;
    segments:any = [];
    schools:any[] = [];

    constructor(private _script: ScriptLoaderService , private _schoolService: SchoolsService ) {

    }
    
    ngOnInit() 
    {
      this.apiURL = _globals.apiURL;
      this.segments = _globals.segments;
      this.getAll();
    }
    
    ngAfterViewInit() 
    {
        this.frmNewSchool = $("#frmNewSchool");
        this.handleFormSubmit();

        Dropzone.options.mDropzoneSchool = 
        {
            init:function()
            {
              this.destroy();
            }
        };

        setTimeout( ()=>{ this.appendDropZone() } , 500 );
    }

    getAll()
    {
      this.block("Carregando...");

      this._schoolService.getAll().subscribe(
        response => {
          this.schools = response.data.entries;
          this.unblock();
        },
        error => {
          this.loading = false;
          this.unblock();
        });
    }

    create() 
    {
      if( this.frmNewSchool.valid() )
      {
        $("#btn-close-modal").trigger("click");

        this.model.segment = this.model.segment.identifier;

        console.log(this.file);
        
        if( this.file )
        {
          this.model.photo = this.file.dataURL;
        } else {
          this.model.photo = null;
        }

        if( this.boolNewSchool )
        {
          this.append();  
        } else{
          this.update();
        }
      }

    }

    edit(school)
    {
       this.model = Object.assign({}, {});
       this.boolNewSchool = false;
       this.frmNewSchool.validate().resetForm(); 

       console.log(school);

       if( school.photo ) this.appendImage( this.getImage(school.photo) );
       
       setTimeout(()=> { this.model = school; } , 100 );
       setTimeout(()=> { $('#filter-areas-modal').selectpicker('destroy').selectpicker({noneSelectedText:"Escolha a qual segmento a escola pertence"}); } , 200 );
    }

    new()
    {
      this.model = Object.assign({}, {});
      this.model.segment = { identifier:"" };
      this.myDropzone.removeAllFiles(true);
      this.boolNewSchool = true;
      this.frmNewSchool.validate().resetForm();

      setTimeout(()=> 
      {
         $('#filter-areas-modal').selectpicker('destroy').selectpicker({noneSelectedText:"Escolha a qual segmento a escola pertence"});
       } , 300 );
    }

    append()
    {
      this.loading = true;
      this.block("Enviando Dados...");
      this._schoolService.create(this.model).subscribe(
        response => 
        {
          console.log(response);
          swal("Sucesso!", "Escola criada com sucesso.", "success");
          this.schools.push( response.data );
          _globals.schools = this.schools;

          this.model = {};
          this.loading = false;
          this.unblock();
        },
        error => {

          //this.showAlert('alertSignin');
          //this._alertService.error(error);
          swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
          this.loading = false;
          this.unblock();
        });

    }

    update()
    {
      this.loading = true;
      this.block("Enviando Dados...");
      this._schoolService.update(this.model).subscribe(
        response => 
        {
          console.log(response);
          this.model = {};
          let Image = response.data.photo;
          console.log(Image);

          swal("Sucesso!", "Escola atualizada com sucesso.", "success");
          
          for( var i in this.schools )
          {
            if( this.schools[i].identifier === response.data.identifier )
            {
                this.schools[i] = response.data;
                
                if( this.file )
                {
                  this.schools[i].photo = this.file.dataURL;
                  this.file = null;
                }
            }
          }

          console.log(this.schools);
          _globals.schools = this.schools;
          this.loading = false;
          this.unblock();
        },
        error => {
          swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
          this.loading = false;
          this.unblock();
        });
    }

    appendImage(url)
    {
      var mockFile = 
      { 
            type: 'image/jpeg', 
            status: Dropzone.ADDED, 
            url: url
        };

        this.myDropzone.removeAllFiles(true);
        this.myDropzone.emit("addedfile", mockFile);
        this.myDropzone.emit("thumbnail", mockFile, url );
        this.myDropzone.files.push(mockFile);
    }

    filterTab(txt)
    {
      this.filterSegment = txt;
    }

    handleFormSubmit() 
    {
      this.frmNewSchool.validate({
        rules: {
          name: {
            required: true
          },
          segment:{
            required: true
          }
        },
      });
    }

    appendDropZone()
    {
      let _ref = this;

      $('#m-dropzone-school').dropzone({
          paramName: "file", // The name that will be used to transfer the file
          maxFiles: 1,
          maxFilesize: 10, // MB
          resizeWidth:150,
          resizeHeight:150,
          addRemoveLinks: true,
          acceptedFiles: "image/*,application/pdf,.psd",
          init:function()
          {
            _ref.myDropzone = this;
            //console.log("init drop school");
          },
          accept: function(file, done) 
          {
              _ref.file = file;
              done(); 
          }
      });
    }

    openDeleteModal(id)
    {
      swal({
          title: 'Você tem certeza que deseja deletar essa escola?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Sim',
          cancelButtonText:'Cancelar'
      }).then((result)=> {
          
          if (result.value) 
          {
              this.loading = true;
              this.block("Enviando Dados...");
              this._schoolService.delete(id).subscribe(
              response => 
              {
                swal("Sucesso!", "Escola deletada com sucesso.", "success");
                this.schools = this.schools.filter(function(item){ return item.identifier !== id; });
                _globals.schools = this.schools;
                this.loading = false;
                this.unblock();
              },
              error => {
                swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
                this.loading = false;
                this.unblock();
              });
          }
      });
    }

    getImage(imgURL)
    {
      if(imgURL)
      {
        if( imgURL.indexOf("base64") != -1 )
        {
          return imgURL;
        } else {
          return this.apiURL + imgURL;
        }
      }  else {
        return null;
      }
    }

    block(msg)
    {
      mApp.block('.m-page', {
          overlayColor: '#000000',
          type: 'loader',
          state: 'success',
          message: msg
      });
    }

    unblock(){
      mApp.unblock('.m-page');
    }

}