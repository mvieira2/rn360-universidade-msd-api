import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { RadioService } from '../../../../_services/radio.service';  
import { _globals } from '../../../../globals'; 

declare var swal:any;
declare var mApp:any;
declare var Dropzone:any;

@Component({
    selector: "app-inner",
    templateUrl: "./radio.component.html",
    encapsulation: ViewEncapsulation.None
})
export class RadioComponent implements OnInit, AfterViewInit {

    apiURL:any;
    frmNewAlerts:any;
    searchText:any;
    model: any = {};
    comboSchools:any = [];
    filterArea:any;
    filterSchool:any;
    file:any;
    profiles:any;
    loading = false;
    topics=[1,2,3,4,5,6,7,8];
    optsSchools:any = [];
    boolNewItem:boolean = false;
    questions:any = [];
    myDropzone:any;
    segments:any = [];
    schools:any[] = [];
    list: any[] = [];
    selectedSchools:any[] = [];

    constructor(private _script: ScriptLoaderService , private _radioService: RadioService) {

    }

    ngOnInit() 
    {
       this.apiURL = _globals.apiURL;
       this.profiles = _globals.profiles;
       this.segments = _globals.segments;
       this.schools = _globals.schools;
       this.getAll();
    }

    ngAfterViewInit() 
    {
        this.frmNewAlerts = $("#frmNewAlerts");
        $('#filter-areas').selectpicker({noneSelectedText:"Filtrar pelo segmento"});
        $('#m_modal_1').on('hidden.bs.modal', (e)=> { this.model = Object.assign({}, {});});
        Dropzone.options.mDropzoneSchool = 
        {
            init:function()
            {
              this.destroy();
            }
        };

        setTimeout( ()=>{ this.appendDropZone() } , 500 );

        this.handleFormSubmit();
    }

    getAll()
    {
      this.block("Carregando...");

      this._radioService.getAll().subscribe(
        response => {
          this.list = response.data.entries;
          this.unblock();
        },
        error => {
          this.loading = false;
          this.unblock();
        });

    }

    appendDropZone()
    {
      let _ref = this;

      $('#m-dropzone-school').dropzone({
          paramName: "file", // The name that will be used to transfer the file
          maxFiles: 2,
          maxFilesize: 10, // MB
          resizeWidth:150,
          resizeHeight:150,
          addRemoveLinks: true,  
          previewTemplate: document.getElementById('template-preview').innerHTML,
          acceptedFiles: "audio/*",
          init:function()
          {
             _ref.myDropzone = this;
             _ref.myDropzone.on("addedfile", function(file, dataUrl) {
              
              if( file.url )
              {
                  _ref.addPlayer(file.url);
              } else {
                  var reader = new FileReader();
                  reader.addEventListener("loadend", function(event:any) 
                  { 
                    _ref.addPlayer(event.target.result);
                  });
                  reader.readAsDataURL(file);
              } 

            });
          }
      })
    }

    selectArea()
    {
      this.comboSchools = [];
      this.filterSchool = null;
      
      if( this.filterArea !== "" )
      {
        for( var i in this.schools )
        {
          if( this.schools[i].segment.identifier === this.filterArea )
          {
            this.comboSchools.push(this.schools[i]);
          }
        }

        if( this.comboSchools.length > 0) this.showSchoolsList();

      } else {
        this.comboSchools = this.schools;
        this.showSchoolsList();
      }
    }

    showAreaList()
    {
      setTimeout(()=>
      { 
        $('#filter-areas-modal')
        .selectpicker('destroy')
        .selectpicker({noneSelectedText:"Escolha a qual segmento o alerta pertence"});
      },300);
    }

    showSchoolsList()
    {
      setTimeout(()=>{ $('#filter-schools').selectpicker('destroy').selectpicker({noneSelectedText:"Filtrar por escola"}); },300);    
    }

    handleFormSubmit() 
    {
      this.frmNewAlerts.validate({
        rules: {
          name: {
            required: true
          },
          edition: {
            required: true
          },
          description:{
            required: true
          },
          segment:{
            required: true
          },
          topicgroup:
          {
            required: true
          }
        },
      });
    }

    edit(item)
    {
       this.model = Object.assign({}, {});
       this.boolNewItem = false;
       this.selectedSchools = item.schools;
       this.frmNewAlerts.validate().resetForm();
       
       if(item.audioFile) this.appendFile( this.getFile(item.audioFile) );
       this.showAreaList();
       
       setTimeout(()=> { this.model = item; } , 100 );
    }

    new()
    {
      this.boolNewItem = true;
      this.model = Object.assign({}, {});
      this.myDropzone.removeAllFiles(true);
      this.model.segment = {};
      this.model.schools = [];
      this.frmNewAlerts.validate().resetForm();
      this.showAreaList();
    }

    create() 
    {
      this.loading = true;

      if( this.frmNewAlerts.valid() )
      {
        $("#btn-close-modal").trigger("click");

        this.model.segment = this.model.segment.identifier;
        this.model.schools = this.selectedSchools;
        
        if( this.model.schools && this.model.schools.length === 0 ) this.model.schools = null;
        if( this.model.schools ) this.validateSchools();

        if( this.model.audioFile && this.model.audioFile.indexOf("base64") === -1 )
        {
          this.model.audioFile = null;
        }

        console.log(this.model);

        if( this.boolNewItem )
        {
          this.append();  
        } else{
          this.update();
        }

      }

    }

    checkSchoolValue(value)
    {
      for( var i in this.model.schools )
      {
        if( this.model.schools[i].identifier === value ) return true;
      }
      return false;
    }

    onSelectSchool(item)
    {
      if( $(item).is(":checked") )
      {
        this.selectedSchools.push(item.value);
      } else {
        this.selectedSchools = this.selectedSchools.filter(function(value){ return value !== item.value; });
      }
    }

    verifySegment()
    {
      if( this.model.segment && this.model.segment.identifier && this.hasSchollsOnSegment() )
      {
        return true;
      } else {
        return false;
      }
    }

    hasSchollsOnSegment()
    {
      for( var i in this.schools )
      {
        if( this.schools[i].segment.identifier === this.model.segment.identifier ) return true;
      }
      return false;
    }

    append()
    {
      this.loading = true;
      this.block("Enviando Dados...");
      this._radioService.create(this.model).subscribe(
        response => 
        {
          swal("Sucesso!", "Radio criado com sucesso.", "success");
          this.list.push( response.data );
          this.model = {};
          this.loading = false;
          this.unblock();
        },
        error => 
        {
          swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
          this.loading = false;
          this.unblock();
        });
    }

    update()
    {
      this.loading = true;
      this.block("Enviando Dados...");
      this._radioService.update(this.model).subscribe(
        response => 
        {
          console.log(response);
          swal("Sucesso!", "Notícia atualizada com sucesso.", "success");
          
          for( var i in this.list )
          {
            if( this.list[i].identifier === response.data.identifier )
            {
                let obj = Object.assign({}, response.data);
                obj.segment = { identifier:this.list[i].segment };
                this.list[i] = obj;
            }
          }

          this.loading = false;
          this.unblock();

        },
        error => {
          swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
          this.loading = false;
          this.unblock();
        });
    }

    validateSchools()
    {
      let list = [];

      this.model.schools.forEach((item)=> 
      {
         if(typeof item === "string")
         {
           list.push(item);
         } else {
           list.push(item.identifier);
         }
      });

      this.model.schools = list;
    }

    appendFile(url)
    {
      var mockFile = 
      { 
            type: 'audio/mpeg', 
            status: Dropzone.ADDED, 
            url: url
        };

        this.addPlayer(url);
        this.myDropzone.removeAllFiles(true);
        this.myDropzone.emit("addedfile", mockFile);
        this.myDropzone.emit("thumbnail", mockFile, "./assets/admin/media/img/ico-audio.png" );
        this.myDropzone.files.push(mockFile);
    }

    addPlayer(url)
    {
      this.model.audioFile = url;
      $("#audio-player").html('<audio controls><source src="'+ url +'" type="audio/mpeg"></audio>');
    }

    openDeleteModal(idItem)
    {
      swal({
          title: 'Você tem certeza que deseja deletar esse rádio?',
          //text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Sim',
          cancelButtonText:'Cancelar'
      }).then((result)=> {
          
          if (result.value) 
          {
              this.loading = true;
              this.block("Enviando Dados...");
              this._radioService.delete(idItem).subscribe(
              response => 
              {
                swal("Sucesso!", "Rádio deletado com sucesso.", "success");
                this.list = this.list.filter(function(item){ return item.identifier !== idItem; });
                this.loading = false;
                this.unblock();
              },
              error => {
                swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
                this.loading = false;
                this.unblock();
              });
          }
      });
    }

    getFile(fileURL)
    {
      if(fileURL)
      {
        if( fileURL.indexOf("base64") != -1 )
        {
          return fileURL;
        } else {
          return this.apiURL + fileURL;
        }
       } else {
         return null;
       }
    }

    block(msg)
    {
      mApp.block('.m-page', {
          overlayColor: '#000000',
          type: 'loader',
          state: 'success',
          message: msg
      });
    }

    unblock(){
      mApp.unblock('.m-page');
    }

}