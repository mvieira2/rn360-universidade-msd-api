import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { LessonsService } from '../../../../_services/lessons.service';  
import { _globals } from '../../../../globals'; 

declare var swal:any;
declare var mApp:any;
declare var moment:any;

@Component({
    selector: "app-inner",
    templateUrl: "./classrooms.component.html",
    encapsulation: ViewEncapsulation.None
})
export class ClassroomsComponent implements OnInit, AfterViewInit {

    frmNewClass:any;
    searchText:any;
    model: any = {};
    comboSchools:any = [];
    filterArea:string = "";
    filterSchool:any;
    loading = false;
    topics=[1,2,3,4,5,6,7,8];
    profiles: any;
    boolNewItem:boolean = false;
    datePicker:any;
    timePicker:any;
    questions:any = [];
    segments:any = [];
    schools:any = [];
    list: any[] = [];
    listQuestions: any[] = [];
    questionsAdded:number = 0;
    selectedSchools:any[] = [];

    constructor(private _script: ScriptLoaderService , private _lessonsService: LessonsService) {

    }

    ngOnInit() 
    {
      this.profiles = _globals.profiles;
      this.segments = _globals.segments;   
      this.schools = _globals.schools;
      this.getAll();
    }

    ngAfterViewInit() 
    {
        this.frmNewClass = $("#frmNewClass");
        $('#m_modal_1').on('hidden.bs.modal', (e)=> { this.model = Object.assign({}, {});});
        $('#filter-areas').selectpicker({noneSelectedText:"Filtrar pelo segmento"});
        this.datePicker = $('#m_datepicker_2');
        this.timePicker = $('#m_timepicker_1');
        this.handleFormSubmit();
    }

    getAll()
    {
      this.block('Carregando...');

      this._lessonsService.getAll().subscribe(
        response => {
          //console.log(response.data.entries);
          this.list = response.data.entries;

          this._lessonsService.getAllQuestions().subscribe(
          response => {
            console.log(response.data.entries);
            this.listQuestions = response.data.entries;
            //this.listQuestions = JSON.parse('[{"title":"Title Question 1","lesson":{"name":"Aula 2","teachers":"Professores","description":"Teste","startedAt":"2018-06-20T13:00:00","link":"https://www.youtube.com/watch?v=Ra8IbQ7-vcc","options":0,"segment":{"name":"Saúde Animal Brasil","identifier":"2a6139a40ddd4d11b020661ea47dfbbd"},"schools":[{"name":"Avicultura","photo":"static/e76e1/e76e133f3fcf42c58878f1f2ffd0ce58.jpeg","segment":{"name":"Saúde Animal Brasil","identifier":"2a6139a40ddd4d11b020661ea47dfbbd"},"identifier":"e76e133f3fcf42c58878f1f2ffd0ce58"},{"name":"Pecuária","photo":"static/a2f7f/a2f7f555cc224f7d964edae14e79bd94.jpeg","segment":{"name":"Saúde Animal Brasil","identifier":"2a6139a40ddd4d11b020661ea47dfbbd"},"identifier":"a2f7f555cc224f7d964edae14e79bd94"}],"profileAccess":"[0,1]","identifier":"1d295f90917c4c1e9eaec0e67b5c7225"},"answers":[{"value":"Answer 1","isRight":true,"identifier":"9e0bcef4f323405796a97bf352456fc2"},{"value":"Answer 2","isRight":false,"identifier":"195152b7718c4ea687e8bf847bff7eaa"},{"value":"Answer 3","isRight":false,"identifier":"c0580fda0cd8441b88a2b63c5496d6bf"},{"value":"Answer 4","isRight":false,"identifier":"8e635952e075415482e3b5fe0f1bafb4"},{"value":"Answer 5","isRight":false,"identifier":"ff369d20362f47c783a3c25243b73c65"}],"identifier":"e22389e76e2e41efa0ee968d4a02c2b6"},{"title":"Title Question 2","lesson":{"name":"Aula 2","teachers":"Professores","description":"Teste","startedAt":"2018-06-20T13:00:00","link":"https://www.youtube.com/watch?v=Ra8IbQ7-vcc","options":0,"segment":{"name":"Saúde Animal Brasil","identifier":"2a6139a40ddd4d11b020661ea47dfbbd"},"schools":[{"name":"Avicultura","photo":"static/e76e1/e76e133f3fcf42c58878f1f2ffd0ce58.jpeg","segment":{"name":"Saúde Animal Brasil","identifier":"2a6139a40ddd4d11b020661ea47dfbbd"},"identifier":"e76e133f3fcf42c58878f1f2ffd0ce58"},{"name":"Pecuária","photo":"static/a2f7f/a2f7f555cc224f7d964edae14e79bd94.jpeg","segment":{"name":"Saúde Animal Brasil","identifier":"2a6139a40ddd4d11b020661ea47dfbbd"},"identifier":"a2f7f555cc224f7d964edae14e79bd94"}],"profileAccess":"[0,1]","identifier":"6670147eddd74d5cb51af9032afa9339"},"answers":[{"value":"Answer 6","isRight":true,"identifier":"818dbe0747124177b836c28deecb65c6"},{"value":"Answer 7","isRight":false,"identifier":"bad4537e66664684b6725e056c3e5a20"},{"value":"Answer 8","isRight":false,"identifier":"88542ba6f46a4827b7d6993b99048f53"},{"value":"Answer 9","isRight":false,"identifier":"718555c9bcd54feba695e758478dd869"},{"value":"Answer 10","isRight":false,"identifier":"95f753f4e0794250ad88ae51a2f79f08"}],"identifier":"818a1f1593ce4053bc992f9dfb8d3735"},{"title":"Title Question 1","lesson":{"name":"Aula 1 ","teachers":"Professor 1","description":"Teste Descrição","startedAt":"2018-06-20T16:00:00","link":"https://www.youtube.com/watch?v=yf7p-uWcQ-4","options":0,"segment":{"name":"Latam","identifier":"a5a5c132d3474119ab68b8d890955928"},"schools":[{"name":"Suínios","photo":"static/5a303/5a303757ab8345ceb0295cdcd56f956c.jpeg","segment":{"name":"Latam","identifier":"a5a5c132d3474119ab68b8d890955928"},"identifier":"5a303757ab8345ceb0295cdcd56f956c"}],"profileAccess":"[0]","identifier":"1d295f90917c4c1e9eaec0e67b5c7225"},"answers":[{"value":"Answer 1","isRight":true,"identifier":"7f902d50b16847a9a268e961b2a74f97"},{"value":"Answer 2","isRight":false,"identifier":"be47c3539d4f4e9c8940f24a17ea0836"},{"value":"Answer 3","isRight":false,"identifier":"0430551e8c574089b4bd2ae8018bd14b"},{"value":"Answer 4","isRight":false,"identifier":"c35b8bc789ae444598e367f0bf7c5692"},{"value":"Answer 5","isRight":false,"identifier":"7e3813d9c6fe4ee9a0dfdb1016775ea2"}],"identifier":"4921638ee23d45a9918e78891167f1c3"},{"title":"Title Question 1","lesson":{"name":"Aula 1 ","teachers":"Professor 1","description":"Teste Descrição","startedAt":"2018-06-20T16:00:00","link":"https://www.youtube.com/watch?v=yf7p-uWcQ-4","options":0,"segment":{"name":"Latam","identifier":"a5a5c132d3474119ab68b8d890955928"},"schools":[{"name":"Suínios","photo":"static/5a303/5a303757ab8345ceb0295cdcd56f956c.jpeg","segment":{"name":"Latam","identifier":"a5a5c132d3474119ab68b8d890955928"},"identifier":"5a303757ab8345ceb0295cdcd56f956c"}],"profileAccess":"[0]","identifier":"bea2b18a9bf746deb2e9d0f406232af6"},"answers":[{"value":"Answer 1","isRight":true,"identifier":"10db6edede6041e58669504d433dd51a"},{"value":"Answer 2","isRight":false,"identifier":"d0e476d143f34d8cae9e950028b255b7"},{"value":"Answer 3","isRight":false,"identifier":"737f7b4b51be4e7ca3a929b1cfa7ce21"},{"value":"Answer 4","isRight":false,"identifier":"71d2c4aeb3394f8c90161ebff3a499cf"},{"value":"Answer 5","isRight":false,"identifier":"fb3f67c48bd04de4a3ceccbd351eee41"}],"identifier":"6bd1675b4760485ebc507ea49b679189"}]');
            //console.log(this.listQuestions);
            this.unblock();
          },
          error => {
            this.loading = false;
            this.unblock();
          });

        },
        error => {
          this.loading = false;
          this.unblock();
        });

    }

    selectArea()
    {
      this.comboSchools = [];
      this.filterSchool = null;
      
      if( this.filterArea !== "" )
      {
        for( var i in this.schools )
        {
          if( this.schools[i].segment.identifier === this.filterArea )
          {
            this.comboSchools.push(this.schools[i]);
          }
        }

        if( this.comboSchools.length > 0) this.showSchoolsList();

      } else {
        this.comboSchools = this.schools;
        this.showSchoolsList();
      }

      console.log(this.filterSchool);
      console.log(this.comboSchools);
    }

    showAreaList()
    {
      setTimeout(()=>
      { 
        $('#filter-areas-modal').selectpicker('destroy').selectpicker({noneSelectedText:"Escolha a qual segmento a aula pertence"}); 
      }, 600);
    }

    showSchoolsList()
    {
      setTimeout(()=>{ $('#filter-schools').selectpicker('destroy').selectpicker({noneSelectedText:"Filtrar por escola"}); },300);    
    }

    handleFormSubmit() 
    {
      this.frmNewClass.validate({
        rules: {
          name: {
            required: true
          },
          teachers: {
            required: true
          },
          description:{
            required: true
          },
          date:{
            required: true
          },
          time:{
            required: true
          },
          video:{
            required: true
          },
          segment:{
            required: true
          }
        },
      });
    }

    edit(item)
    {
       this.model = Object.assign({}, {});
       //this.questions = this.copy(item.questions);
       this.questions = [];
       this.boolNewItem = false;
       this.selectedSchools = item.schools;
       this.frmNewClass.validate().resetForm();
       this.showAreaList();

       //console.log(item.profileAccess);

       if( item.profileAccess && typeof item.profileAccess === "string" && item.profileAccess != "" )
       {
         let arr = item.profileAccess.substring(1,item.profileAccess.length-1).split(",");
         for( var i in arr ){ arr[i] = parseInt(arr[i]); }
         item.profileAccess = arr;
       }

       setTimeout(()=> { this.model = item; } , 400 );
       setTimeout(()=> 
       { 
         this.appendDateTime(); 
         this.showQuestions(item.identifier);
       } , 500 );
    }

    new()
    {
      this.boolNewItem = true;
      this.model = Object.assign({}, {});
      this.model.segment = {};
      this.questions = [];
      //this.model.questions = [];
      this.model.schools = [];
      this.model.profileAccess = [];
      this.frmNewClass.validate().resetForm();
      this.showAreaList();
      this.appendDateTime();
    }

    create() 
    {
      this.loading = true;

      if( this.frmNewClass.valid() )
      {
        
        this.model.startedAt = moment(this.datePicker.val() + " " + this.timePicker.val(), "DD/MM/YYYY HH:mm").format();
        //this.model.questions = this.copy(this.questions);
        this.model.segment = this.model.segment.identifier;
        this.model.schools = this.selectedSchools;
        
        if( this.model.schools && this.model.schools.length === 0 ) this.model.schools = null;
        if( this.model.schools ) this.validateSchools();

        this.model.profileAccess = [];
        if( $("#profile1").is(":checked") ) this.model.profileAccess.push(0);
        if( $("#profile2").is(":checked") ) this.model.profileAccess.push(1);

        $("#btn-close-modal").trigger("click");

        if( this.boolNewItem )
        {
          this.append();  
        } else{
          this.update();
        }
      }

    }

    append()
    {
      this.loading = true;
      this.block('Enviando dados...');
      this._lessonsService.create(this.model).subscribe(
        response => 
        {
          console.log(response);

          if( this.questions.length > 0 ) 
          {
            this.questionsAdded = 0;
            this.appendQuestions(response.data.identifier);
          } else {
            swal("Sucesso!", "Aula criada com sucesso.", "success");
            this.loading = false;
            this.unblock();
          }

          this.list.push( response.data );
          this.model = {};
        },
        error => 
        {
          swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
          this.loading = false;
          this.unblock();
        });
    }

    update()
    {
      this.loading = true;
      this.block('Enviando dados...');
      this._lessonsService.update(this.model).subscribe(
        response => 
        {
          console.log(response);
          this.model = {};
          
          for( var i in this.list )
          {
            if( this.list[i].identifier === response.data.identifier )
            {
                let obj = Object.assign({}, response.data);
                obj.segment = { identifier:this.list[i].segment };
                this.list[i] = obj;
            }
          }

          if( this.questions.length > 0 ) 
          {
            this.questionsAdded = 0;
            this.appendQuestions(response.data.identifier);
          } else {
            swal("Sucesso!", "Aula atualizada com sucesso.", "success");
            this.loading = false;
            this.unblock();
          }

        },
        error => {
          swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
          this.loading = false;
          this.unblock();
        });
    }

    appendQuestions(lesson)
    {
      if( this.questionsAdded < this.questions.length )
      {
        this.questions[this.questionsAdded].lesson = lesson;

        if( this.questions[this.questionsAdded].identifier )
        {

          this._lessonsService.updateQuestion(this.questions[this.questionsAdded]).subscribe(
          response => 
          {
            console.log( "Update question : " + response.data );
            
            for( var i in this.listQuestions )
            {
              if( this.listQuestions[i].identifier === response.data.identifier )
              {
                  let obj = Object.assign({}, response.data);
                  this.listQuestions[i] = obj;
              }
            }

            this.questionsAdded++;
            this.appendQuestions(lesson);
          },
          error => 
          {
            this.questionsAdded++;
            this.appendQuestions(lesson);  
          });

        } else {

          this._lessonsService.createQuestion(this.questions[this.questionsAdded]).subscribe(
          response => 
          {
            console.log( "Added question : " + response.data );
            this.listQuestions.push(response.data);
            this.questionsAdded++;
            this.appendQuestions(lesson);
          },
          error => 
          {
            this.questionsAdded++;
            this.appendQuestions(lesson);  
          });

        }

        
      } else {
        swal("Sucesso!", "Aula criada com sucesso.", "success");
        this.unblock();
      }
    }

    showQuestions(lesson)
    {
      let copyList = this.copy(this.listQuestions);
      let list:any = [];
      let count:number = 1;

      copyList.forEach((item)=> 
      {
         if( item.lesson.identifier === lesson ) 
         {
           item.id = count;
           item.answers.forEach((answer)=> { answer.saved = true; });
           list.push(item);
           count++;
         }
      });

      this.questions = list;
      //console.log(this.questions.length)
      if( this.questions.length > 0 ) this.model.optExame = true;
    }

    checkSchoolValue(value)
    {
      for( var i in this.model.schools )
      {
        if( this.model.schools[i].identifier === value ) return true;
      }
      return false;
    }

    checkProfile(value)
    {
      for( var i in this.model.profileAccess )
      {
        if( this.model.profileAccess[i] === value ) return true;
      }      
      return false;
    }

    onSelectSchool(item)
    {
      if( $(item).is(":checked") )
      {
        this.selectedSchools.push(item.value);
      } else {
        this.selectedSchools = this.selectedSchools.filter(function(value){ return value !== item.value; });
      }
    }

    addQuestion()
    {
      this.questions.push(
      {
        id:(this.questions.length+1),
        answers:[{ value:"" , saved:false , isRight:true }]
      });
    }

    addAnswer(list)
    {
      list.push({ value:"" , saved:false , isRight:false });
    }

    removeAnswer(array, element) 
    {
        const index = array.indexOf(element);
        array.splice(index, 1);
    }

    getLabelField(isRight)
    {
      if( isRight )
      {
        return "Adicione uma resposta certa";
      } else {
        return "Adicione uma resposta errada";
      }
    }  

    copy(o) 
    {
       var output, v, key;
       output = Array.isArray(o) ? [] : {};
       for (key in o) {
           v = o[key];
           output[key] = (typeof v === "object") ? this.copy(v) : v;
       }
       return output;
     }

    appendDateTime()
    {
      this.datePicker.datepicker({
            format: 'dd/mm/yyyy',
            todayHighlight: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
      });

      this.timePicker.timepicker({
          defaultTime: '00:00',
          minuteStep: 1,
          showSeconds: false,
          showMeridian: false
      });

      if( this.model.startedAt )
      {
         this.model.date = moment(this.model.startedAt).format("DD/MM/YYYY");
         this.model.time = moment(this.model.startedAt).format('H:mm');
      }
    }

    validateSchools()
    {
      let list = [];

      this.model.schools.forEach((item)=> 
      {
         if(typeof item === "string")
         {
           list.push(item);
         } else {
           list.push(item.identifier);
         }
      });

      this.model.schools = list;
    }

    verifySegment()
    {
      if( this.model.segment && this.model.segment.identifier && this.hasSchollsOnSegment() )
      {
        return true;
      } else {
        return false;
      }
    }

    hasSchollsOnSegment()
    {
      for( var i in this.schools )
      {
        if( this.schools[i].segment.identifier === this.model.segment.identifier ) return true;
      }
      return false;
    }

    openDeleteModal(idItem)
    {
      swal({
          title: 'Você tem certeza que deseja deletar esssa aula?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Sim',
          cancelButtonText:'Cancelar'
      }).then((result)=> {
          
          if (result.value) 
          {
              this.loading = true;
              this.block('Enviando dados...');
              this._lessonsService.delete(idItem).subscribe(
              response => 
              {
                swal("Sucesso!", "Aula deletada com sucesso.", "success");
                this.list = this.list.filter(function(item){ return item.identifier !== idItem; });
                this.loading = false;
                this.unblock();
              },
              error => {
                swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
                this.loading = false;
                this.unblock();
              });
          }
      });
    }

    block(msg)
    {
      mApp.block('.m-page', {
          overlayColor: '#000000',
          type: 'loader',
          state: 'success',
          message: msg
      });
    }

    unblock(){
      mApp.unblock('.m-page');
    }


}