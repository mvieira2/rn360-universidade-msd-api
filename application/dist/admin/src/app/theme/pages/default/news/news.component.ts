import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { NewsService } from '../../../../_services/news.service';  
import { _globals } from '../../../../globals'; 

declare var swal:any;
declare var mApp:any;
declare var $:any;
declare var moment:any;
declare var Dropzone:any;

@Component({
    selector: "app-inner",
    templateUrl: "./news.component.html",
    encapsulation: ViewEncapsulation.None
})
export class NewsComponent implements OnInit, AfterViewInit {

    frmNewAlerts:any;
    searchText:any;
    profiles:any;
    model: any = {};
    comboSchools:any = [];
    filterArea:any;
    filterSchool:any;
    validateForm:any;
    loading = false;
    topics=[1,2,3,4,5,6,7,8];
    datePicker:any;
    myDropzone:any;
    optsSchools:any = [];
    file:any;
    boolNewItem:boolean = false;
    questions:any = [];
    segments:any = [];
    schools:any[] = [];
    list: any[] = [];
    selectedSchools:any[] = [];

    constructor(private _script: ScriptLoaderService, private _newsService: NewsService ) {

    }

    ngOnInit() 
    {
       this.profiles = _globals.profiles;
       this.segments = _globals.segments;
       this.schools = _globals.schools;
       this.getAll();
    }

    ngAfterViewInit() 
    {
        this.frmNewAlerts = $("#frmNewAlerts");
        this.datePicker = $('#m_datepicker_2');
        this.handleFormSubmit();

        $('#m_modal_1').on('hidden.bs.modal', (e)=> { this.model = Object.assign({}, {});});
        $('#filter-areas').selectpicker({noneSelectedText:"Filtrar pelo segmento"});
        $('#m_summernote').summernote({
          placeholder: 'Insira sua notícia',
          tabsize: 2,
          height: 250,
          callbacks: 
          {
              onImageUpload : (files, editor, welEditable)=>
              {
                for(var i = files.length - 1; i >= 0; i--) {
                     this.sendFile(files[i], this);
                }
              }
          }
        });

        Dropzone.options.mDropzoneSchool = 
        {
            init:function()
            {
              this.destroy();
            }
        };

        setTimeout( ()=>{ this.appendDropZone() } , 500 );
    }

    getAll()
    {
      this.block('Carregando...');

      this._newsService.getAll().subscribe(
        response => {
          this.list = response.data.entries;
          this.unblock();
        },
        error => {
          this.loading = false;
          this.unblock();
        });

    }

    sendFile(file, el)
    {
      console.log(file);
      var reader = new FileReader();
      reader.addEventListener("loadend", function(event:any) 
      { 
        console.log(event);
        $('#m_summernote').summernote('editor.insertImage', event.target.result );
      });
      reader.readAsDataURL(file);
    }

    appendDropZone()
    {
      let _ref = this;

      $('#m-dropzone-school').dropzone({
          paramName: "file", // The name that will be used to transfer the file
          maxFiles: 1,
          maxFilesize: 10, // MB
          resizeWidth:150,
          resizeHeight:150,
          addRemoveLinks: true,
          acceptedFiles: "image/*",
          init:function()
          {
            _ref.myDropzone = this;
            console.log("init drop profile");
          },
          accept: function(file, done) 
          {
              _ref.file = file;
              done(); 
          }
      })
    }

    appendImage(url)
    {
      var mockFile = 
      { 
            type: 'image/jpeg', 
            status: Dropzone.ADDED, 
            url: url
        };

        this.myDropzone.removeAllFiles(true);
        this.myDropzone.emit("addedfile", mockFile);
        this.myDropzone.emit("thumbnail", mockFile, url );
        this.myDropzone.files.push(mockFile);
    }

    selectArea()
    {
      this.comboSchools = [];
      this.filterSchool = null;
      
      if( this.filterArea !== "" )
      {
        for( var i in this.schools )
        {
          if( this.schools[i].segment.identifier === this.filterArea )
          {
            this.comboSchools.push(this.schools[i]);
          }
        }

        if( this.comboSchools.length > 0) this.showSchoolsList();

      } else {
        this.comboSchools = this.schools;
        this.showSchoolsList();
      }
    }

    showAreaList()
    {
      setTimeout(()=>
      { 
        $('#filter-areas-modal')
        .selectpicker('destroy')
        .selectpicker({noneSelectedText:"Escolha a qual segmento o alerta pertence"});
      },300);
    }

    showSchoolsList()
    {
      setTimeout(()=>{ $('#filter-schools').selectpicker('destroy').selectpicker({noneSelectedText:"Filtrar por escola"}); },300);    
    }

    handleFormSubmit() 
    {
      $.validator.setDefaults({
        // This will ignore all hidden elements alongside `contenteditable` elements
        // that have no `name` attribute
        ignore: ":hidden, [contenteditable='true']:not([name])"
      });

      this.validateForm = this.frmNewAlerts.validate({
        rules: {
          name: {
            required: true
          },
          description: {
            required: true
          },
          segment:{
            required: true
          },
          topicgroup:
          {
            required: true
          }
        }
      });
    }

    edit(item)
    {
       this.model = Object.assign({}, {});
       this.boolNewItem = false;
       this.selectedSchools = item.schools;
       this.frmNewAlerts.validate().resetForm();
       this.showAreaList();

       if( item.photo ) this.appendImage(item.photo);

       $("#m_summernote").summernote('code' , item.content );
       
       setTimeout(()=> { this.model = item; } , 100 );
       setTimeout(()=> { this.appendDateTime(); $('.note-editable').scrollTop(0); } , 200 );
    }

    new()
    {
      this.myDropzone.removeAllFiles(true);
      this.boolNewItem = true;
      this.model = Object.assign({}, {});
      this.model.segment = {};
      this.model.schools = [];
      
      $("#m_summernote").summernote('code' , "" );
      $('.note-editable').scrollTop(0);

      this.frmNewAlerts.validate().resetForm();
      this.showAreaList();
      this.appendDateTime();
      setTimeout(()=> { $('.note-editable').scrollTop(0); } , 100 );
    }

    create() 
    {
      this.loading = true;
      
      
      if( this.frmNewAlerts.valid() )
      {
        $("#btn-close-modal").trigger("click");

        this.model.publishedAt = moment(this.datePicker.val() , "DD/MM/YYYY").format();
        this.model.segment = this.model.segment.identifier;
        this.model.schools = this.selectedSchools;
        
        if( this.model.schools && this.model.schools.length === 0 ) this.model.schools = null;
        if( this.model.schools ) this.validateSchools();

        var textareaValue = $("#m_summernote").summernote('code');
        this.model.content = textareaValue;

        if( this.boolNewItem )
        {
          this.append();  
        } else{
          this.update();
        }

      }
      
    }

    checkSchoolValue(value)
    {
      for( var i in this.model.schools )
      {
        if( this.model.schools[i].identifier === value ) return true;
      }
      return false;
    }

    onSelectSchool(item)
    {
      if( $(item).is(":checked") )
      {
        this.selectedSchools.push(item.value);
      } else {
        this.selectedSchools = this.selectedSchools.filter(function(value){ return value !== item.value; });
      }
    }

    verifySegment()
    {
      if( this.model.segment && this.model.segment.identifier && this.hasSchollsOnSegment() )
      {
        return true;
      } else {
        return false;
      }
    }

    hasSchollsOnSegment()
    {
      for( var i in this.schools )
      {
        if( this.schools[i].segment.identifier === this.model.segment.identifier ) return true;
      }
      return false;
    }

    append()
    {
      this.loading = true;
      this.block('Enviando Dados...');
      this._newsService.create(this.model).subscribe(
        response => 
        {
          swal("Sucesso!", "Notícia criada com sucesso.", "success");
          this.list.push( response.data );
          this.model = {};
          this.loading = false;
          this.unblock();
        },
        error => 
        {
          swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
          this.loading = false;
          this.unblock();
        });
    }

    update()
    {
      this.loading = true;
      this.block('Enviando Dados...');
      this._newsService.update(this.model).subscribe(
        response => 
        {
          console.log(response);
          swal("Sucesso!", "Notícia atualizada com sucesso.", "success");
          
          for( var i in this.list )
          {
            if( this.list[i].identifier === response.data.identifier )
            {
                let obj = Object.assign({}, response.data);
                obj.segment = { identifier:this.list[i].segment };
                this.list[i] = obj;
            }
          }

          this.loading = false;
          this.unblock();
        },
        error => {
          swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
          this.loading = false;
          this.unblock();
        });
    }

    appendDateTime()
    {
      this.datePicker.datepicker({
            format: 'dd/mm/yyyy',
            todayHighlight: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
      });

      if( this.model.publishedAt )
      {
         this.model.date = moment(this.model.publishedAt).format("DD/MM/YYYY");
      }
    }

    openDeleteModal(idItem)
    {
      swal({
          title: 'Você tem certeza que deseja deletar essa notícia?',
          //text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Sim',
          cancelButtonText:'Cancelar'
      }).then((result)=> {
          
          if (result.value) 
          {
              this.loading = true;
              this.block('Enviando Dados...');
              this._newsService.delete(idItem).subscribe(
              response => 
              {
                swal("Sucesso!", "Notícia deletada com sucesso.", "success");
                this.list = this.list.filter(function(item){ return item.identifier !== idItem; });
                this.loading = false;
                this.unblock();
              },
              error => {
                swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
                this.loading = false;
                this.unblock();
              });
          }
      });
    }

    validateSchools()
    {
      let list = [];

      this.model.schools.forEach((item)=> 
      {
         if(typeof item === "string")
         {
           list.push(item);
         } else {
           list.push(item.identifier);
         }
      });

      this.model.schools = list;
    }


    block(msg)
    {
      mApp.block('.m-page', {
          overlayColor: '#000000',
          type: 'loader',
          state: 'success',
          message: msg
      });
    }

    unblock(){
      mApp.unblock('.m-page');
    }

}