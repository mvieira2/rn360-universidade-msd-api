import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { AuthenticationService } from '../../../../auth/_services/authentication.service';  
import { UserService } from '../../../../auth/_services/user.service';  
import { _globals } from '../../../../globals'; 

declare var Dropzone:any;  
declare var swal:any;
declare var mApp:any;

@Component({
    selector: "app-index",
    templateUrl: "./index.component.html",
    providers: [
      AuthenticationService
    ],
    encapsulation: ViewEncapsulation.None
})
export class IndexComponent implements OnInit, AfterViewInit {

    apiURL:any;
    frmNewUser:any;
    searchText:any;
    model: any = {};
    filterArea:any;
    loading = false;
    myDropzone:any;
    imgCrop:any;
    file:any;
    boolNewUser:boolean = false;
    profiles: any;
    segments:any = [];
    schools: any[] = [];
    users: any[] = [];

    constructor(private _script: ScriptLoaderService , private _authService: AuthenticationService , private _userService: UserService ) {

    }

    ngOnInit() 
    {
        this.apiURL = _globals.apiURL;
        this.profiles = _globals.profiles;
        this.segments = _globals.segments;
        this.schools = _globals.schools;
        this.getAllUsers();
    }

    ngAfterViewInit() 
    {
        this.frmNewUser = $("#frmNewUser");

        var SPMaskBehavior = function(val) 
        {
          return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        }

        var optionsBehavior = 
        {
            onKeyPress: function(val, e, field, options)
            {
              field.mask( SPMaskBehavior.apply({}, arguments), options);
            }
        };

        Dropzone.options.mDropzoneProfile = 
        {
            init:function()
            {
              this.destroy();
            }
        };

        $('#filter-areas').selectpicker({noneSelectedText:"Filtrar pelo segmento"});
        $('.phone').mask( SPMaskBehavior , optionsBehavior );
        $('#m_modal_1').on('hidden.bs.modal', (e)=> { this.model = Object.assign({}, {});});
        $('input[type=radio][name=profile]').on('change', (e)=>
        {
          this.showAreaList();
        });

        this.handleFormSubmit();
        setTimeout( ()=>{ this.appendDropZone() } , 500 );
    }

    getAllUsers()
    {
      this.block('Carregando...');

      this._userService.getAll().subscribe(
        response => {
          this.users = response.data.entries;
          this.unblock();
        },
        error => {
          this.loading = false;
          this.unblock();
        });

    }

    showAreaList()
    {
      setTimeout(()=>{ $('#filter-areas-modal').selectpicker('destroy').selectpicker({noneSelectedText:"Escolha a qual segmento o usuário pertence"}); },300);
    }

    showSchoolsList()
    {
      setTimeout(()=>{ $('#filter-schools-modal').selectpicker('destroy').selectpicker({noneSelectedText:"Escolha a qual escola o usuário pertence"}); },300);    
    }

    appendImage(url)
    {
      var mockFile = 
      { 
            type: 'image/jpeg', 
            status: Dropzone.ADDED, 
            url: url
        };

        this.myDropzone.removeAllFiles(true);
        this.myDropzone.emit("addedfile", mockFile);
        this.myDropzone.emit("thumbnail", mockFile, url );
        this.myDropzone.files.push(mockFile);
    }

    handleFormSubmit() 
    {
      this.frmNewUser.validate({
        rules: {
          name: {
            required: true
          },
          email: {
            required: true,
            email: true,
          },
          phone: {
            required: true
          },
          profile:{
            required: true
          },
          segment:{
            required: true
          }
        },
      });
    }

    editUser(user)
    {
       this.model = Object.assign({}, {});
       this.boolNewUser = false;
       this.frmNewUser.validate().resetForm();

       if( !user.segment ) user.segment = {identifier:""};
       if( !user.school ) user.school = {identifier:""};
       if( user.photo ) this.appendImage( this.getImage(user.photo) );
       if( user.profileAccess && typeof user.profileAccess === "string" && user.profileAccess != "" )
       {
         let arr = user.profileAccess.substring(1,user.profileAccess.length-1).split(",");
         for( var i in arr ){ arr[i] = parseInt(arr[i]); }
         user.profileAccess = arr;
       }

       user.profile = String(user.profile);

       console.log(user);
       
       setTimeout(()=> 
       { 
         this.model = user; 
         this.showAreaList();
         this.showSchoolsList();
       } , 100 );

    }

    newUser()
    {
      this.myDropzone.removeAllFiles(true);
      this.boolNewUser = true;
      this.model = {};
      //this.schools = this.areas = [];
      this.model.segment = { identifier:"" };
      this.model.school = { identifier:"" };
      this.frmNewUser.validate().resetForm();
    }

    create() 
    {
      
      this.loading = true;

      if( this.frmNewUser.valid() )
      {
        $("#btn-close-modal").trigger("click");

        this.model.segment = this.model.segment.identifier;
        this.model.school = ( this.model.school && this.model.school.identifier !== "" ) ? this.model.school.identifier : null;
        this.model.profile = parseInt( this.model.profile );
        this.model.password = null;

        if( this.model.photo && this.model.photo.indexOf("base64") === -1 )
        {
          this.model.photo = null;
        }

        if( this.imgCrop ) this.model.photo = this.imgCrop;

        console.log(this.model);

        if( this.boolNewUser )
        {
          this.appendNewUser();
        } else{
          this.updateUserList();
        }

      }

    }

    appendNewUser()
    {
      this.loading = true;
      this.block('Enviando dados...');

      this._userService.create(this.model).subscribe(
        response => 
        {
          console.log(response);
          swal("Sucesso!", "Usuário criado com sucesso.", "success");
          this.users.push( response.data );
          this.model = {};
          this.loading = false;
          this.unblock();

        },
        error => {

          //this.showAlert('alertSignin');
          //this._alertService.error(error);
          swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
          this.loading = false;
          this.unblock();

        });
    }

    updateUserList()
    {
      this.model.profile = parseInt( this.model.profile );
      this.loading = true;
      this.block('Enviando dados...');

      this._userService.update(this.model).subscribe(
        response => 
        {
          this.model = {};
          swal("Sucesso!", "Usuário atualizado com sucesso.", "success");
          
          for( var i in this.users )
          {
            if( this.users[i].identifier === response.data.identifier )
            {
                this.users[i] = response.data;
                if( this.imgCrop ) this.users[i].photo = this.imgCrop;
            }
          }

          this.loading = false;
          this.unblock();
        },
        error => {
          swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
          this.loading = false;
          this.unblock();
        });
    }

    appendDropZone()
    {
      let _ref = this;
      
      $('#m-dropzone-profile').dropzone({
          paramName: "file", // The name that will be used to transfer the file
          maxFiles: 1,
          maxFilesize: 10, // MB
          resizeWidth:150,
          resizeHeight:150,
          addRemoveLinks: true,
          acceptedFiles: "image/*",
          init:function()
          {
            _ref.myDropzone = this;
            this.on("addedfile", function(file) 
            {
             if(file.name){
               var reader = new FileReader();
                reader.addEventListener("loadend", function(event:any) 
                { 
                  _ref.imgCrop = event.target.result;
                  $('#imageCrop').cropper('destroy');
                  setTimeout( ()=>{ _ref.appendCropCntrl() } , 600 );
                  $('#modal_crop').modal();
                });

                reader.readAsDataURL(file);  
             }
             
            });
            console.log("init drop profile");
          },
          accept: function(file, done) 
          {
              _ref.file = file;
              done(); 
          }
      });
    }

    openDeleteModal(idUser)
    {
      swal({
          title: 'Você tem certeza que deseja deletar esse usuário?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Sim',
          cancelButtonText:'Cancelar'
      }).then((result)=> 
      {    
          if (result.value) 
          {
              this.loading = true;
              this.block('Enviando dados...');
              this._userService.delete(idUser).subscribe(
              response => 
              {
                swal("Sucesso!", "Usuário deletado com sucesso.", "success");
                this.users = this.users.filter(function(item){ return item.identifier !== idUser; });
                this.loading = false;
                this.unblock();
              },
              error => {
                swal("Ops", "Ocorreu erro ao enviar os dados.", "error");
                this.loading = false;
                this.unblock();
              });
          }
      });
    }

    getItemName(item , list)
    {
      if( typeof item === "string" )
      {
        let obj = this.getItemById(item , list);
        return obj.name;
      } else {
        return item.name;
      }
    }

    getItemById( value:string , list:any[] )
    {
      for( var i in list)
      {
         if( list[i].identifier === value )
         {
            return list[i];
         }
      }
      return null;
    }

    getImage(imgURL)
    {
      if(imgURL)
      {
        if( imgURL.indexOf("base64") != -1 )
        {
          return imgURL;
        } else {
          return this.apiURL + imgURL;
        }
       } else {
         return null;
       }
    }

    appendCropCntrl()
    {
        var $image = $('#imageCrop');
        $image.cropper({ aspectRatio: 1 / 1, crop: function(event) {}});
        var cropper = $image.data('cropper');
    }

    verifySegment()
    {
      if( this.model.segment && this.model.segment.identifier && this.hasSchollsOnSegment() )
      {
        return true;
      } else {
        return false;
      }
    }

    hasSchollsOnSegment()
    {
      for( var i in this.schools )
      {
        if( this.schools[i].segment.identifier === this.model.segment.identifier ) return true;
      }
      return false;
    }


    getCroppedImage()
    {
      let $image = $('#imageCrop');
      let canvas = $image.cropper('getCroppedCanvas');
      this.imgCrop = canvas.toDataURL();
      this.appendImage(this.imgCrop);
      $("#btn-close-modal-crop").trigger("click");
    }

    block(msg)
    {
      mApp.block('.m-page', {
          overlayColor: '#000000',
          type: 'loader',
          state: 'success',
          message: msg
      });
    }

    unblock(){
      mApp.unblock('.m-page');
    }

}