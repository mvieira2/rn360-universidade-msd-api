export class News {
	identifier: string;
	publishedAt: string;
	content:any;
	segment:any;
	schools:any[];
}