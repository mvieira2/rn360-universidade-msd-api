export class Radio {
	identifier: string;
	name: string;
	description: string;
	audioFile:any;
	segment:any;
	edition:string;
	schools:any[];
}