export class Alert {
	identifier: string;
	name: string;
	description: string;
	schools:any[];
}