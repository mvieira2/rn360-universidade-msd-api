# README #
Repositório para versionamento do Projeto da Universidade MSD, em parceria com a RN360

### Introdução ###
* Universidade MSD
* 1.0

## Wireframes ##
* https://8xj4a7.axshare.com
* Senha de acesso: RNMSD360

## Escopo ##
https://docs.google.com/document/d/1Ex9dr5e-3LK4Ln3ng2EkxPWpNko2MkVcPWiatXBT66I/edit?usp=sharing

### Ambiente de Homologação ###
* URL: http://msd.embed.com.br

## Database ##
* URL: msdembed.cjlzpwcgwyye.us-east-1.rds.amazonaws.com
* User: db_msd
* Pass: emb@74Pass
* Database: msdDB
* Port: 1433

* Master Username DB: root
* Master Password DB: ekgg92To0

## FTP ##
* IP: 35.174.70.38
* User: msd
* Pass: emb@93Pass
* Port: 21

### Who do I talk to? ###
* Ary Oliveira
* ary.oliveira@embed.com.br